﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpByte.Dynamic.Impl
{
    /// <summary>A sample formatter implementation, which formats pattern-based dynamically executable objects for compilation</summary>
    internal sealed class ExecutablePatternFormatter: ExecutableFormatter
    {
        /// <summary>A regular expression for capturing numbered placeholders</summary>
        private const string NumberedPlaceholderRegex = "{{{[0]}}}|{{{[1-9][0-9]*}}}";

        #region Tokens

        private const string UsingDirectivesToken = "{{{UsingDirectives}}}";

        /// <summary>A token used in replacement within code templates</summary>
        private const string SourceToken = "{{{Source}}}";

        /// <summary>A token used in replacement within code templates</summary>
        private const string SourceEscapedToken = "{{{SourceEscaped}}}";

        /// <summary>A token used in replacement within code templates</summary>
        private const string KeyEscapedToken = "{{{KeyEscaped}}}";

        /// <summary>A token used in replacement within code templates</summary>
        private const string TypeIdToken = "{{{TypeID}}}";

        #endregion

        /// <summary>A unique key prefix</summary>
        private static string KeyPrefix = ExecutableType.Pattern.ToString();

        /// <summary>A safe default parameter collection value</summary>
        private static IEnumerable<string> EmptyParameterNames = new List<string>();

        /// <summary>A safe default parameter collection value</summary>
        private static IEnumerable<string> EmptyNamespaces = new List<string>();

        /// <summary>The executable code template</summary>
        public const string CodeTemplate =
@"{{{UsingDirectives}}}

namespace SharpByte.Dynamic
{
    public sealed class Executable{{{TypeID}}} : SharpByte.Dynamic.Impl.ExecutablePattern
    {
        public override string SourceCode { get { return @""{{{SourceEscaped}}}""; }}
        public override string Key { get { return @""{{{KeyEscaped}}}""; }}

        public Executable{{{TypeID}}}() : base() {}

        protected sealed override object ExecuteImpl(dynamic this{{{TypeID}}}, IList<dynamic> ph{{{TypeID}}}, IDictionary<string, dynamic> p{{{TypeID}}}, object df{{{TypeID}}} = null) 
        {   
{{{Source}}};
        }

        public override IExecutable Copy() { return new Executable{{{TypeID}}}(); }
    }
}";

        /// <summary>Formats source code for compilation</summary>
        /// <param name="source">The source to format</param>
        /// <param name="parameterNames">Named parameters for the source</param>
        /// <param name="usingNamespaces">Namespaces to add</param>
        /// <returns>Formatted source code</returns>
        public override string Format(string source, IEnumerable<string> parameterNames, IEnumerable<string> usingNamespaces)
        {
            if (source == null) return "";
            if (parameterNames == null) parameterNames = EmptyParameterNames;
            if (usingNamespaces == null) usingNamespaces = EmptyNamespaces;

            string typeId = Guid.NewGuid().ToString("N").Substring(0, 16);
            string className = "SharpByte.Dynamic.Executable" + typeId;
            string placeholderPrefix = "ph" + typeId;
            string parameterPrefix = "p" + typeId;
            string contextReference = "this" + typeId;
            string sourceEscaped = source.Replace("\"", "\"\"");
            string keyEscaped = (KeyPrefix + "|" + string.Join(",", parameterNames.OrderBy(p => p)) + "|" + source).Replace("\"", "\"\"");

            List<string> placeholderFields = new List<string>();
            StringBuilder usingDirectives = new StringBuilder();

            #region 1. Generate using directives

            foreach (string usingNamespace in usingNamespaces)
            {
                string usingDirective = usingNamespace.Trim();
                if (!string.IsNullOrEmpty(usingDirective))
                {
                    if (!usingDirective.StartsWith("using "))
                        usingDirective = "using " + usingDirective;
                    if (!usingDirective.EndsWith(";"))
                        usingDirective += ";";

                    if (usingDirectives.Length > 0)
                        usingDirectives.Append(Environment.NewLine);
                    usingDirectives.Append(usingDirective);
                }
            }

            #endregion

            #region 2. Rename parameters

            source = source.ReplacePattern("\\bthis\\b", contextReference);

            foreach (string placeholder in source.FindAll(NumberedPlaceholderRegex, true))
                source = source.Replace(placeholder, placeholderPrefix + "[" + placeholder.Trim('{', '}') + "]");

            foreach (string parameter in parameterNames)
                source = source.ReplacePattern("\\b" + parameter + "\\b", parameterPrefix + "[\"" + parameter + "\"]", true);
//                source = source.ReplacePattern("([^a-zA-Z0-9_]|^|\\w)" + parameter + "([^a-zA-Z0-9_]|$|\\w)", parameterPrefix + "[" + parameter + "]", true);

            #endregion

            source = CodeTemplate.ReplacePattern(SourceToken, source);
            source = source.ReplacePattern(SourceEscapedToken, sourceEscaped);
            source = source.ReplacePattern(KeyEscapedToken, keyEscaped);

            source = source.ReplacePattern(TypeIdToken, typeId);
            source = source.ReplacePattern(UsingDirectivesToken, usingDirectives.ToString());

            return new ClassTemplate(className, sourceCode);
        } 
    }
}
