﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace SharpByte.Dynamic.Impl
{
    /// <summary>A base class for executable implementations</summary>
    public abstract class Executable : IExecutable
    {
        /// <summary>A date/time format compliant with ISO 8601</summary>
        private const string TimestampFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffzzz";

        /// <summary>A zero-duration time span</summary>
        private static TimeSpan ZeroDuration = new TimeSpan(0L);

        /// <summary>Used in executation synchronization</summary>
        private object monitor = new object();

        /// <summary>Constructs a new instance</summary>
        internal Executable()
        {
            this.ExecutionStatus = ExecutionStatus.Ready;
            this.ExecutionStart = DateTime.MinValue;
            this.ExecutionEnd = DateTime.MaxValue;
        }

        #region Execution tracking members

        /// <summary>Gets the execution status for this executable</summary>
        public ExecutionStatus ExecutionStatus { get; protected set; }

        /// <summary>Gets the last exception, if any, encountered by the executable</summary>
        public Exception Exception { get; protected set; }

        /// <summary>Gets the execution start time for this executable</summary>
        public DateTime ExecutionStart { get; protected set; }

        /// <summary>Gets the execution end time for this executable</summary>
        public DateTime ExecutionEnd { get; protected set; }

        /// <summary>Calculates the execution time for this executable</summary>
        public TimeSpan ExecutionDuration
        {
            get
            {
                DateTime start = this.ExecutionStart;
                DateTime end = this.ExecutionEnd;

                if (start == DateTime.MinValue || start == DateTime.MaxValue) return ZeroDuration;
                if (end == DateTime.MaxValue || end < start) end = DateTime.Now;

                return new TimeSpan(end.Ticks - start.Ticks);
            }
        }

        /// <summary>Indicates whether this executable object records its start and end times</summary>
        public bool IsTimingEnabled { get { return true; } }

        /// <summary>Gets a descriptive message about this executable object</summary>
        /// <param name="executable">This executable object</param>
        /// <returns>A string representation of this executable</returns>
        public sealed override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.GetType().FullName).Append(" [").Append(this.ExecutionStatus).Append("]");

            DateTime start = this.ExecutionStart;
            DateTime end = this.ExecutionEnd;
            if (start != DateTime.MinValue && start != DateTime.MaxValue)
            {
                sb.Append(" ").Append(start.ToString(TimestampFormat));
                if (end != DateTime.MinValue && end != DateTime.MaxValue)
                {
                    sb.Append(" -> ").Append(start.ToString(TimestampFormat));
                    sb.Append(" (").Append((((double)(end.Ticks - start.Ticks)) / 10000.0D)).Append(" ms)");
                }
            }

            Exception e = this.Exception;
            if (e != null)
                sb.Append(Environment.NewLine).Append(e.GetType().FullName + ": " + e.Message);

            return sb.ToString();
        }

        #region Abstract members

        /// <summary>Gets the executable type for this executable</summary>
        public abstract ExecutableType ExecutableType { get; }

        /// <summary>Indicates whether this executable object writes to an in-memory log</summary>
        public abstract bool IsLoggingEnabled { get; }

        /// <summary>Logs a mesage</summary>
        /// <param name="message">The message to log</param>
        protected abstract void Log(string message);

        /// <summary>Logs an exception</summary>
        /// <param name="exception">The exception to log</param>
        protected abstract void Log(Exception exception);

        /// <summary>Gets a unique key for this executable</summary>
        public abstract string Key { get; }

        /// <summary>Any available source code for this executable</summary>
        public abstract string SourceCode { get; }

        /// <summary>Gets the log for this executable</summary>
        /// <param name="clear">If true, the log will be cleared during the operation</param>
        /// <returns>The in-memory log statements generated by the executable, if any</returns>
        public abstract List<string> GetLog(bool clear);

        /// <summary>A factory method to create a new executable copy of this object, essentially using this as a template. 
        /// Note that executables are desiged to be lightweight-to-create and thread-safe, but may save state related to timing and logging; 
        /// copying avoids lock contention during execution</summary>
        /// <returns>A new copy of this executable</returns>
        public abstract IExecutable Copy();

        /// <summary>Readies this intance for execution</summary>
        protected abstract void Initialize();

        #endregion

        #endregion

        #region Execution signatures

        #region Basic

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholders">Numbered placeholder values</param>
        /// <param name="parameters">Key-value pairs of parameters</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        protected abstract object ExecuteImpl(object context, IList<dynamic> placeholders, IDictionary<string, dynamic> parameters, object defaultValue = null);

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholders">Numbered placeholder values</param>
        /// <param name="parameters">Key-value pairs of parameters</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private object Execute(object context, IList<dynamic> placeholders, IDictionary<string, dynamic> parameters, object defaultValue = null)
        {
            if (placeholders == null) placeholders = new List<dynamic>();
            if (parameters == null) parameters = new Dictionary<string, dynamic>();

            object returnValue = defaultValue;
            
            if (this.IsLoggingEnabled)
            {
                lock (monitor) 
                {
                    Initialize();
                    this.ExecutionStatus = ExecutionStatus.Ready;
                    this.Log("Starting...");
                    this.Exception = null;
                    this.ExecutionStart = DateTime.Now;
                    this.ExecutionEnd = DateTime.MaxValue;
                    this.ExecutionStatus = ExecutionStatus.Started;

                    try 
                    {
                        returnValue = ExecuteImpl(context, placeholders, parameters, defaultValue);
                        this.ExecutionStatus = ExecutionStatus.Stopped;
                        this.Log("Stopped");
                        this.ExecutionEnd = DateTime.Now;
                    } catch (Exception e) {
                        this.ExecutionStatus = ExecutionStatus.Exception;
                        this.Log("Stopped");
                        this.Exception = e;
                        this.Log(e);
                        this.ExecutionEnd = DateTime.Now;
                    }
                }
            }
            else
            {
                lock (monitor) 
                {
                    this.ExecutionStatus = ExecutionStatus.Ready;
                    this.ExecutionStart = DateTime.Now;
                    this.Exception = null;
                    this.ExecutionEnd = DateTime.MaxValue;
                    this.ExecutionStatus = ExecutionStatus.Started;

                    try 
                    {
                        returnValue = ExecuteImpl(context, placeholders, parameters, defaultValue);
                        this.ExecutionStatus = ExecutionStatus.Stopped;
                        this.ExecutionEnd = DateTime.Now;
                    } catch (Exception e) {
                        this.ExecutionStatus = ExecutionStatus.Exception;
                        this.Exception = e;
                        this.ExecutionEnd = DateTime.Now;
                    }
                }
            }

            return returnValue;
        }

        /// <summary>Executes this executable</summary>
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholders">Numbered placeholder values</param>
        /// <param name="parameters">Key-value pairs of parameters</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private T Execute<T>(object context, IList<dynamic> placeholders, IDictionary<string, dynamic> parameters = null, T defaultValue = default(T)) 
        {
            object returnValue = this.Execute(context, placeholders, parameters, defaultValue);
            if (returnValue is T)
                return (T)returnValue;
            else
                return default(T);
        }

        /// <summary>Executes this executable</summary>
        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, object defaultValue = null)
        {
            return this.Execute(context, null, null, defaultValue);
        }

        #endregion

        #region Placeholders

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholders">Numbered placeholder values</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, IEnumerable<object> placeholders, object defaultValue = null)
        {
            return this.Execute(context, placeholders.ToArray<dynamic>(), null, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, object placeholder0, object defaultValue = null)
        {
            return this.Execute(context, new dynamic[] { placeholder0 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="placeholder1">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, object placeholder0, object placeholder1, object defaultValue = null)
        {
            return this.Execute(context, new dynamic[] { placeholder0, placeholder1 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="placeholder1">A numbered placeholder value</param>
        /// <param name="placeholder2">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, object placeholder0, object placeholder1, object placeholder2, object defaultValue = null)
        {
            return this.Execute(context, new dynamic[] { placeholder0, placeholder1, placeholder2 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="placeholder1">A numbered placeholder value</param>
        /// <param name="placeholder2">A numbered placeholder value</param>
        /// <param name="placeholder3">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, object placeholder0, object placeholder1, object placeholder2, object placeholder3, object defaultValue = null)
        {
            return this.Execute(context, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="placeholder1">A numbered placeholder value</param>
        /// <param name="placeholder2">A numbered placeholder value</param>
        /// <param name="placeholder3">A numbered placeholder value</param>
        /// <param name="placeholder4">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, object placeholder0, object placeholder1, object placeholder2, object placeholder3, object placeholder4, object defaultValue = null)
        {
            return this.Execute(context, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3, placeholder4 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholders">Numbered placeholder values</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, IEnumerable<object> placeholders, T defaultValue = default(T))
        {
            return this.Execute(context, placeholders.ToArray<dynamic>(), null, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, object placeholder0, object defaultValue = null)
        {
            return this.Execute<T>(context, new dynamic[] { placeholder0 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="placeholder1">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, object placeholder0, object placeholder1, object defaultValue = null)
        {
            return this.Execute<T>(context, new dynamic[] { placeholder0, placeholder1 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="placeholder1">A numbered placeholder value</param>
        /// <param name="placeholder2">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, object placeholder0, object placeholder1, object placeholder2, object defaultValue = null)
        {
            return this.Execute<T>(context, new dynamic[] { placeholder0, placeholder1, placeholder2 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="placeholder1">A numbered placeholder value</param>
        /// <param name="placeholder2">A numbered placeholder value</param>
        /// <param name="placeholder3">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, object placeholder0, object placeholder1, object placeholder2, object placeholder3, object defaultValue = null)
        {
            return this.Execute<T>(context, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3 }, null, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="placeholder0">A numbered placeholder value</param>
        /// <param name="placeholder1">A numbered placeholder value</param>
        /// <param name="placeholder2">A numbered placeholder value</param>
        /// <param name="placeholder3">A numbered placeholder value</param>
        /// <param name="placeholder4">A numbered placeholder value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, object placeholder0, object placeholder1, object placeholder2, object placeholder3, object placeholder4, object defaultValue = null)
        {
            return this.Execute<T>(context, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3, placeholder4 }, null, defaultValue);
        }

        #endregion Placeholders

        #region Named parameters

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameters">Parameter name-value pairs</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, IDictionary<string, object> parameters, object defaultValue = null)
        {
            return this.Execute(context, null, parameters, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, string parameterName0, object parameterValue0, object defaultValue = null)
        {
            return this.Execute(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="parameterName1">A parameter name</param>
        /// <param name="parameterValue1">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, object defaultValue = null)
        {
            return this.Execute(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="parameterName1">A parameter name</param>
        /// <param name="parameterValue1">A parameter value</param>
        /// <param name="parameterName2">A parameter name</param>
        /// <param name="parameterValue2">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, object defaultValue = null)
        {
            return this.Execute(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="parameterName1">A parameter name</param>
        /// <param name="parameterValue1">A parameter value</param>
        /// <param name="parameterName2">A parameter name</param>
        /// <param name="parameterValue2">A parameter value</param>
        /// <param name="parameterName3">A parameter name</param>
        /// <param name="parameterValue3">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, object defaultValue = null)
        {
            return this.Execute(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="parameterName1">A parameter name</param>
        /// <param name="parameterValue1">A parameter value</param>
        /// <param name="parameterName2">A parameter name</param>
        /// <param name="parameterValue2">A parameter value</param>
        /// <param name="parameterName3">A parameter name</param>
        /// <param name="parameterValue3">A parameter value</param>
        /// <param name="parameterName4">A parameter name</param>
        /// <param name="parameterValue4">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public object Execute(object context, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, string parameterName4, object parameterValue4, object defaultValue = null)
        {
            return this.Execute(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 }, { parameterName4, parameterValue4 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameters">Parameter name-value pairs</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, IDictionary<string, object> parameters, T defaultValue = default(T))
        {
            return this.Execute<T>(context, null, parameters, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, string parameterName0, object parameterValue0, T defaultValue = default(T))
        {
            return this.Execute<T>(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="parameterName1">A parameter name</param>
        /// <param name="parameterValue1">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, T defaultValue = default(T))
        {
            return this.Execute<T>(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="parameterName1">A parameter name</param>
        /// <param name="parameterValue1">A parameter value</param>
        /// <param name="parameterName2">A parameter name</param>
        /// <param name="parameterValue2">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, T defaultValue = default(T))
        {
            return this.Execute<T>(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="parameterName1">A parameter name</param>
        /// <param name="parameterValue1">A parameter value</param>
        /// <param name="parameterName2">A parameter name</param>
        /// <param name="parameterValue2">A parameter value</param>
        /// <param name="parameterName3">A parameter name</param>
        /// <param name="parameterValue3">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, T defaultValue = default(T))
        {
            return this.Execute<T>(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 } }, defaultValue);
        }

        /// <summary>Executes this executable</summary>
        /// <typeparam name="T">The type to return</typeparam>        
        /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
        /// <param name="parameterName0">A parameter name</param>
        /// <param name="parameterValue0">A parameter value</param>
        /// <param name="parameterName1">A parameter name</param>
        /// <param name="parameterValue1">A parameter value</param>
        /// <param name="parameterName2">A parameter name</param>
        /// <param name="parameterValue2">A parameter value</param>
        /// <param name="parameterName3">A parameter name</param>
        /// <param name="parameterValue3">A parameter value</param>
        /// <param name="parameterName4">A parameter name</param>
        /// <param name="parameterValue4">A parameter value</param>
        /// <param name="defaultValue">The default value to return</param>
        /// <returns>A return value from the executable, or the default</returns>
        public T Execute<T>(object context, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, string parameterName4, object parameterValue4, T defaultValue = default(T))
        {   
            return this.Execute<T>(context, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 }, { parameterName4, parameterValue4 } }, defaultValue);
        }

        #endregion

        #endregion
    }
}
