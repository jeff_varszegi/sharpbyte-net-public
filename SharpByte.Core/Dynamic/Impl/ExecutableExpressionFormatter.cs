﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace SharpByte.Dynamic.Impl
{
    /// <summary>Formats dynamically executable expressions for compilation</summary>
    public sealed class ExecutableExpressionFormatter: ExecutableFormatter
    {
        /// <summary>A regular expression for capturing numbered placeholders</summary>
        private const string NumberedPlaceholderRegex = "{{{[0]}}}|{{{[1-9][0-9]*}}}";

        /// <summary>Default namespaces</summary>
        private static string DefaultNamespaces;

        /// <summary>A unique key prefix</summary>
        private static string KeyPrefix = ExecutableType.Expression.ToString() + "|";

        /// <summary>Prepares the class for use</summary>
        static ExecutableExpressionFormatter()
        {
            StringBuilder sb = new StringBuilder();

            foreach (string ns in Constants.DefaultExpressionNamespaces.OrderBy(ns => ns))
                sb.Append(FormatUsingDirective(ns)).Append(Environment.NewLine);

            DefaultNamespaces = sb.ToString();
        }

        /// <summary>Formats a namespace as a using directive</summary>
        /// <param name="ns">The namespace to format</param>
        /// <returns>A formatted using directive</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string FormatUsingDirective(string ns)
        {
            if (string.IsNullOrWhiteSpace(ns)) return "";

            if (!ns.StartsWith("using "))
            {
                if (!ns.EndsWith(";"))
                {
                    return "using " + ns + ";";
                }
                else
                {
                    return "using " + ns;
                }
            }
            else if (!ns.EndsWith(";"))
            {
                return ns + ";";
            }
            else
            {
                return ns;
            }
        }

/* The original code template:
@"{{{UsingDirectives}}}

namespace SharpByte.Dynamic
{
    public sealed class Executable{{{TypeID}}} : SharpByte.Dynamic.Impl.ExecutableExpression
    {
        public override string SourceCode { get { return @""{{{SourceEscaped}}}""; }}
        public override string Key { get { return @""{{{KeyEscaped}}}""; }}

        public Executable{{{TypeID}}}() : base() {}

        protected sealed override object ExecuteImpl(dynamic this{{{TypeID}}}, IList<dynamic> ph{{{TypeID}}}, IDictionary<string, dynamic> p{{{TypeID}}}, object df{{{TypeID}}} = null) 
        {   
            return ({{{Source}}});
        }

        public override IExecutable Copy() { return new Executable{{{TypeID}}}(); }
    }
}";
*/

        /// <summary>Formats source code for compilation</summary>
        /// <param name="source">The source to format</param>
        /// <param name="parameterNames">Named parameters for the source</param>
        /// <param name="usingNamespaces">Namespaces to add</param>
        /// <returns>Formatted source code</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override ClassTemplate Format(string source, IEnumerable<string> parameterNames, IEnumerable<string> usingNamespaces)
        {
            if (string.IsNullOrWhiteSpace(source)) throw new ArgumentException("Invalid (null or white-space) source");

            string typeId = Guid.NewGuid().ToString("N").Substring(0, 16);
            string className = "SharpByte.Dynamic.Executable" + typeId;
            string placeholderPrefix = "ph" + typeId;
            string parameterPrefix = "p" + typeId;
            string sourceEscaped = source.Replace("\"", "\"\"");
            string keyEscaped =  
                parameterNames == null ? 
                KeyPrefix + "|" + sourceEscaped :
                KeyPrefix + string.Join(",", parameterNames.OrderBy(p => p)) + "|" + sourceEscaped;

            if (source.IndexOf("this") > -1)
                source = source.ReplacePattern("\\bthis\\b", "this" + typeId, true);

            if (source.IndexOf("{{{0}}}") > -1)
                foreach (string placeholder in source.FindAll(NumberedPlaceholderRegex, true).Distinct())
                    source = source.Replace(placeholder, placeholderPrefix + "[" + placeholder.Trim('{', '}') + "]");

            if (parameterNames != null)
                foreach (string parameter in parameterNames)
                    source = source.ReplacePattern("\\b" + parameter + "\\b", parameterPrefix + "[\"" + parameter + "\"]", true);

            StringBuilder output = new StringBuilder(DefaultNamespaces, source.Length + 1200);
            
            if (usingNamespaces != null)
                foreach(string ns in usingNamespaces)
                    if (!Constants.DefaultScriptNamespaces.Contains(ns))
                        output.Append(FormatUsingDirective(ns)).Append(Environment.NewLine);
            
            output.Append(
@"namespace SharpByte.Dynamic {
	public sealed class Executable").Append(typeId).Append(@" : SharpByte.Dynamic.Impl.ExecutableExpression {
		public override string SourceCode { get { return @""").Append(sourceEscaped).Append(@"""; }}
		public override string Key { get { return @""").Append(keyEscaped).Append(@"""; }}
		public Executable").Append(typeId).Append(@"() : base() {}
		protected sealed override object ExecuteImpl(dynamic this").Append(typeId).Append(@", IList<dynamic> ph").Append(typeId).Append(@", IDictionary<string, dynamic> p").Append(typeId).Append(@", object df").Append(typeId).Append(@" = null) {
			return (").Append(source).Append(@");
			return df").Append(typeId).Append(@";
		}
		public override IExecutable Copy() { return new Executable").Append(typeId).Append(@"(); }
	}
}");

            return new ClassTemplate(className, output.ToString());
        } 
    }
}
