﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SharpByte.Dynamic.Impl
{
    /// <summary>Compiles C# source code, returning a type</summary>
    public class CodeDomCompiler : Compiler
    {
        /// <summary>Constructs a new instance</summary>
        public CodeDomCompiler()
        {
            AddReference("System.dll");
            AddReference("System.Core.dll");
            AddReference("Microsoft.CSharp.dll");
            AddReference("SharpByte.Core.dll");

            // Consider adding further references always used, perhaps configured at application startup
//            AddReference("System.Data.dll");
//            AddReference("System.Net.Http.dll");
//            AddReference("System.Runtime.Caching.dll");
//            AddReference("System.Web.dll");
//            AddReference("System.Web.Extensions.dll");
        }


        /// <summary>Compiles (or retrieves) a type for the specified source code, throwing an exception in case of a compilation failure</summary>
        /// <param name="typeName">The fully-qualified type name to compile</param>
        /// <param name="sourceCode">The code to compile</param>
        /// <param name="assemblyReferences">References to use in compilation</param>
        /// <returns>A compiled type</returns>
        public override Type Compile(string typeName, string sourceCode, IEnumerable<string> assemblyReferences = null)
        {
            CSharpCodeProvider codeProvider = new CSharpCodeProvider();

            CompilerParameters compilerParameters = new CompilerParameters();
            compilerParameters.CompilerOptions = "/optimize";
            compilerParameters.GenerateExecutable = false;
            compilerParameters.GenerateInMemory = true;
            compilerParameters.TreatWarningsAsErrors = false;
            compilerParameters.IncludeDebugInformation = false;

            if (assemblyReferences == null) {                
                compilerParameters.ReferencedAssemblies.AddRange(References.ToArray());
            }
            else
                compilerParameters.ReferencedAssemblies.AddRange(assemblyReferences.Union(References).ToArray());

            CompilerResults compilerResults = codeProvider.CompileAssemblyFromSource(compilerParameters, sourceCode);

            if (compilerResults.Errors.HasErrors)
            {
                string exceptionText = "Compiler error(s): ";

                foreach (CompilerError error in compilerResults.Errors)
                    exceptionText += Environment.NewLine + error.ToString();

                throw new Exception(exceptionText);
            }

            Module module = compilerResults.CompiledAssembly.GetModules()[0];
            Type type = null;

            if (module != null)
                type = module.GetType(typeName);

            return type;
        }
    }
}
