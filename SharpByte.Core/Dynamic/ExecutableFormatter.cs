﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;

using SharpByte.Dynamic.Impl;

namespace SharpByte.Dynamic
{
    /// <summary>Formats dynamically executable statements for compilation</summary>
    public abstract class ExecutableFormatter: IExecutableFormatter
    {
        /// <summary>An executable formatter</summary>
        private static IExecutableFormatter scriptFormatter = new ExecutableScriptFormatter();

        /// <summary>An executable formatter</summary>
        private static IExecutableFormatter statementFormatter = new ExecutableExpressionFormatter();

        /// <summary>A factory method for formatters</summary>
        /// <returns>An appropriate formatter for the specified executable type</returns>
        public static IExecutableFormatter GetExecutableFormatter(ExecutableType executableType)
        {
            switch (executableType)
            {
                case ExecutableType.Script:
                    return scriptFormatter;
                case ExecutableType.Expression:
                    return statementFormatter;                
            }

            throw new InvalidOperationException("Unknown executable type");
        }

        /// <summary>Formats source code for compilation</summary>
        /// <param name="source">The source to format</param>
        /// <param name="parameterNames">Named parameters for the source</param>
        /// <param name="usingNamespaces">Namespaces to add</param>
        /// <returns>Formatted source code</returns>
        public abstract ClassTemplate Format(string source, IEnumerable<string> parameterNames, IEnumerable<string> usingNamespaces);
    }
}
