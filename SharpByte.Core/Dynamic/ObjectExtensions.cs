﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Runtime.CompilerServices;

#if (COLOCATE_EXTENSIONS || GLOBAL_EXTENSIONS)
using SharpByte.Dynamic;
#endif

#if COLOCATE_EXTENSIONS
namespace System.Runtime.CompilerServices
{
#elif !GLOBAL_EXTENSIONS
namespace SharpByte.Dynamic
{
#endif

/// <summary>Contains utility/extension logic for objects</summary>
public static partial class ObjectExtensions
{
    #region Extended data properties/caching

    #region Inner types

    /// <summary>Contains extended pseudo-property and memory-cached data for an object</summary>
    private sealed class ExtendedData : IDisposable
    {
        /// <summary>The owner of this extended data</summary>
        private object owner;

        /// <summary>Key-value pairs of extended data for an object</summary>
        private ConcurrentDictionary<string, object> properties;

        /// <summary>Key-value pairs of extended data for an object</summary>
        public ConcurrentDictionary<string, object> Properties
        {
            get
            {
                if (properties == null)
                    lock (this)
                        if (properties == null)
                            properties = new ConcurrentDictionary<string, object>();

                return properties;
            }
        }

        /// <summary>Provides local memory caching facilities for an object</summary>
        private MemoryCache cache;

        /// <summary>Provides local memory caching facilities for an object</summary>
        public MemoryCache Cache
        {
            get
            {
                if (cache == null)
                    lock (this)
                        if (cache == null)
                            cache = new MemoryCache(Guid.NewGuid().ToString("N"));
                return cache;
            }
        }

        /// <summary>Default constructor</summary>
        private ExtendedData() { }

        /// <summary>Constructs a new instance</summary>
        /// <param name="_owner">The object for which to store extended data</param>
        public ExtendedData(object _owner)
        {
            owner = _owner;
        }

        /// <summary>Releases this object's resources</summary>
        public void Dispose()
        {
            try { if (cache != null) cache.Dispose(); } catch { }
        }
    }

    #endregion Inner types

    /// <summary>A global object used for fallback values</summary>
    private static object globalObject = new object();

    ///<summary>Stores extended data for objects</summary>
    private static ConditionalWeakTable<object, object> extendedDataMap = new ConditionalWeakTable<object, object>();

    /// <summary>
    /// Creates extended data for an object
    /// </summary>
    /// <param name="o">The object to receive the tacked-on data values</param>
    /// <returns>A new dictionary</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static ExtendedData CreateExtendedData(object o)
    {
        return new ExtendedData(o);
    }

    /// <summary>
    /// Sets an extended pseudo-property value on this object
    /// </summary>
    /// <param name="owner">this object</param>
    /// <param name="name">The pseudo-property name</param>
    /// <param name="value">The value to set (if null, any value for the name will be removed)</param>
    /// <param name="expirationMilliseconds">The number of milliseconds to cache the value</param>
    /// <param name="isExpirationSliding">If true, sliding cache expiration will be used; if false, absolute</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void SetProperty(this object owner, string name, object value, long expirationMilliseconds = long.MaxValue, bool isExpirationSliding = true)
    {
        if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Invalid (null or whitespace) name");

        if (expirationMilliseconds <= 0)
            expirationMilliseconds = long.MaxValue;

        if (expirationMilliseconds == long.MaxValue)
        {
            IDictionary<string, object> properties = ((ExtendedData)extendedDataMap.GetValue(owner, CreateExtendedData)).Properties;

            if (value != null)
                properties[name] = value;
            else
                properties.Remove(name);
        }
        else
        {
            MemoryCache cache = ((ExtendedData)extendedDataMap.GetValue(owner, CreateExtendedData)).Cache;
            cache.Set(name, value, expirationMilliseconds, isExpirationSliding);
        }
    }

    /// <summary>
    /// Gets a pseudo-property value stored for this object
    /// </summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="o">this object</param>
    /// <param name="name">The pseudo-property name</param>
    /// <param name="defaultValue">The default value to return if the key/name is not found</param>
    /// <param name="fallBackStatic">If true, static/type extended properties will be checked</param>
    /// <param name="fallBackGlobal">If true, the global extended properties will be checked after static</param>
    /// <returns>A value of the indicated type, or the type default if not found or of a different type</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T GetProperty<T>(this object o, string name, T defaultValue = default(T), bool fallBackStatic = false, bool fallBackGlobal = false)
    {
        if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Invalid (null or whitespace) name");

        ExtendedData extendedData = (ExtendedData)extendedDataMap.GetValue(o, CreateExtendedData);
        object value = null;

        if (extendedData.Properties.TryGetValue(name, out value))
        {
            if (value is T)
                return (T)value;
            else
                return default(T);
        }

        value = extendedData.Cache.Get(name);
        if (value != null)
        {
            if (value is T)
                return (T)value;
            else
                return defaultValue;
        }

        if (fallBackStatic)
        {
            T tValue;

            tValue = o.GetType().GetProperty<T>(name, default(T), false, false);
            if (!object.ReferenceEquals(tValue, default(T)))
                return tValue;
        }

        if (fallBackStatic)
        {
            T tValue;

            tValue = globalObject.GetProperty<T>(name, default(T), false, false);
            if (!object.ReferenceEquals(tValue, default(T)))
                return tValue;
        }

        return defaultValue;
    }


    /// <summary>
    /// Gets a pseudo-property value stored for this object
    /// </summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="o">this object</param>
    /// <param name="name">The pseudo-property name</param>
    /// <param name="fallBackStatic">If true, static/type extended properties will be checked</param>
    /// <param name="fallBackGlobal">If true, the global extended properties will be checked after static</param>
    /// <returns>A value of the indicated type, or the type default if not found or of a different type</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T GetProperty<T>(this object o, string name, bool fallBackStatic = false, bool fallBackGlobal = false)
    {
        if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Invalid (null or whitespace) name");

        ExtendedData extendedData = (ExtendedData)extendedDataMap.GetValue(o, CreateExtendedData);
        object value = null;

        if (extendedData.Properties.TryGetValue(name, out value))
        {
            if (value is T)
                return (T)value;
            else
                return default(T);
        }

        value = extendedData.Cache.Get(name);
        if (value != null)
        {
            if (value is T)
                return (T)value;
            else
                return default(T);
        }

        if (fallBackStatic)
        {
            T tValue;

            tValue = o.GetType().GetProperty<T>(name, default(T), false, false);
            if (!object.ReferenceEquals(tValue, default(T)))
                return tValue;
        }

        if (fallBackGlobal)
        {
            T tValue;

            tValue = globalObject.GetProperty<T>(name, default(T), false, false);
            if (!object.ReferenceEquals(tValue, default(T)))
                return tValue;
        }

        return default(T);
    }

    /// <summary>
    /// Gets a pseudo-property value stored for this object
    /// </summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="o">this object</param>
    /// <param name="name">The pseudo-property name</param>
    /// <param name="defaultValue">The default value to return if the key/name is not found</param>
    /// <param name="fallBackStatic">If true, static/type extended properties will be checked</param>
    /// <param name="fallBackGlobal">If true, the global extended properties will be checked after static</param>
    /// <returns>A value of the indicated type, or the type default if not found or of a different type</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static object GetProperty(this object o, string name, object defaultValue = null, bool fallBackStatic = false, bool fallBackGlobal = false)
    {
        if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Invalid (null or whitespace) name");

        ExtendedData extendedData = (ExtendedData)extendedDataMap.GetValue(o, CreateExtendedData);
        object value = null;

        if (extendedData.Properties.TryGetValue(name, out value))
        {
            if (value != null)
                return value;
        }

        value = extendedData.Cache.Get(name);
        if (value != null)
            return value;

        if (fallBackStatic)
        {
            value = o.GetType().GetProperty(name, false, false);
            if (value != null)
                return value;
        }

        if (fallBackGlobal)
        {
            value = globalObject.GetProperty(name, false, false);
            if (value != null)
                return value;
        }

        return defaultValue;
    }

    /// <summary>
    /// Gets a pseudo-property value stored for this object
    /// </summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="o">this object</param>
    /// <param name="name">The pseudo-property name</param>
    /// <param name="fallBackStatic">If true, static/type extended properties will be checked</param>
    /// <param name="fallBackGlobal">If true, the global extended properties will be checked after static</param>
    /// <returns>A value of the indicated type, or the type default if not found or of a different type</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static object GetProperty(this object o, string name, bool fallBackStatic = false, bool fallBackGlobal = false)
    {
        if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Invalid (null or whitespace) name");

        ExtendedData extendedData = (ExtendedData)extendedDataMap.GetValue(o, CreateExtendedData);
        object value = null;

        if (extendedData.Properties.TryGetValue(name, out value))
        {
            if (value != null)
                return value;
        }

        value = extendedData.Cache.Get(name);
        if (value != null)
            return value;

        if (fallBackStatic)
        {
            value = o.GetType().GetProperty(name, false, false);
            if (value != null)
                return value;
        }

        if (fallBackGlobal)
        {
            value = globalObject.GetProperty(name, false, false);
            if (value != null)
                return value;
        }

        return null;
    }

    /// <summary>Sets the specified property statically, i.e. on the type</summary>
    /// <param name="owner">this object</param>
    /// <param name="key">The cache key to use</param>
    /// <param name="value">The object to cache; if null, any value for the key will be removed</param>
    /// <param name="expirationMilliseconds">The number of milliseconds to cache the item</param>
    /// <param name="isExpirationSliding">If true, sliding expiration will be used; if false, absolute</param>
    public static void SetPropertyStatic(this object owner, string key, object value, long expirationMilliseconds = long.MaxValue, bool isExpirationSliding = true)
    {
        owner.GetType().SetProperty(key, value, expirationMilliseconds, isExpirationSliding);
    }

    /// <summary>Sets the specified property globally</summary>
    /// <param name="owner">this object</param>
    /// <param name="key">The cache key to use</param>
    /// <param name="value">The object to cache; if null, any value for the key will be removed</param>
    /// <param name="expirationMilliseconds">The number of milliseconds to cache the item</param>
    /// <param name="isExpirationSliding">If true, sliding expiration will be used; if false, absolute</param>
    public static void SetPropertyGlobal(this object owner, string key, object value, long expirationMilliseconds = long.MaxValue, bool isExpirationSliding = true)
    {
        globalObject.SetProperty(key, value, expirationMilliseconds, isExpirationSliding);
    }

    #endregion Extended data properties/caching

    #region Dynamic execution

    #region Execution signatures

    #region Basic
		
    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="executableType">The type of executable code</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="placeholders">Numbered placeholder values</param>
    /// <param name="parameters">Key-value pairs of parameters</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static object Execute(object context, ExecutableType executableType, string sourceCode, IEnumerable<string> usingNamespaces, IList<dynamic> placeholders, IDictionary<string, dynamic> parameters, object defaultValue = null)
    {
        IExecutable executable = ExecutableFactory.Default.GetExecutable(executableType, sourceCode, parameters == null ? null : parameters.Keys, usingNamespaces);
        if (executable == null) throw new InvalidOperationException("Cannot execute specified text with specified parameters");
        return executable.Execute(context, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="executableType">The type of executable code</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="placeholders">Numbered placeholder values</param>
    /// <param name="parameters">Key-value pairs of parameters</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static T Execute<T>(object context, ExecutableType executableType, string sourceCode, IEnumerable<string> usingNamespaces, IList<dynamic> placeholders, IDictionary<string, dynamic> parameters, object defaultValue = null) 
    {
        object returnValue = Execute(context, executableType, sourceCode, usingNamespaces, placeholders, parameters, defaultValue);
        if (returnValue is T)
            return (T)returnValue;
        else
            return default(T);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="executable">this executable</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, IEnumerable<string> usingNamespaces, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, usingNamespaces, null, null, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, null, null, null, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, IEnumerable<string> usingNamespaces, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, usingNamespaces, null, null, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, null, null, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string sourceCode, IEnumerable<string> usingNamespaces, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, sourceCode, usingNamespaces, null, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="expression">The expression to evaluate</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, null, null, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string sourceCode, IEnumerable<string> usingNamespaces, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, sourceCode, usingNamespaces, null, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="expression">The expression to evaluate</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, null, null, null, defaultValue);
    }

    #endregion

    #region Placeholders

	#region No namespaces supplied
	
    /// <summary>Evaluates the specified expression</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholders">Numbered placeholder values</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, IEnumerable<object> placeholders,  object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, null, placeholders.ToArray<dynamic>(), null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0,  object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0, object placeholder1,  object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0, placeholder1 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0, object placeholder1, object placeholder2,  object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0, placeholder1, placeholder2 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="placeholder3">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0, object placeholder1, object placeholder2, object placeholder3,  object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="placeholder3">A numbered placeholder value</param>
    /// <param name="placeholder4">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0, object placeholder1, object placeholder2, object placeholder3, object placeholder4,  object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3, placeholder4 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholders">Numbered placeholder values</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, IEnumerable<object> placeholders, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, null, placeholders.ToArray<dynamic>(), null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0,  T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, object placeholder1,  T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0, placeholder1 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, object placeholder1, object placeholder2,  T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0, placeholder1, placeholder2 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="placeholder3">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, object placeholder1, object placeholder2, object placeholder3,  T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="placeholder3">A numbered placeholder value</param>
    /// <param name="placeholder4">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, object placeholder1, object placeholder2, object placeholder3, object placeholder4,  T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, null, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3, placeholder4 }, null, defaultValue);
    }
	
	#endregion No namespaces supplied
	
	#region With namespaces supplied
	
    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholders">Numbered placeholder values</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, IEnumerable<object> placeholders,  IEnumerable<string> usingNamespaces, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, usingNamespaces, placeholders.ToArray<dynamic>(), null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0,  IEnumerable<string> usingNamespaces, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0, object placeholder1,  IEnumerable<string> usingNamespaces, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0, placeholder1 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0, object placeholder1, object placeholder2,  IEnumerable<string> usingNamespaces, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0, placeholder1, placeholder2 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="placeholder3">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0, object placeholder1, object placeholder2, object placeholder3,  IEnumerable<string> usingNamespaces, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <param name="executable">this executable</param>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="placeholder3">A numbered placeholder value</param>
    /// <param name="placeholder4">A numbered placeholder value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Evaluate(this object context, string expression, object placeholder0, object placeholder1, object placeholder2, object placeholder3, object placeholder4,  IEnumerable<string> usingNamespaces, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3, placeholder4 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholders">Numbered placeholder values</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, IEnumerable<object> placeholders, IEnumerable<string> usingNamespaces, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, usingNamespaces, placeholders.ToArray<dynamic>(), null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, IEnumerable<string> usingNamespaces, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, object placeholder1,  IEnumerable<string> usingNamespaces, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0, placeholder1 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, object placeholder1, object placeholder2,  IEnumerable<string> usingNamespaces, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0, placeholder1, placeholder2 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="placeholder3">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, object placeholder1, object placeholder2, object placeholder3,  IEnumerable<string> usingNamespaces, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3 }, null, defaultValue);
    }

    /// <summary>Evaluates the specified expression</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="expression">The code to execute</param>
    /// <param name="placeholder0">A numbered placeholder value</param>
    /// <param name="placeholder1">A numbered placeholder value</param>
    /// <param name="placeholder2">A numbered placeholder value</param>
    /// <param name="placeholder3">A numbered placeholder value</param>
    /// <param name="placeholder4">A numbered placeholder value</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Evaluate<T>(this object context, string expression, object placeholder0, object placeholder1, object placeholder2, object placeholder3, object placeholder4,  IEnumerable<string> usingNamespaces, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Expression, expression, usingNamespaces, new dynamic[] { placeholder0, placeholder1, placeholder2, placeholder3, placeholder4 }, null, defaultValue);
    }

    #endregion With namespaces supplied

    #endregion Placeholders

    #region Named parameters

    #region With namespaces supplied

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameters">Parameter name-value pairs</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, IEnumerable<string> usingNamespaces, IDictionary<string, object> parameters, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, usingNamespaces, null, parameters, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="parameterName3">A parameter name</param>
    /// <param name="parameterValue3">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="parameterName3">A parameter name</param>
    /// <param name="parameterValue3">A parameter value</param>
    /// <param name="parameterName4">A parameter name</param>
    /// <param name="parameterValue4">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, string parameterName4, object parameterValue4, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 }, { parameterName4, parameterValue4 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameters">Parameter name-value pairs</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, IEnumerable<string> usingNamespaces, IDictionary<string, object> parameters, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, usingNamespaces, null, parameters, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="parameterName3">A parameter name</param>
    /// <param name="parameterValue3">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="usingNamespaces">Namespaces to add in compilation</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="parameterName3">A parameter name</param>
    /// <param name="parameterValue3">A parameter value</param>
    /// <param name="parameterName4">A parameter name</param>
    /// <param name="parameterValue4">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, IEnumerable<string> usingNamespaces, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, string parameterName4, object parameterValue4, T defaultValue = default(T))
    {   
        return Execute<T>(context, ExecutableType.Script, sourceCode, usingNamespaces, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 }, { parameterName4, parameterValue4 } }, defaultValue);
    }

    #endregion

    #region Without namespaces

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameters">Parameter name-value pairs</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, IDictionary<string, object> parameters, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, null, null, parameters, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, string parameterName0, object parameterValue0, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="parameterName3">A parameter name</param>
    /// <param name="parameterValue3">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="parameterName3">A parameter name</param>
    /// <param name="parameterValue3">A parameter value</param>
    /// <param name="parameterName4">A parameter name</param>
    /// <param name="parameterValue4">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static object Execute(this object context, string sourceCode, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, string parameterName4, object parameterValue4, object defaultValue = null)
    {
        return Execute(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 }, { parameterName4, parameterValue4 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameters">Parameter name-value pairs</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, IDictionary<string, object> parameters, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, null, null, parameters, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, string parameterName0, object parameterValue0, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="parameterName3">A parameter name</param>
    /// <param name="parameterValue3">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, T defaultValue = default(T))
    {
        return Execute<T>(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 } }, defaultValue);
    }

    /// <summary>Runs the specified script</summary>
    /// <typeparam name="T">The type to return</typeparam>
    /// <param name="context">The context in which to execute the executable (corresponding to 'this' in source code)</param>
    /// <param name="sourceCode">The code to execute</param>
    /// <param name="parameterName0">A parameter name</param>
    /// <param name="parameterValue0">A parameter value</param>
    /// <param name="parameterName1">A parameter name</param>
    /// <param name="parameterValue1">A parameter value</param>
    /// <param name="parameterName2">A parameter name</param>
    /// <param name="parameterValue2">A parameter value</param>
    /// <param name="parameterName3">A parameter name</param>
    /// <param name="parameterValue3">A parameter value</param>
    /// <param name="parameterName4">A parameter name</param>
    /// <param name="parameterValue4">A parameter value</param>
    /// <param name="defaultValue">The default value to return</param>
    /// <returns>A return value from the executable, or the default</returns>
    public static T Execute<T>(this object context, string sourceCode, string parameterName0, object parameterValue0, string parameterName1, object parameterValue1, string parameterName2, object parameterValue2, string parameterName3, object parameterValue3, string parameterName4, object parameterValue4, T defaultValue = default(T))
    {   
        return Execute<T>(context, ExecutableType.Script, sourceCode, null, null, new Dictionary<string, object>() { { parameterName0, parameterValue0 }, { parameterName1, parameterValue1 }, { parameterName2, parameterValue2 }, { parameterName3, parameterValue3 }, { parameterName4, parameterValue4 } }, defaultValue);
    }

    #endregion

    #endregion Named parameters

    #endregion

    #endregion Dynamic execution
}

#if (!COLOCATE_EXTENSIONS && !GLOBAL_EXTENSIONS)
}
#endif