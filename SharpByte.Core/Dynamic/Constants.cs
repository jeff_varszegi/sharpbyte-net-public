﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System.Collections.Generic;
using SharpByte.Collections;

namespace SharpByte.Dynamic
{
    /// <summary>Constants related to dynamic compilation</summary>
    public static class Constants
    {
        /// <summary>C# reserved words, which should not be used in executable source code as variable names</summary>
        public static readonly ISet<string> CSharpReservedWords = new ReadOnlySet<string>(new HashSet<string>() {
            "abstract", "add", "alias", "as", "ascending", "async", "await", "base", "bool", "break", 
            "byte", "case", "catch", "char", "checked", "class", "const", "continue", "decimal", "default", 
            "delegate", "descending", "do", "double", "dynamic", "else", "enum", "event", "explicit", "extern", 
            "false", "finally", "fixed", "float", "for", "foreach", "from", "get", "global", "goto", 
            "group", "if", "implicit", "in", "int", "interface", "internal", "into", "is", "join", 
            "let", "lock", "long", "nameof", "namespace", "new", "null", "object", "operator", "orderby", 
            "out", "override", "params", "partial", "private", "protected", "public", "readonly", "ref", "remove", 
            "return", "sbyte", "sealed", "select", "set", "short", "sizeof", "stackalloc", "static", "string", 
            "struct", "switch", "this", "throw", "true", "try", "typeof", "uint", "ulong", "unchecked", 
            "unsafe", "ushort", "using", "value", "var", "virtual", "void", "volatile", "when", "where", 
            "while", "yield"
        });

        /// <summary>Default namespaces, included in all dynamic script compilations</summary>
        public static readonly ISet<string> DefaultScriptNamespaces = new HashSet<string>() {
            "System",
            "System.Collections",
            "System.Collections.Concurrent",
            "System.Collections.Generic",
            "System.Collections.Specialized",
            "System.Configuration",
            "System.IO",
            "System.Linq",
            "System.Linq.Expressions",
            "System.Text",
            "System.Text.RegularExpressions",
            "System.Threading",
            "System.Threading.Tasks"
        };

        /// <summary>Default namespaces, included in all dynamic expression compilations</summary>
        public static readonly ISet<string> DefaultExpressionNamespaces = new HashSet<string>() {
            "System",
            "System.Collections",
            "System.Collections.Generic",
            "System.Linq",
            "System.Linq.Expressions",
            "System.Text",
            "System.Text.RegularExpressions"
        };

    }
}
