﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

using SharpByte.Collections.Specialized;

namespace SharpByte.Dynamic
{
    /// <summary>A get-or-create collection of long-lived objects which reduces lock contention</summary>
    public sealed class ExecutableFactory
    {
        /// <summary>Empty, default parameter names</summary>
        private static IEnumerable<string> EmptyParameterNames = new List<string>();

        #region Singleton instance

        /// <summary>A singleton instance</summary>
        private static ExecutableFactory defaultInstance;

        /// <summary>A synchronization locking object</summary>
        private static object defaultInstanceLock = new object();

        /// <summary>Gets a singleton instance</summary>
        public static ExecutableFactory Default
        {
            get 
            {
                if (defaultInstance == null)
                    lock (defaultInstanceLock)
                        if (defaultInstance == null)
                            defaultInstance = new ExecutableFactory();
                return defaultInstance;   
            }
        }

        #endregion

        /// <summary>Inner collections of values, partitioned into separate buckets/stripes to foster greater concurrency</summary>
        private ConcurrentDictionary<string, IExecutable>[] values;

        /// <summary>Used to track whether a bucket has been initialized</summary>
        private object[] locks;

        /// <summary>Used to track whether a bucket has been initialized</summary>
        private bool[] initializationFlags;

        /// <summary>Synchronization objects; for each value's key in the values collection, there is a corresponding locking object in this 
        /// one to prevent double initialization of a particular value due to race conditions</summary>
        private ConcurrentDictionary<string, object>[] valueLocks;

        /// <summary>The minimum partition count to use</summary>
        private const int MinimumPartitionCount = 16;

        /// <summary>The number of partitions/internal collections</summary>
        private int partitionCount;

        /// <summary>Constructs a new instance</summary>
        /// <param name="_partitionCount">The number of partitions to use</param>
        /// <param name="uncompiledStorageEnabled">If true, uncompiled regexes will also be stored</param>
        public ExecutableFactory(int _partitionCount, bool uncompiledStorageEnabled = true)
        {
            partitionCount = Math.Max(_partitionCount, MinimumPartitionCount);

            values = new ConcurrentDictionary<string, IExecutable>[partitionCount];
            valueLocks = new ConcurrentDictionary<string, object>[partitionCount];
            locks = new object[partitionCount];
            for (int x = 0; x < partitionCount; x++)
                locks[x] = new object();
            initializationFlags = new bool[partitionCount];
        }

        /// <summary>Constructs a new instance</summary>
        public ExecutableFactory() : this(128) {}

        /// <summary>Creates an executable for the specified type, source code and parameters</summary>
        /// <param name="executableType">The type of executable to get</param>
        /// <param name="sourceCode">The source code for the executable, which needs reformatting into source code for a class</param>
        /// <param name="parameterNames">The parameter names for the executable</param>
        /// <returns>An executable for the specified combination of type, source code and parameters</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private IExecutable CreateExecutable(ExecutableType executableType, string sourceCode, IEnumerable<string> parameterNames, IEnumerable<string> usingNamespaces)
        {
            ClassTemplate classTemplate = ExecutableFormatter.GetExecutableFormatter(executableType).Format(sourceCode, parameterNames, usingNamespaces);
            Type type = Compiler.Default.Compile(classTemplate.ClassName, classTemplate.SourceCode);
            return (IExecutable)Activator.CreateInstance(type);
        }

        /// <summary>Gets-or-creates an executable for the specified type, source code and parameters</summary>
        /// <param name="executableType">The type of executable to get</param>
        /// <param name="sourceCode">The source code for the executable</param>
        /// <param name="parameterNames">The parameter names for the executable (may be null)</param>
        /// <param name="usingNamespaces">Namespaces to add (may be null)</param>
        /// <returns>An executable for the specified combination of type, source code and parameters</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IExecutable GetExecutable(ExecutableType executableType, string sourceCode, IEnumerable<string> parameterNames, IEnumerable<string> usingNamespaces)
        {
            if (string.IsNullOrWhiteSpace(sourceCode)) throw new ArgumentException("Invalid (null or zero-length) source code");

            string key = (parameterNames == null || !parameterNames.Any()) ? 
                executableType.ToString() + "||" + sourceCode :
                executableType.ToString() + "|" + string.Join(",", parameterNames.Where(p => !string.IsNullOrWhiteSpace(p)).OrderBy(p => p)) + "|" + sourceCode;

            int index = key.GetHashCodeFastAbsolute() % partitionCount;
            ConcurrentDictionary<string, IExecutable> valueCollection;
            IExecutable value;

            // 1. Initialize bucket if it has not yet been

            if (!initializationFlags[index])
            {
                valueCollection = new ConcurrentDictionary<string, IExecutable>();
                ConcurrentDictionary<string, object> valueLocksCollection = new ConcurrentDictionary<string, object>();

                lock (locks[index])
                {
                    if (!initializationFlags[index])
                    {
                        values[index] = valueCollection;
                        valueLocks[index] = valueLocksCollection;
                        initializationFlags[index] = true;
                    }
                }
            }

            // 2. Check for pre-existence of value sought

            valueCollection = values[index];
            if (valueCollection.TryGetValue(key, out value))
                return value.Copy(); // Executables are desiged to be lightweight-to-create and thread-safe; copying avoids lock contention

            // 3. Use double-checked locking to check again for pre-existence of value sought

            lock (valueLocks[index].GetOrAdd(key, new object()))
            {
                if (valueCollection.TryGetValue(key, out value))
                    return value.Copy();

                valueCollection[key] = value = CreateExecutable(executableType, sourceCode, parameterNames, usingNamespaces);
            }

            return value.Copy();
        }

        /* Hidden for now due to conflict with signature of GetExecutable when parameters are null
        /// <summary>Gets-or-creates an executable for the specified type, source code and parameters</summary>
        /// <param name="executableType">The type of executable to get</param>
        /// <param name="sourceCode">The source code for the executable</param>
        /// <param name="parameters">The named parameters for the executable</param>
        /// <param name="usingNamespaces">Namespaces to add (may be null)</param>
        /// <returns>An executable for the specified combination of type, source code and parameters</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IExecutable GetExecutable(ExecutableType executableType, string sourceCode, IDictionary<string, object> parameters, IEnumerable<string> usingNamespaces)
        {
            return GetExecutable(executableType, sourceCode, parameters != null ? parameters.Keys : (IEnumerable<string>)null, usingNamespaces);
        }
        */
    }
}
