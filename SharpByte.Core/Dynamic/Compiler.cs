﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpByte.Dynamic
{
    /// <summary>An adapter for a .NET/C# compiler</summary>
    public abstract class Compiler : ICompiler
    {
        #region Static members

        /// <summary>A compiler of a specific type</summary>
        private static ICompiler codeDomCompiler;
        
        /// <summary>Used in double-checked locking</summary>
        private static object codeDomCompilerLock = new object();

        /// <summary>Gets a compiler of a specific type</summary>
        private static ICompiler CodeDomCompiler
        {
            get
            {
                if (codeDomCompiler == null)
                    lock (codeDomCompilerLock)
                        if (codeDomCompiler == null)
                            codeDomCompiler = (ICompiler)Activator.CreateInstance(Type.GetType("SharpByte.Dynamic.Impl.CodeDomCompiler", true, false));

                return codeDomCompiler;
            }
        }

        /// <summary>A compiler of a specific type</summary>
        private static ICompiler compilerPlatformCompiler;
        
        /// <summary>Used in double-checked locking</summary>
        private static object compilerPlatformCompilerLock = new object();

        /// <summary>Gets a compiler of a specific type</summary>
        private static ICompiler CompilerPlatformCompiler
        {
            get
            {
                if (compilerPlatformCompiler == null)
                    lock (compilerPlatformCompilerLock)
                        if (compilerPlatformCompiler == null)
                            compilerPlatformCompiler = (ICompiler)Activator.CreateInstance(Type.GetType("SharpByte.Dynamic.Impl.CompilerPlatformCompiler", true, false));

                return compilerPlatformCompiler;
            }
        }

        /// <summary>A default/singleton instance</summary>
        private static ICompiler defaultInstance;
        
        /// <summary>Used in double-checked locking</summary>
        private static object defaultInstanceLock = new object();

        /// <summary>Gets a default/singleton instance</summary>
        public static ICompiler Default
        {
            get
            {
                if (defaultInstance == null)
                    lock (defaultInstanceLock)
                        if (defaultInstance == null)
                        {
                            try { defaultInstance = Compiler.CompilerPlatformCompiler; } catch { }
                            if (defaultInstance == null)
                                defaultInstance = Compiler.CodeDomCompiler;
                        }

                return defaultInstance;
            }
        }

        /// <summary>Gets a C# compiler of the default type</summary>
        /// <returns>A C# compiler</returns>
        public static ICompiler GetCompiler()
        {
            return Compiler.Default;
        }

        /// <summary>Gets a compiler by type</summary>
        /// <param name="compilerType">The type of compiler to get</param>
        /// <returns>A compiler of the requested type</returns>
        public static ICompiler GetCompiler(CompilerType compilerType)
        {
            switch (compilerType)
            {
                case CompilerType.CodeDom:
                    return Compiler.CodeDomCompiler;
                case CompilerType.CompilerPlatform:
                    return Compiler.CompilerPlatformCompiler;
                default:
                    throw new InvalidOperationException("Unknown compiler type");
            }
        }

        #endregion 

        /// <summary>A list of references to use in compilation</summary>
        private IList<string> references = new List<string>();

        /// <summary>A synchronization locking object</summary>
        private object referencesLock = new object();

        /// <summary>Gets a list of references added to each compilation</summary>
        public IEnumerable<string> References
        {
            get
            {
                return references.ToList();
            }
        }

        /// <summary>Adds the specified reference for future compilations</summary>
        /// <param name="reference">The reference to add</param>
        public void AddReference(string reference)
        {
            if (string.IsNullOrWhiteSpace(reference)) return;
            reference = reference.Trim();

            lock (referencesLock)
            {
                if (!references.Contains(reference))
                {
                    List<string> copy = references.ToList();
                    copy.Add(reference);
                    references = copy.OrderBy(r => r).ToList();                   
                }
            }
        }

        /// <summary>Adds the specified references for future compilations</summary>
        /// <param name="references">The references to add</param>
        public void AddReferences(IEnumerable<string> references)
        {
            if (references != null)
            {
                lock (referencesLock)
                {
                    List<string> copy = references.ToList();
                    string reference;
                    foreach (string r in references)
                        if (!string.IsNullOrWhiteSpace(r) && !references.Contains(reference = r.Trim()))
                            copy.Add(reference);
                    references = copy.OrderBy(r => r).ToList();                   
                }
            }
        }

        /// <summary>Constructs a new instance</summary>
        protected Compiler() { }

        /// <summary>Compiles (or retrieves) a type for the specified source code, throwing an exception in case of a compilation failure</summary>
        /// <param name="typeName">The fully-qualified type name to compile</param>
        /// <param name="sourceCode">The code to compile</param>
        /// <param name="references">References to use in compilation</param>
        /// <returns>A compiled type</returns>
        public abstract Type Compile(string typeName, string sourceCode, IEnumerable<string> references = null);
    }
}
