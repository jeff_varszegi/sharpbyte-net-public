﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SharpByte.Utility
{
    /// <summary>A thread-safe random number generator</summary>
    public class Random
    {
        #region Static members
        
        /// <summary>A class-wide seed generator</summary>
        private static System.Random staticSeedGenerator;

        /// <summary>A default/singleton instance</summary>
        private static Random defaultInstance;

        /// <summary>A static initializer for this class, which sets up safe seed generation</summary>
        static Random()
        {
            int seed = Environment.TickCount;
            if (seed < 524287)
                seed *= 3571;
            else
                seed = seed % 524287;

            staticSeedGenerator = new System.Random(seed);
            staticSeedGenerator.NextDouble();            
        }

        /// <summary>A default/singleton instance</summary>
        public static Random Default
        {
            get
            {
                if (defaultInstance == null)
                    defaultInstance = new Random();
                return defaultInstance;
            }
        }

        /// <summary>Gets a seed value for random number generation</summary>
        /// <returns>A pseudorandom seed value not directly based on Environment.Ticks, which will vary even if called sequentially</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static int GetSeed()
        {
            lock (staticSeedGenerator)
                return staticSeedGenerator.Next();
        }

        #endregion Static members

        /// <summary>A collection of random number generators used to decrease lock contention while providing thread safety</summary>
        private System.Random random;

        /// <summary>Constructs a new instance</summary>
        /// <param name="capacity">The number of underlying random number generators to create</param>
        public Random()
        {
            random = new System.Random(GetSeed());
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="capacity">The number of underlying random number generators to create</param>
        /// <param name="seed">The seed to use in initialization</param>
        public Random(int capacity, int seed)
        {
            random = new System.Random(seed);
        }

        /// <summary>Gets a non-negative random number</summary>
        /// <returns>A non-negative random number</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual int Next()
        {
            lock (random)
                return random.Next();
        }

        /// <summary>Gets a non-negative random number less than a maximum</summary>
        /// <param name="maximumValue">The non-inclusive upper bound of the range for potential return values</param>
        /// <returns>A non-negative random number, less than the specified maximum value</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual int Next(int maximumValue)
        {
            lock (random)
                return random.Next(maximumValue);
        }

        /// <summary>Gets a random number within a range</summary>
        /// <param name="minimumValue">The inclusive lower bound of the range for potential return values</param>
        /// <param name="maximumValue">The non-inclusive upper bound of the range for potential return values</param>
        /// <returns>A random number within the specified range</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual int Next(int minimumValue, int maximumValue)
        {
            lock (random)
                return random.Next(minimumValue, maximumValue);
        }

        /// <summary>Fills the specified buffer with random bytes</summary>
        /// <param name="buffer">An array of bytes to randomize</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual void NextBytes(byte[] buffer)
        {
            int blockCount = buffer.Length / 4;
            int i;

            lock (random)
            {
                for (int x = (blockCount * 4) - 4; x >= 0; x -= 4)
                {
                    i = random.Next(Int32.MinValue, Int32.MaxValue);
                    buffer[x] = (byte)(i >> 24);
                    buffer[x + 1] = (byte)(i >> 16);
                    buffer[x + 2] = (byte)(i >> 8);
                    buffer[x + 3] = (byte)i;
                }

                int y = buffer.Length % 4;
                if (y > 0)
                {
                    i = random.Next(Int32.MinValue, Int32.MaxValue);

                    if (y > 2) buffer[buffer.Length - 3] = (byte)(i >> 16);
                    if (y > 1) buffer[buffer.Length - 2] = (byte)(i >> 8);
                    buffer[buffer.Length - 1] = (byte)i;
                }
            }
        }

        /// <summary>Returns a random floating-point number rom 0.0 to 1.0</summary>
        /// <returns>A pseudorandom floating-point number</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual double NextDouble()
        {
            lock (random)
                return random.NextDouble();
        }

        /// <summary>Generates a random string of the specified length</summary>
        /// <param name="length">The length of string to generate</param>
        /// <param name="chars">The characters to use</param>
        /// <returns>A random string of the specified length, restricted to the supplied characters</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public virtual string NextString(int length, string chars = "0123456789ABCDEF")
        {
            if (length <= 0) return "";

            char[] sb = new char[length];
            int index, charsLength = chars.Length;

            if (charsLength <= 256)
            {
                byte[] buffer = new byte[length];
                this.NextBytes(buffer);

                for (int x = 0; x < length; x++)
                    sb[x] = chars[buffer[x] % charsLength];
            }
            else
            {
                lock (random)
                {
                    for (int x = 0; x < length; x++)
                    {
                        index = random.Next(charsLength);
                        sb[x] = chars[index];
                    }
                }
            }

            return new string(sb);
        }

        /// <summary>Shuffles a collection in place</summary>
        /// <typeparam name="T">The type of items</typeparam>
        /// <param name="items">The collection to shuffle</param>
        public virtual void Shuffle<T>(T[] items)
        {
            int y;
            T temp;
            lock (random)
            {
                for (int x = items.Length - 1; x >= 0; x--)
                {
                    y = random.Next(items.Length);
                    if (x != y)
                    {
                        temp = items[x];
                        items[x] = items[y];
                        items[y] = temp;
                    }
                }
            }
        }

        /// <summary>Shuffles a collection in place</summary>
        /// <typeparam name="T">The type of items</typeparam>
        /// <param name="items">The collection to shuffle</param>
        public virtual void Shuffle<T>(List<T> items)
        {
            int y;
            T temp;
            int count = items.Count;
            lock (random)
            {
                for (int x = count - 1; x >= 0; x--)
                {
                    y = random.Next(count);
                    if (x != y)
                    {
                        temp = items[x];
                        items[x] = items[y];
                        items[y] = temp;
                    }
                }
            }
        }
    }
}
