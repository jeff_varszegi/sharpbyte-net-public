﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

using SharpByte.Collections.Specialized;
using System.Text;

#if COLOCATE_EXTENSIONS
namespace System.Text.RegularExpressions
{
#elif !GLOBAL_EXTENSIONS
namespace SharpByte.Text.RegularExpressions
{
#endif

/// <summary>Provides extension/utility logic for strings</summary>
public static partial class StringExtensions
{
    #region Regex-related

    /// <summary>A collection of regexes</summary>
    private static RegexLibrary library = new RegexLibrary();

    /// <summary>A collection of regexes</summary>
    private static RegexLibrary Library
    {
        get
        {
            if (library == null)
                library = RegexLibrary.Default;
            return library;
        }
    }

    /// <summary>A reusable empty result</summary>
    private static MatchCollection EmptyMatchCollection = Regex.Matches("", "0");

    /// <summary>A reusable empty result</summary>
    private static Match UnsuccessfulMatch = Regex.Match("a", "b");

    /// <summary>Constructs RegexOptions using the supplied settings</summary>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <param name="rightToLeft">If true, the right-to-left option will be used</param>
    /// <returns>RegexOptions based on the supplied settings</returns>    
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static RegexOptions GetRegexOptions(bool compiled, bool ignoreCase, bool multiline, bool rightToLeft)
    {
        RegexOptions options = RegexOptions.CultureInvariant;
        if (ignoreCase)
            options |= RegexOptions.IgnoreCase;
        if (compiled)
            options |= RegexOptions.Compiled;
        if (multiline)
            options |= RegexOptions.Multiline;
        if (rightToLeft)
            options |= RegexOptions.RightToLeft;

        return options;
    }

    /// <summary>Default regex options</summary>
    private static RegexOptions defaultRegexOptions = RegexOptions.None;

    /// <summary>Sets default regex options for use with strings</summary>
    /// <param name="s">this string</param>
    /// <param name="options">The options to set as defaults for use with regex string extensions</param>
    public static void SetDefault(this string s, RegexOptions options)
    {
        defaultRegexOptions = options;
    }

    /// <summary>Indicates whether this string matches the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The pattern to test against</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>true if this string matches the pattern, otherwise false</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsMatch(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return false;
        return Library.GetRegex(pattern, options).IsMatch(s);
    }

    /// <summary>Indicates whether this string matches the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The pattern to test against</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>true if this string matches the pattern, otherwise false</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsMatch(this string s, string pattern)
    {
        if (s == null || pattern == null) return false;
        return Library.GetRegex(pattern, defaultRegexOptions).IsMatch(s);
    }

    /// <summary>Indicates whether this string matches the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The pattern to test against</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>true if this string matches the pattern, otherwise false</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool IsMatch(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return false;
        return Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, false)).IsMatch(s);
    }

    /// <summary>Gets the first index of the specified pattern in this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>The index of the first pattern match, or -1 if not found</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int IndexOfPattern(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return -1;
        Match match = Library.GetRegex(pattern, options).Match(s);
        return match.Success ? match.Index : -1;
    }

    /// <summary>Gets the first index of the specified pattern in this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <returns>The index of the first pattern match, or -1 if not found</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int IndexOfPattern(this string s, string pattern)
    {
        if (s == null || pattern == null) return -1;
        Match match = Library.GetRegex(pattern, defaultRegexOptions).Match(s);
        return match.Success ? match.Index : -1;
    }

    /// <summary>Gets the first index of the specified pattern in this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>The index of the first pattern match, or -1 if not found</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int IndexOfPattern(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return -1;
        Match match = Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, false)).Match(s);
        return match.Success ? match.Index : -1;
    }

    /// <summary>Gets the last index of the specified pattern in this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>The index of the last pattern match, or -1 if not found</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int LastIndexOfPattern(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return -1;
        Match match = Library.GetRegex(pattern, options).Match(s);
        return match.Success ? match.Index : -1;
    }

    /// <summary>Gets the last index of the specified pattern in this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <returns>The index of the last pattern match, or -1 if not found</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int LastIndexOfPattern(this string s, string pattern)
    {
        if (s == null || pattern == null) return -1;
        Match match = Library.GetRegex(pattern, defaultRegexOptions).Match(s);
        return match.Success ? match.Index : -1;
    }

    /// <summary>Gets the last index of the specified pattern in this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>The index of the last pattern match, or -1 if not found</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int LastIndexOfPattern(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return -1;
        Match match = Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, true)).Match(s);
        return match.Success ? match.Index : -1;
    }

    /// <summary>Gets the matches for this string using the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>The collection of matches for this string and the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static MatchCollection Matches(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return EmptyMatchCollection;
        return Library.GetRegex(pattern, options).Matches(s);
    }

    /// <summary>Gets the matches for this string using the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <returns>The collection of matches for this string and the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static MatchCollection Matches(this string s, string pattern)
    {
        if (s == null || pattern == null) return EmptyMatchCollection;
        return Library.GetRegex(pattern, defaultRegexOptions).Matches(s);
    }

    /// <summary>Gets the matches for this string using the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>The collection of matches for this string and the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static MatchCollection Matches(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return EmptyMatchCollection;
        return Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, false)).Matches(s);
    }

    /// <summary>Used in replacement delegation</summary>
    /// <param name="match">The match to which to conform</param>
    /// <param name="replacement">The replacement to conform</param>
    /// <returns>A case-conformed string</returns>
    private static string ConformCase(Match match, string replacement) {
        return replacement.ConformCase(match.Value);
    }

    /// <summary>Replaces all occurrences of the specified pattern within this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The pattern to replace</param>
    /// <param name="replacement">The value to use in replacement</param>
    /// <param name="options">Regex options to use</param>
    /// <param name="conformCase">If true, the case of replacements will be conformed to the values replaced</param>
    /// <returns>A version of this string with the specified pattern replaced</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string ReplacePattern(this string s, string pattern, string replacement, RegexOptions options, bool conformCase = false)
    {
        if (s == null || s.Length == 0) return "";
        else if (pattern == null || pattern.Length == 0) return s;

        Regex regex = Library.GetRegex(pattern, options);
        return conformCase ? regex.Replace(s, m => ConformCase(m, replacement)) : regex.Replace(s, replacement);
    }

    /// <summary>Replaces all occurrences of the specified pattern within this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The pattern to replace</param>
    /// <param name="replacement">The value to use in replacement</param>
    /// <param name="conformCase">If true, the case of replacements will be conformed to the values replaced</param>
    /// <returns>A version of this string with the specified pattern replaced</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string ReplacePattern(this string s, string pattern, string replacement)
    {
        return ReplacePattern(s, pattern, replacement, defaultRegexOptions);
    }

    /// <summary>Replaces all occurrences of the specified pattern within this string</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The pattern to replace</param>
    /// <param name="replacement">The value to use in replacement</param>
    /// <param name="compiled">If true, the regex used will be compiled</param>
    /// <param name="ignoreCase">If true, case will be ignored in matching</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <param name="conformCase">If true, the case of replacements will be conformed to the values replaced</param>
    /// <returns>A version of this string with the specified pattern replaced</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string ReplacePattern(this string s, string pattern, string replacement, bool compiled = false, bool ignoreCase = false, bool multiline = false, bool conformCase = false)
    {
        return ReplacePattern(s, pattern, replacement, GetRegexOptions(compiled, ignoreCase, multiline, false), conformCase);
    }

    /// <summary>Finds all substrings matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>All substrings of this string matching the pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<string> FindAll(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return new List<string>();
        return Library.GetRegex(pattern, options).Matches(s).Select(m => m.Value);
    }

    /// <summary>Finds all substrings matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <returns>All substrings of this string matching the pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<string> FindAll(this string s, string pattern)
    {
        if (s == null || pattern == null) return new List<string>();
        return Library.GetRegex(pattern, defaultRegexOptions).Matches(s).Select(m => m.Value);
    }

    /// <summary>Finds all substrings matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>All substrings of this string matching the pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<string> FindAll(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return new List<string>();
        return Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, false)).Matches(s).Select(m => m.Value);
    }

    /// <summary>Gets the first match in this string for the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>The first match for the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match Match(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return UnsuccessfulMatch;
        return Library.GetRegex(pattern, options).Match(s);
    }

    /// <summary>Gets the first match in this string for the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <returns>The first match for the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match Match(this string s, string pattern)
    {
        if (s == null || pattern == null) return UnsuccessfulMatch;
        return Library.GetRegex(pattern, defaultRegexOptions).Match(s);
    }

    /// <summary>Gets the first match in this string for the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>The first match for the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match Match(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return UnsuccessfulMatch;
        return Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, false)).Match(s);
    }

    /// <summary>Gets the last match in this string for the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>The last match for the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match MatchLast(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return UnsuccessfulMatch;
        return Library.GetRegex(pattern, options).Match(s);
    }

    /// <summary>Gets the last match in this string for the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>The last match for the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match MatchLast(this string s, string pattern)
    {
        if (s == null || pattern == null) return UnsuccessfulMatch;
        return Library.GetRegex(pattern, defaultRegexOptions).Match(s);
    }

    /// <summary>Gets the last match in this string for the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>The last match for the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match MatchLast(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return UnsuccessfulMatch;
        return Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, true)).Match(s);
    }

    /// <summary>Finds the first substring matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>The first substring of this string matching the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Find(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return "";
        Match match = Library.GetRegex(pattern, options).Match(s);
        return match.Success ? match.Value : "";
    }

    /// <summary>Finds the first substring matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <returns>The first substring of this string matching the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Find(this string s, string pattern)
    {
        if (s == null || pattern == null) return "";
        Match match = Library.GetRegex(pattern, defaultRegexOptions).Match(s);
        return match.Success ? match.Value : "";
    }

    /// <summary>Finds the first substring matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>The first substring of this string matching the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Find(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return "";
        Match match = Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, false)).Match(s);
        return match.Success ? match.Value : "";
    }

    /// <summary>Finds the last substring matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="options">Regex options to use</param>
    /// <returns>The last substring of this string matching the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string FindLast(this string s, string pattern, RegexOptions options)
    {
        if (s == null || pattern == null) return "";
        Match match = Library.GetRegex(pattern, options).Match(s);
        return match.Success ? match.Value : "";
    }

    /// <summary>Finds the last substring matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <returns>The last substring of this string matching the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string FindLast(this string s, string pattern)
    {
        if (s == null || pattern == null) return "";
        Match match = Library.GetRegex(pattern, defaultRegexOptions).Match(s);
        return match.Success ? match.Value : "";
    }

    /// <summary>Finds the last substring matching the specified pattern</summary>
    /// <param name="s">this string</param>
    /// <param name="pattern">The regex pattern</param>
    /// <param name="compiled">If true, a compiled regex will be used</param>
    /// <param name="ignoreCase">If true, casing will be ignored</param>
    /// <param name="multiline">If true, the multiline option will be used</param>
    /// <returns>The last substring of this string matching the specified pattern</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string FindLast(this string s, string pattern, bool compiled = false, bool ignoreCase = false, bool multiline = false)
    {
        if (s == null || pattern == null) return "";
        Match match = Library.GetRegex(pattern, GetRegexOptions(compiled, ignoreCase, multiline, true)).Match(s);
        return match.Success ? match.Value : "";
    }

    #endregion Regex-related
}

#if (COLOCATE_EXTENSIONS || !GLOBAL_EXTENSIONS)
}
#endif