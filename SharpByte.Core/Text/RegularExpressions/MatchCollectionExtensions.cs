﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

#if COLOCATE_EXTENSIONS
namespace System.Text.RegularExpressions
{
#elif !GLOBAL_EXTENSIONS
namespace SharpByte.Text.RegularExpressions
{
#endif

/// <summary>Provides extension/utility logic for MatchCollection objects</summary>
public static partial class MatchCollectionExtensions
{
    #region Linq enablement

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool Any(this MatchCollection source, Func<Match, bool> predicate)
    {
        return source.Cast<Match>().Any(predicate);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match First(this MatchCollection source)
    {
        return (source == null || source.Count == 0) ? null : source[0];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match First(this MatchCollection source, Func<Match, bool> predicate) {
        return source.Cast<Match>().First(predicate);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match FirstOrDefault(this MatchCollection source)
    {
        return (source == null || source.Count == 0) ? null : source[0];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match FirstOrDefault<T>(this MatchCollection source, Func<Match, bool> predicate)
    {
        return source.Cast<Match>().FirstOrDefault(predicate);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match Last<T>(this MatchCollection source)
    {
        return (source == null || source.Count == 0) ? null : source[0];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match Last(this MatchCollection source, Func<Match, bool> predicate)
    {
        return source.Cast<Match>().Last(predicate);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match LastOrDefault(this MatchCollection source)
    {
        return (source == null || source.Count == 0) ? null : source[source.Count - 1];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Match LastOrDefault<T>(this MatchCollection source, Func<Match, bool> predicate)
    {
        return source.Cast<Match>().LastOrDefault(predicate);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IOrderedEnumerable<Match> OrderBy<Match, TKey>(this MatchCollection source, Func<Match, TKey> keySelector)
    {
        return source.Cast<Match>().OrderBy(keySelector);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IOrderedEnumerable<Match> OrderBy<Match, TKey>(this MatchCollection source, Func<Match, TKey> keySelector, IComparer<TKey> comparer)
    {
        return source.Cast<Match>().OrderBy(keySelector, comparer);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IOrderedEnumerable<Match> OrderByDescending<Match, TKey>(this MatchCollection source, Func<Match, TKey> keySelector)
    {
        return source.Cast<Match>().OrderByDescending(keySelector);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IOrderedEnumerable<Match> OrderByDescending<Match, TKey>(this MatchCollection source, Func<Match, TKey> keySelector, IComparer<TKey> comparer)
    {
        return source.Cast<Match>().OrderByDescending(keySelector, comparer);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<Match> Reverse(this MatchCollection source)
    {
        return source.Cast<Match>().Reverse();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<TResult> Select<TResult>(this MatchCollection source, Func<Match, TResult> selector)
    {
        return source.Cast<Match>().Select(selector);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<TResult> Select<TResult>(this MatchCollection source, Func<Match, int, TResult> selector)
    {
        return source.Cast<Match>().Select(selector);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<Match> Skip(this MatchCollection source, int count)
    {
        return source.Cast<Match>().Skip(count);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<Match> Take(this MatchCollection source, int count)
    {
        return source.Cast<Match>().Take(count);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Dictionary<TKey, Match> ToDictionary<Match, TKey>(this MatchCollection source, Func<Match, TKey> keySelector)
    {
        return source.Cast<Match>().ToDictionary(keySelector);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Dictionary<TKey, Match> ToDictionary<Match, TKey>(this MatchCollection source, Func<Match, TKey> keySelector, IEqualityComparer<TKey> comparer)
    {
        return source.Cast<Match>().ToDictionary(keySelector, comparer);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Dictionary<TKey, TElement> ToDictionary<Match, TKey, TElement>(this MatchCollection source, Func<Match, TKey> keySelector, Func<Match, TElement> elementSelector)
    {
        return source.Cast<Match>().ToDictionary(keySelector, elementSelector);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static Dictionary<TKey, TElement> ToDictionary<Match, TKey, TElement>(this MatchCollection source, Func<Match, TKey> keySelector, Func<Match, TElement> elementSelector, IEqualityComparer<TKey> comparer)
    {
        return source.Cast<Match>().ToDictionary(keySelector, elementSelector, comparer);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static List<Match> ToList<T>(this MatchCollection source)
    {
        return source.Cast<Match>().ToList();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static IEnumerable<Match> Where<T>(this MatchCollection source, Func<Match, bool> predicate)
    {
        return source.Cast<Match>().Where(predicate);
    }

    #endregion Linq enablement
}

#if (COLOCATE_EXTENSIONS || !GLOBAL_EXTENSIONS)
}
#endif