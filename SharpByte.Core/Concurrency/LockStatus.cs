﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

namespace SharpByte.Concurrency
{
    /// <summary>Enumerates statuses for composite locks</summary>
    public enum LockStatus
    {
        /// <summary>Indicates that a lock is not locked</summary>
        Open,

        /// <summary>Indicates that a lock is being moved from not locked to read mode</summary>
        EnteringRead,

        /// <summary>Indicates that a lock is in read-only mode</summary>
        Read,

        /// <summary>Indicates that a lock is being moved from not locked to write mode</summary>
        EnteringWrite,

        /// <summary>Indicates that a lock is in write mode</summary>
        Write,

        /// <summary>Indicates that the last operation performed on a lock resulted in an exception</summary>
        Error
    }
}
