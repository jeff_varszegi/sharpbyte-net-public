﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Runtime.CompilerServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;

namespace SharpByte.Concurrency
{
    /// <summary>An inter-process lock, which should be used only when necessary due to its resource-intensive nature</summary>
    internal class MutexLock : Lock, IDisposable
    {
        /// <summary>The underlying system mutex</summary>
        private Mutex mutex;

        /// <summary>The name of the mutex</summary>
        public string MutexName { get; private set; }

        /// <summary>The number of milliseconds this instance will wait for a mutex handle</summary>
        public int TimeoutMilliseconds { get; private set; }

        /// <summary>Indicates whether the handle has been acquired to the underlying mutex</summary>
        private bool hasMutexHandle = false;

        /// <summary>A locking object</summary>
        private object monitor = new object();

        /// <summary>A hidden default constructor</summary>
        private MutexLock() { }

        /// <summary>Creates a new instance, initializing a named mutex</summary>
        /// <param name="mutexName">The name of the mutex to create (cannot be null or empty)</param>
        /// <param name="timeoutMilliseconds">The number of milliseconds this instance will wait for a mutex handle</param>
        public MutexLock(string mutexName, int timeoutMilliseconds = int.MaxValue)
        {
            if (string.IsNullOrWhiteSpace(mutexName)) throw new ArgumentException("Invalid (null or whitespace) mutex name");
            mutexName = mutexName.Replace(' ', '_').Replace('.', '_').RestrictToCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_");
            if (string.IsNullOrWhiteSpace(mutexName)) throw new ArgumentException("Invalid mutex name (must be alphanumeric)");
            MutexName = mutexName;

            TimeoutMilliseconds = (timeoutMilliseconds < 0) ? int.MaxValue : timeoutMilliseconds;

            mutex = new Mutex(false, "Global\\" + MutexName);
            MutexAccessRule accessRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
            MutexSecurity security = new MutexSecurity();
            security.AddAccessRule(accessRule);
            mutex.SetAccessControl(security);
        }

        /// <summary>Attempts to enter a read lock (if read locking is not supported, the general read-write lock is entered instead)</summary>
        /// <returns>true if the lock was entered, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool EnterReadLock()
        {
            lock (monitor)
            {
                if (hasMutexHandle) return true;

                try
                {
                    if (TimeoutMilliseconds == int.MaxValue)
                        hasMutexHandle = mutex.WaitOne(Timeout.Infinite, false);
                    else
                        hasMutexHandle = mutex.WaitOne(TimeoutMilliseconds, false);

                    if (hasMutexHandle == false)
                        throw new TimeoutException("Timeout waiting for mutex handle");
                }
                catch (AbandonedMutexException)
                {
                    hasMutexHandle = true;
                }

                return hasMutexHandle;
            }
        }

        /// <summary>Attempts to enter a write lock (if write locking is not supported, the general read-write lock is entered instead)</summary>
        /// <returns>true if the lock was entered, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool EnterWriteLock()
        {
            lock (monitor)
            {
                if (hasMutexHandle) return true;

                try
                {
                    if (TimeoutMilliseconds == int.MaxValue)
                        hasMutexHandle = mutex.WaitOne(Timeout.Infinite, false);
                    else
                        hasMutexHandle = mutex.WaitOne(TimeoutMilliseconds, false);

                    if (hasMutexHandle == false)
                        throw new TimeoutException("Timeout waiting for mutex handle");
                }
                catch (AbandonedMutexException)
                {
                    hasMutexHandle = true;
                }
                catch
                {
                    throw;
                }

                return hasMutexHandle;
            }
        }

        /// <summary>Exits a previously taken lock</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool ExitReadLock()
        {
            lock (monitor)
            {
                if (hasMutexHandle)
                {
                    hasMutexHandle = false;                        

                    try
                    {
                        mutex.ReleaseMutex();
                    }
                    catch
                    {
                        throw;
                    }
                }

                return true;
            }
        }

        /// <summary>Exits a previously taken lock</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool ExitWriteLock()
        {
            throw new NotImplementedException();
        }


        /// <summary>Disposes of this object and the underlying mutex</summary>
        public void Dispose()
        {
            lock (monitor)
            {
                if (mutex != null)
                {
                    if (hasMutexHandle)
                    {
                        try { mutex.ReleaseMutex(); } catch {}
                        hasMutexHandle = false;                        
                    }

                    try { mutex.Dispose(); } catch {}
                    mutex = null;
                }
            }
        }

    }
}
