﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System.Runtime.CompilerServices;
using System.Threading;

namespace SharpByte.Concurrency
{
    /// <summary>A reader-writer lock</summary>
    internal sealed class ReaderWriterLock : Lock
    {
        /// <summary>The underlying lock used by this instance</summary>
        public readonly ReaderWriterLockSlim Lock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        /// <summary>Constructs a new lock</summary>
        /// <param name="isTrackingEnabled">Indicates whether this lock is tracking-enabled</param>
        public ReaderWriterLock() {}

        /// <summary>Attempts to enter a read lock (if read locking is not supported, the general read-write lock is entered instead)</summary>
        /// <returns>true if the lock was entered, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool EnterReadLock()
        {
            return Lock.TryEnterReadLock(System.Threading.Timeout.Infinite);
        }

        /// <summary>Attempts to enter a write lock (if write locking is not supported, the general read-write lock is entered instead)</summary>
        /// <returns>true if the lock was entered, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool EnterWriteLock()
        {
            return Lock.TryEnterWriteLock(System.Threading.Timeout.Infinite);
        }

        /// <summary>Exits a previously taken lock</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool ExitReadLock()
        {
            Lock.ExitReadLock();
            return true;
        }

        /// <summary>Exits a previously taken lock</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool ExitWriteLock()
        {
            Lock.ExitWriteLock();
            return true;
        }
    }
}
