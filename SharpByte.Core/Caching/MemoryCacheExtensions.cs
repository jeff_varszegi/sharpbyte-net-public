﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Concurrent;
using System.Runtime.Caching;
using System.Runtime.CompilerServices;

#if COLOCATE_EXTENSIONS
namespace System.Runtime.Caching
{
#elif !GLOBAL_EXTENSIONS
namespace SharpByte.Caching
{
#endif

/// <summary>Provides extension/utility logic for MemoryCache and related objects, useful for quick-and-dirty local caching</summary>
public static partial class MemoryCacheExtensions
{
    #region MemoryCache extensions

    /// <summary>The maximum safe value for conversions from milliseconds to ticks</summary>
    private const long Int64MillisecondConversionCeiling = long.MaxValue / 10000L;
        
    /// <summary>A cache of CacheItemPolicy objects for reuse</summary>
    private static ConcurrentDictionary<long, CacheItemPolicy> slidingExpirationCacheItemPolicyMap = new ConcurrentDictionary<long, CacheItemPolicy>();
        
    /// <summary>The last-used CacheItemPolicy object</summary>
    private static CacheItemPolicy lastSlidingExpirationCacheItemPolicy = null;

    /// <summary>Gets-or-creates a cache item policy object</summary>
    /// <param name="slidingExpiration">The sliding duration, in milliseconds</param>
    /// <returns>An appropriate CacheItemPolicy object</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static CacheItemPolicy GetSlidingDurationCacheItemPolicy(long slidingExpiration)
    {
        CacheItemPolicy lastUsed = lastSlidingExpirationCacheItemPolicy;

        if (slidingExpiration <= Int64MillisecondConversionCeiling)
            slidingExpiration *= 10000L;

        if (lastUsed == null)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.SlidingExpiration = new TimeSpan(slidingExpiration);
            lastSlidingExpirationCacheItemPolicy = policy;
            slidingExpirationCacheItemPolicyMap[slidingExpiration] = policy;
            return policy;
        }
        else if (lastUsed.SlidingExpiration.Ticks == slidingExpiration)
        {
            return lastUsed;
        }
        else
        {
            CacheItemPolicy policy = null;

            if (slidingExpirationCacheItemPolicyMap.TryGetValue(slidingExpiration, out policy))
            {
                lastSlidingExpirationCacheItemPolicy = policy;
                return policy;
            }

            policy = new CacheItemPolicy();
            policy.SlidingExpiration = new TimeSpan(slidingExpiration);
            slidingExpirationCacheItemPolicyMap[slidingExpiration] = policy;
            lastSlidingExpirationCacheItemPolicy = policy;

            return policy;
        }
    }

    /// <summary>Caches the specified object with a sliding expiration</summary>
    /// <param name="cache">this in-memory cache</param>
    /// <param name="key">The cache key</param>
    /// <param name="value">The object to cache</param>
    /// <param name="expirationMilliseconds">The number of milliseconds to cache the object between accesses</param>
    /// <param name="isExpirationSliding">If true (the default), sliding expiration will be used; if false, absolute expiration</param>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Set(this MemoryCache cache, string key, object value, long expirationMilliseconds = long.MaxValue, bool isExpirationSliding = true)
    {
        if (cache == null || key == null) throw new ArgumentNullException();
        else if (expirationMilliseconds <= 0L || value == null)
            try { cache.Remove(key); } catch {}

        if (isExpirationSliding)
            cache.Set(key, value, GetSlidingDurationCacheItemPolicy(expirationMilliseconds));
        else
            cache.Set(key, value, DateTime.Now.AddMilliseconds(expirationMilliseconds));
    }

    /// <summary>Gets a value from this cache</summary>
    /// <typeparam name="T">The type of object to get</typeparam>
    /// <param name="cache">this cache</param>
    /// <param name="key">The key for which to retrieve the value</param>
    /// <param name="defaultValue">The default value to use if the key is not found</param>
    /// <returns>An object found of the specified type, or the type default</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T Get<T>(this MemoryCache cache, string key, T defaultValue = default(T))
    {
        object value = cache.Get(key);

        if (value == null || !(value is T))
            return defaultValue;
        else
            return (T)value;
    }

    #endregion MemoryCache extensions
}

#if (COLOCATE_EXTENSIONS || !GLOBAL_EXTENSIONS)
}
#endif
