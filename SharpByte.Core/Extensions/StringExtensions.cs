﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Runtime.CompilerServices;
using System.Text;

using SharpByte.Collections.Specialized;

#if COLOCATE_EXTENSIONS
namespace System
{
#elif !GLOBAL_EXTENSIONS
namespace SharpByte.Extensions
{
#endif

/// <summary>Provides extension/utility logic for strings</summary>
public static partial class StringExtensions
{

    /// <summary>Conforms the case of this string to that of the specified string</summary>
    /// <param name="s">this string</param>
    /// <param name="caseExample">The casing example to which to conform this string</param>
    /// <param name="repeatExample">If true, the case example will be used on a repeating basis if shorter than this string</param>
    /// <returns>A version of this string with casing conformed to that of the specified string</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string ConformCase(this string s, string caseExample, bool repeatExample = false)
    {
        if (s == null || s.Length == 0) return "";
        else if (caseExample == null || caseExample.Length == 0) return s;

        StringBuilder sb = new StringBuilder(s.Length);

        if (s.Length <= caseExample.Length)
        {
            for (int x = 0; x < s.Length; x++)
                sb.Append(char.IsUpper(caseExample[x]) ? char.ToUpper(s[x]) : char.ToLower(s[x]));
        }
        else if (repeatExample)
        {
            for (int x = 0; x < s.Length; x++)
                sb.Append(char.IsUpper(caseExample[x % caseExample.Length]) ? char.ToUpper(s[x]) : char.ToLower(s[x]));
        }
        else
        {
            bool lastWasUpperCase = false;

            for (int x = 0; x < caseExample.Length; x++)
                sb.Append((lastWasUpperCase = char.IsUpper(caseExample[x])) ? char.ToUpper(s[x]) : char.ToLower(s[x]));

            sb.Append(lastWasUpperCase ? s.Substring(caseExample.Length).ToUpper() : s.Substring(caseExample.Length));
        }

        return sb.ToString();
    }

    /// <summary>Replaces all occurrences of the specified word within this string, disregarding occurrences within the middle of other words</summary>
    /// <param name="s">this string</param>
    /// <param name="word">The word to replace</param>
    /// <param name="ignoreCase">If true, matching will ignore case of the strings being compared</param>
    /// <param name="conformCase">If true, replacements will be conformed to the case of the occurrence of each word replaced</param>
    /// <returns>A version of this string with the specified word replaced</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string ReplaceWord(this string s, string word, string replacement, bool ignoreCase = false, bool conformCase = false)
    {
        if (s == null) return "";
        else if (word == null || word.Length == 0) return s;
        if (replacement == null) replacement = "";

        StringComparison comparisonType = ignoreCase ? StringComparison.CurrentCultureIgnoreCase : StringComparison.CurrentCulture;
        StringBuilder sb = new StringBuilder(s.Length + 10);

        int lastIndex = 0;
        int nextIndex;

        while ((nextIndex = s.IndexOf(word, lastIndex, comparisonType)) > -1)
        {
            if ((nextIndex == 0 || !char.IsLetterOrDigit(s[nextIndex - 1])) &&
                (nextIndex + word.Length >= s.Length || !char.IsLetterOrDigit(s[nextIndex + word.Length])))
                sb.Append(s.Substring(lastIndex, nextIndex - lastIndex)).Append(conformCase ? replacement.ConformCase(s.Substring(nextIndex, word.Length)) : replacement);
            else
                sb.Append(s.Substring(lastIndex, (nextIndex - lastIndex) + word.Length));
            lastIndex = nextIndex + word.Length;
        }

        sb.Append(s.Substring(lastIndex));
        
        return sb.ToString();
    }

    /// <summary>Returns a copy of this string restricted to the specified characters</summary>
    /// <param name="s">this string</param>
    /// <param name="characters">The characters to which to restrict (make unique for best performance)</param>
    /// <returns>A new string containing the characters from this one conforming to the restricted character list</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string RestrictToCharacters(this string s, string characters)
    {
        if (s == null || s.Length == 0 || characters == null || characters.Length == 0) return "";

        char[] sb = new char[s.Length];
        int count = 0;
        char c;

        if (s.Length <= 30 && characters.Length <= 16)
        {
            for (int x = 0; x < s.Length; x++)
                if (characters.IndexOf(c = s[x]) > -1)
                    sb[count++] = c;
        }
        else
        {
            CharHashSet set = new CharHashSet(characters);
            for (int x = 0; x < s.Length; x++)
                if (set.Contains(c = s[x]))
                    sb[count++] = c;
        } 

        return (count == 0) ? "" : new string(sb, 0, count);
    }

    /// <summary>Indicates whether this string contains only letters</summary>
    /// <param name="s">this string</param>
    /// <returns>true if this string is alphabetic-only, otherwise false</returns>
    public static bool IsAlphabetic(this string s)
    {
        if (s == null) return false;
        for (int x = s.Length - 1; x >= 0; x--)
            if (!char.IsLetter(s, x))
                return false;

        return true;
    }

    /// <summary>Indicates whether this string contains only number</summary>
    /// <param name="s">this string</param>
    /// <returns>true if this string is numeric-only, otherwise false</returns>
    public static bool IsNumeric(this string s)
    {
        if (s == null) return false;
        for (int x = s.Length - 1; x >= 0; x--)
            if (!char.IsDigit(s, x))
                return false;

        return true;
    }

    /// <summary>Indicates whether this string contains only letters and numbers</summary>
    /// <param name="s">this string</param>
    /// <returns>true if this string is alphanumeric-only, otherwise false</returns>
    public static bool IsAlphanumeric(this string s)
    {
        if (s == null) return false;
        for (int x = s.Length - 1; x >= 0; x--)
            if (!char.IsLetterOrDigit(s, x))
                return false;
        
        return true;
    }

    /// <summary>Indicates whether this string contains a valid GUID</summary>
    /// <param name="s">this string</param>
    /// <returns>true if this string can be parsed as a GUID, otherwise false</returns>
    public static bool IsGuid(this string s)
    {
        if (s == null) return false;
        Guid guid;
        return Guid.TryParse(s, out guid);
    }

    /// <summary>Converts this string to a GUID</summary>
    /// <param name="s">this string</param>
    /// <returns>A new GUID if this string can be parsed as a GUID, otherwise default(System.Guid)</returns>
    public static Guid ToGuid(this string s)
    {         
        if (s == null) return default(Guid);

        Guid guid;
        if (Guid.TryParse(s, out guid))
            return guid;
        else
            return default(Guid);
    }
}

#if (COLOCATE_EXTENSIONS || !GLOBAL_EXTENSIONS)
}
#endif
