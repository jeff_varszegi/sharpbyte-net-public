﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Runtime.CompilerServices;

#if COLOCATE_EXTENSIONS
namespace System
{
#elif !GLOBAL_EXTENSIONS
namespace SharpByte.Extensions
{
#endif

/// <summary>Contains utility/extension logic for numeric types</summary>
public static class NumericExtensions
{
    /// <summary>Gets a version of this number restricted to the specified range</summary>
    /// <param name="number">this number</param>
    /// <param name="lowerBound">The lower bound of the range</param>
    /// <param name="upperBound">The upper bound of the range</param>
    /// <returns>The value of this int, if it lies within the range; otherwise, the closest possible match</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int RestrictTo(this int number, int lowerBound, int upperBound)
    {
        return (lowerBound < upperBound) ? Math.Min(Math.Max(number, lowerBound), upperBound) : Math.Min(Math.Max(number, upperBound), lowerBound);
    }

    /// <summary>Gets a version of this number restricted to the specified range</summary>
    /// <param name="number">this number</param>
    /// <param name="lowerBound">The lower bound of the range</param>
    /// <param name="upperBound">The upper bound of the range</param>
    /// <returns>The value of this int, if it lies within the range; otherwise, the closest possible match</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static long RestrictTo(this long number, long lowerBound, long upperBound)
    {
        return (lowerBound < upperBound) ? Math.Min(Math.Max(number, lowerBound), upperBound) : Math.Min(Math.Max(number, upperBound), lowerBound);
    }
            
    /// <summary>Gets a version of this number restricted to the specified range</summary>
    /// <param name="number">this number</param>
    /// <param name="lowerBound">The lower bound of the range</param>
    /// <param name="upperBound">The upper bound of the range</param>
    /// <returns>The value of this int, if it lies within the range; otherwise, the closest possible match</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float RestrictTo(this float number, float lowerBound, float upperBound)
    {
        return (lowerBound < upperBound) ? Math.Min(Math.Max(number, lowerBound), upperBound) : Math.Min(Math.Max(number, upperBound), lowerBound);
    }
            
    /// <summary>Gets a version of this number restricted to the specified range</summary>
    /// <param name="number">this number</param>
    /// <param name="lowerBound">The lower bound of the range</param>
    /// <param name="upperBound">The upper bound of the range</param>
    /// <returns>The value of this int, if it lies within the range; otherwise, the closest possible match</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static double RestrictTo(this double number, double lowerBound, double upperBound)
    {
        return (lowerBound < upperBound) ? Math.Min(Math.Max(number, lowerBound), upperBound) : Math.Min(Math.Max(number, upperBound), lowerBound);
    }
}

#if (COLOCATE_EXTENSIONS || !GLOBAL_EXTENSIONS)
}
#endif
