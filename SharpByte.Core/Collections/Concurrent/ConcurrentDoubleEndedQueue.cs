﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *                                                                                                                                      *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

using SharpByte.Concurrency;

namespace SharpByte.Collections.Concurrent
{
    // TODO: Use lock-free techniques to further improve performance; see, e.g. ConcurrentQueue

    /// <summary>A non-concurrent, double-ended queue</summary>
    /// <typeparam name="T">The type for this collection</typeparam>
    public sealed class ConcurrentDoubleEndedQueue<T> : Queue<T>, IDoubleEndedQueue<T>
    {
        #region Inner classes

        private class ConcurrentDoubleEndedQueueEnumerator : IEnumerator<T>, System.Collections.IEnumerator
        {
            /// <summary>The queue enumerated by this enumerator</summary>
            private ConcurrentDoubleEndedQueue<T> queue;

            /// <summary>The current index pointed to by this enumerator</summary>
            private int index = -1;

            /// <summary>The current item pointed to by this enumerator</summary>
            private T current;

            /// <summary>Indicates whether an error condition has been found, such as enumerating off the end of the list</summary>
            private bool isError = false;

            /// <summary>Constructs a new instance</summary>
            /// <param name="_queue">The queue to enumerate</param>
            public ConcurrentDoubleEndedQueueEnumerator(ConcurrentDoubleEndedQueue<T> _queue)
            {
                queue = _queue;
                current = default(T);
            }

            /// <summary>Gets the currently pointed-to item</summary>
            public T Current
            {
                get
                {
                    return current;
                }
            }

            /// <summary>Disposes of this object</summary>
            public void Dispose() { }

            /// <summary>
            /// Gets the currently pointed-to item
            /// </summary>
            object System.Collections.IEnumerator.Current { get { return current; } }

            /// <summary>Moves to the next item to be enumerated</summary>
            /// <returns>The next item</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool MoveNext()
            {
                if (isError) return false;

                index++;

                if (!queue.TryGetItemAt(index, out current))
                {
                    isError = true;
                    current = default(T);
                    return false;
                }
                else
                {
                    return true;
                }
            }

            /// <summary>Resets this enumerator</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Reset()
            {
                index = -1;
                current = default(T);
                isError = false;
            }
        }

        #endregion Inner classes

        #region Fields and properties

        /// <summary>The default capacity for new instances</summary>
        public const int DefaultCapacity = 1000;

        /// <summary>The maximum capacity for new instances</summary>
        public const int MaximumInitialCapacity = 10000000;

        /// <summary>The maximum capacity for new instances</summary>
        public const int MinimumInitialCapacity = 10;

        /// <summary>The default growth factor</summary>
        public const double DefaultGrowthFactor = 5.0D;

        /// <summary>The minimum growth factor</summary>
        public const double MinimumGrowthFactor = 2.0D;

        /// <summary>The minimum growth factor</summary>
        private const double MaximumGrowthFactor = 20.0D;

        /// <summary>The maximum size at which to increase capacity</summary>
        private int maximumGrowthCapacity;

        /// <summary>The elements stored in this queue</summary>
        private T[] items;

        /// <summary>The index of the first item in the queue</summary>
        private int startIndex = 0;

        /// <summary>The index of the last item in the queue</summary>
        private int endIndex = -1;

        /// <summary>The number of items which can currently be stored in this queue without resizing</summary>
        private int capacity;

        /// <summary>The number by which the capacity is multiplied during resizing operations</summary>
        private double growthFactor;

        /// <summary>The number of items in this collection</summary>
        private int count = 0;

        /// <summary>The number of items in this collection</summary>
        public override int Count { get { return count; } }

        /// <summary>A synchronization construct used to coordinate reader-writer access</summary>
        private ILock _lock;

        #endregion Fields and properties

        /// <summary>Default constructor</summary>
        public ConcurrentDoubleEndedQueue() : this(DefaultCapacity, DefaultGrowthFactor, LockType.ReaderWriter) { }

        /// <summary>Creates a new instance</summary>
        /// <param name="initialCapacity">The initial capacity of the queue</param>
        /// <param name="initialGrowthFactor">The growth factor to use when increasing capacity</param>
        /// <param name="lockType">The type of lock to use in synchronization</param>
        public ConcurrentDoubleEndedQueue(int initialCapacity = DefaultCapacity, double initialGrowthFactor = DefaultGrowthFactor, LockType lockType = LockType.ReaderWriter)
        {
            capacity = initialCapacity.RestrictTo(MinimumInitialCapacity, MaximumInitialCapacity);
            items = new T[capacity];
            count = 0;

            growthFactor = growthFactor.RestrictTo(MinimumGrowthFactor, MaximumGrowthFactor);
            maximumGrowthCapacity = (int)(int.MaxValue / growthFactor);

            this._lock = Lock.CreateLock(lockType);
        }

        /// <summary>Creates a new instance</summary>
        /// <param name="initialCapacity">The initial capacity of the queue</param>
        /// <param name="initialGrowthFactor">The growth factor to use when increasing capacity</param>
        /// <param name="lockType">The type of lock to use in synchronization</param>
        public ConcurrentDoubleEndedQueue(int initialCapacity = DefaultCapacity, double initialGrowthFactor = DefaultGrowthFactor) : this(initialCapacity, initialGrowthFactor, LockType.ReaderWriter) { }

        /// <summary>Increases the capacity of this queue</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void IncreaseCapacity()
        {
            if (capacity == int.MaxValue) return;

            capacity = capacity < maximumGrowthCapacity ? (int)(capacity * growthFactor) : int.MaxValue;

            T[] newItems = new T[capacity];

            if (startIndex > endIndex)
            {
                Array.Copy(items, startIndex, newItems, 0, (count - startIndex));
                Array.Copy(items, 0, newItems, (count - startIndex), endIndex + 1);
            }
            else
                Array.Copy(items, startIndex, newItems, 0, count);

            items = newItems;
            startIndex = 0;
            endIndex = count - 1;
        }

        /// <summary>Adds an item to the front of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddFirst(T item)
        {
            if (AddToOutput(item)) return;
            _lock.EnterWriteLock();

            if (count == capacity)
            {
                IncreaseCapacity();
                startIndex = capacity - 1;
            }
            else
            {
                startIndex = startIndex == 0 ? capacity - 1 : startIndex - 1;
            }
            items[startIndex] = item;
            count++;

            _lock.ExitWriteLock();
        }

        /// <summary>Adds an item to the end of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddLast(T item)
        {
            if (AddToOutput(item)) return;
            _lock.EnterWriteLock();

            if (count == capacity)
            {
                IncreaseCapacity();
                endIndex++;
            }
            else
            {
                endIndex = endIndex == capacity - 1 ? 0 : endIndex + 1;
            }
            items[endIndex] = item;
            count++;

            _lock.ExitWriteLock();
        }

        /// <summary>Adds an item to the end of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Add(T item) { AddLast(item); }

        /// <summary>Attempts to peek at the first item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool TryPeekFirst(out T returnValue)
        {
            _lock.EnterReadLock();

            if (count == 0)
            {
                returnValue = default(T);
                _lock.ExitReadLock();
                return false;
            }
            else
            {
                returnValue = items[startIndex];
                _lock.ExitReadLock();
                return true;
            }
        }

        /// <summary>Gets the item from the front of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T PeekFirst()
        {
            T returnValue;
            TryPeekFirst(out returnValue);
            return returnValue;
        }

        /// <summary>Attempts to peek at the first item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool TryPeek(out T returnValue)
        {
            return TryPeekFirst(out returnValue);
        }

        /// <summary>Gets the item at the front of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override T Peek()
        {
            T returnValue;
            TryPeekFirst(out returnValue);
            return returnValue;
        }

        /// <summary>Attempts to peek at the last item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool TryPeekLast(out T returnValue)
        {
            _lock.EnterReadLock();

            if (count == 0)
            {
                returnValue = default(T);
                _lock.ExitReadLock();
                return false;
            }
            else
            {
                returnValue = items[endIndex];
                _lock.ExitReadLock();
                return true;
            }
        }

        /// <summary>Gets the item from the end of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T PeekLast()
        {
            T returnValue;
            TryPeekLast(out returnValue);
            return returnValue;
        }

        /// <summary>Attempts to remove the first item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool TryRemoveFirst(out T returnValue)
        {
            _lock.EnterWriteLock();

            if (count == 0)
            {
                returnValue = default(T);
                _lock.ExitWriteLock();
                return false;
            }

            returnValue = items[startIndex];
            items[startIndex] = default(T);
            startIndex = (startIndex + 1) % capacity;
            count--;
            _lock.ExitWriteLock();
            return true;
        }

        /// <summary>Attempts to remove the first item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool TryRemove(out T returnValue)
        {
            return TryRemoveFirst(out returnValue);
        }

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T RemoveFirst()
        {
            T returnValue;
            TryRemoveFirst(out returnValue);
            return returnValue;
        }

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override T Remove() 
        {
            T returnValue;
            TryRemoveFirst(out returnValue);
            return returnValue;
        }

        /// <summary>Attempts to remove the last item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool TryRemoveLast(out T returnValue)
        {
            _lock.EnterWriteLock();
            if (count == 0)
            {
                returnValue = default(T);
                _lock.ExitWriteLock();
                return false;
            }

            returnValue = items[endIndex];
            items[endIndex] = default(T);
            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
            count--;
            _lock.ExitWriteLock();
            return true;
        }

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T RemoveLast()
        {
            T returnValue;
            TryRemoveLast(out returnValue);
            return returnValue;
        }

        /// <summary>Gets the index of the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>The index of the specified item, or -1 if not found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override int IndexOf(T item)
        {
            _lock.EnterReadLock();
            if (count == 0) return -1;

            int stopIndex = (endIndex + 1) % capacity;
            int returnValue = 0;
            for (int x = startIndex; x != stopIndex; x = (x + 1) % capacity)
            {
                if (object.ReferenceEquals(items[x], item)) {
                    _lock.ExitReadLock();
                    return returnValue;
                }
                else
                    returnValue++;
            }

            _lock.ExitReadLock();
            return -1;
        }

        /// <summary>Inserts the item at the specified index</summary>
        /// <param name="index">The index at which to insert the item</param>
        /// <param name="item">The item to insert</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Insert(int index, T item)
        {
            if (index < 0) return;
            _lock.EnterWriteLock();
            if (index >= count) index = count;

            if (count == capacity)
            {
                IncreaseCapacity();
                if (index < count)
                    Array.Copy(items, index, items, index + 1, count - index);
                items[index] = item;
            }
            else
            {
                int translatedIndex = (startIndex + index) % items.Length;

                if (index < count)
                {
                    if (translatedIndex >= startIndex)
                    {
                        if (startIndex > 0)
                        {
                            Array.Copy(items, startIndex, items, startIndex - 1, (translatedIndex - startIndex) + 1);
                            startIndex--;
                        }
                        else
                        {
                            // Shift forward into the hole
                            Array.Copy(items, translatedIndex, items, translatedIndex + 1, endIndex - translatedIndex);
                            endIndex++;
                        }
                    }
                    else
                    {
                        // Shift forward into the hole
                        Array.Copy(items, translatedIndex, items, translatedIndex + 1, endIndex - translatedIndex);
                        endIndex++;
                    }
                }

                items[translatedIndex] = item;
            }

            count++;
            _lock.ExitWriteLock();
        }

        /// <summary>Removes the item at the specified index</summary>
        /// <param name="index">The index at which to remove the item</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void RemoveAt(int index)
        {
            if (index < 0) return;
            
            _lock.EnterWriteLock();
            if (index >= count)
            {
                _lock.ExitWriteLock();
                return;
            }

            int translatedIndex = (startIndex + index) % capacity;

            if (translatedIndex == startIndex)
            {
                items[startIndex] = default(T);
                startIndex = (startIndex + 1) % capacity;
            }
            else if (translatedIndex > startIndex)
            {
                Array.Copy(items, startIndex, items, startIndex + 1, translatedIndex - startIndex);
                items[startIndex] = default(T);
                startIndex = (startIndex + 1) % capacity;
            }
            else if (translatedIndex == endIndex)
            {
                items[endIndex] = default(T);
                endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
            }
            else // translatedIndex < EndIndex
            {
                Array.Copy(items, translatedIndex + 1, items, translatedIndex, endIndex - translatedIndex);
                items[endIndex] = default(T);
                endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
            }

            count--;
            _lock.ExitWriteLock();
        }

        /// <summary>Gets or sets the item at a specified index</summary>
        /// <param name="index">The index at which to access the item</param>
        /// <returns>The item at the specified index</returns>
        public override T this[int index]
        {
            get
            {
                T returnValue = default(T);
                if (index < 0) return returnValue;
                _lock.EnterReadLock();
                if (index < count)
                    returnValue = items[(startIndex + index) % capacity];
                _lock.ExitReadLock();
                return returnValue;
            }

            set
            {
                if (index < 0) return;
                _lock.EnterWriteLock();
                if (index < count)
                    items[(startIndex + index) % capacity] = value;
                _lock.ExitWriteLock();
            }
        }

        /// <summary>Tries to get an item at the specified index</summary>
        /// <param name="index">The index at which to get the item</param>
        /// <param name="returnValue">The return value</param>
        /// <returns>true if the access was successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool TryGetItemAt(int index, out T returnValue)
        {
            if (index < 0)
            {
                returnValue = default(T);
                return false;
            }

            _lock.EnterReadLock();
            if (index < count)
            {
                returnValue = items[(startIndex + index) % capacity];
                _lock.ExitReadLock();
                return true;
            }
            else
            {
                returnValue = default(T);
                _lock.ExitReadLock();
                return false;
            }
        }

        /// <summary>Removes all items from this collection</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Clear()
        {
            _lock.EnterWriteLock();
            Array.Clear(items, 0, capacity);
            count = 0;
            startIndex = 0;
            endIndex = -1;
            _lock.ExitWriteLock();
        }

        /// <summary>Indicates whether this collection contains the specified item</summary>
        /// <param name="item">The item for which to check</param>
        /// <returns>true if the specified item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool Contains(T item)
        {
            _lock.EnterReadLock();

            if (count == 0)
            {
                _lock.ExitReadLock();
                return false;
            }

            if (startIndex > endIndex)
            {
                for (int x = startIndex; x < capacity; x++)
                {
                    if (object.ReferenceEquals(item, items[x]))
                    {
                        _lock.ExitReadLock();
                        return true;
                    }
                }

                for (int x = 0; x <= endIndex; x++)
                {
                    if (object.ReferenceEquals(item, items[x]))
                    {
                        _lock.ExitReadLock();
                        return true;
                    }
                }
            }
            else
            {
                for (int x = startIndex; x <= endIndex; x++)
                    if (object.ReferenceEquals(item, items[x]))
                    {
                        _lock.ExitReadLock();
                        return true;
                    }
            }

            _lock.ExitReadLock();
            return false;
        }

        /// <summary>Copies the items in this collection to the specified array</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index in the target array at which to begin copying</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null || arrayIndex < 0 || arrayIndex >= array.Length) return;
            int arrayLength = array.Length;

            _lock.EnterReadLock();

            int c = this.count;
            for (int x = 0; x < c && arrayIndex < arrayLength; x++, arrayIndex++)
                array[arrayIndex] = items[(startIndex + x) % capacity];
            
            _lock.ExitReadLock();
        }

        /// <summary>Attempts to remove the specified item from this list</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>true if the specified item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool Remove(T item)
        {
            _lock.EnterWriteLock();
            if (count == 0)
            {
                _lock.ExitWriteLock();
                return false;
            } 

            if (startIndex > endIndex)
            {
                for (int translatedIndex = startIndex; translatedIndex < capacity; translatedIndex++)
                {
                    if (object.ReferenceEquals(item, items[translatedIndex]))
                    {
                        if (translatedIndex == startIndex)
                        {
                            items[startIndex] = default(T);
                            startIndex = (startIndex + 1) % capacity;
                        }
                        else // if (translatedIndex > StartIndex)
                        {
                            Array.Copy(items, startIndex, items, startIndex + 1, translatedIndex - startIndex);
                            items[startIndex] = default(T);
                            startIndex = (startIndex + 1) % capacity;
                        }

                        _lock.ExitWriteLock();
                        return true;
                    }
                }

                for (int translatedIndex = 0; translatedIndex <= endIndex; translatedIndex++)
                {
                    if (object.ReferenceEquals(item, items[translatedIndex]))
                    {
                        if (translatedIndex == endIndex)
                        {
                            items[endIndex] = default(T);
                            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
                        }
                        else // translatedIndex < EndIndex
                        {
                            Array.Copy(items, translatedIndex + 1, items, translatedIndex, endIndex - translatedIndex);
                            items[endIndex] = default(T);
                            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
                        }

                        _lock.ExitWriteLock();
                        return true;
                    }
                }

            }
            else
            {
                for (int translatedIndex = startIndex; translatedIndex <= endIndex; translatedIndex++)
                {
                    if (object.ReferenceEquals(item, items[translatedIndex]))
                    {
                        if (translatedIndex == startIndex)
                        {
                            items[startIndex] = default(T);
                            startIndex = (startIndex + 1) % capacity;
                        }
                        else if (translatedIndex > startIndex)
                        {
                            Array.Copy(items, startIndex, items, startIndex + 1, translatedIndex - startIndex);
                            items[startIndex] = default(T);
                            startIndex = (startIndex + 1) % capacity;
                        }
                        else if (translatedIndex == endIndex)
                        {
                            items[endIndex] = default(T);
                            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
                        }
                        else // translatedIndex < EndIndex
                        {
                            Array.Copy(items, translatedIndex + 1, items, translatedIndex, endIndex - translatedIndex);
                            items[endIndex] = default(T);
                            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
                        }

                        _lock.ExitWriteLock();
                        return true;
                    }
                }
            }

            _lock.ExitWriteLock();
            return false;
        }

        /// <summary>Gets an enumerator for this list</summary>
        /// <returns>An enumerator</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override IEnumerator<T> GetEnumerator()
        {
            return new ConcurrentDoubleEndedQueueEnumerator(this);
        }

        /// <summary>Gets an enumerator for this list</summary>
        /// <returns>An enumerator</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new ConcurrentDoubleEndedQueueEnumerator(this);
        }

        public void Print(bool includeRaw)
        {
            for (int y = 0; y < count; y++)
                System.Console.WriteLine("[" + y.ToString().PadLeft(3, '0') + "] " + this[y]);
            Console.WriteLine();

            if (includeRaw)
                for (int y = 0; y < capacity; y++)
                    System.Console.WriteLine("{" + y.ToString().PadLeft(3, '0') + "} " + items[y]);

            Console.WriteLine();
        }
        /* Used in testing
        */

    }
}
