﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

using SharpByte.Concurrency;

namespace SharpByte.Collections.Concurrent
{
    // TODO: Convert to circular list
    // TODO: Use lock-free techniques to further improve performance; see, e.g. ConcurrentQueue
    // See also System.Collections.Generic.SynchronizedCollection<string> in System.ServiceModel

    /// <summary>Implements a concurrent list with support for multiple concurrent readers</summary>
    /// <typeparam name="T">The type for this list</typeparam>
    public sealed class ConcurrentList<T> : IList<T>
    {
        #region Inner classes

        /// <summary>An enumerator which uses synchronization</summary>
        private class LockingEnumerator : IEnumerator<T>, System.Collections.IEnumerator
        {
            /// <summary>A locking construct</summary>
//            private SpinningLock _lock;
            private SpinLock _lock = new SpinLock(false);

            /// <summary>The list to enumerate</summary>
            private ConcurrentList<T> list;

            /// <summary>The current index pointed to by this enumerator</summary>
            private int index = -1;

            /// <summary>The current item pointed to by this enumerator</summary>
            private T current = default(T);

            /// <summary>Indicates whether an error condition has been found, such as enumerating off the end of the list</summary>
            private bool isError = false;

            /// <summary>Constructs a new instance</summary>
            /// <param name="_list">The list to enumerate</param>
            public LockingEnumerator(ConcurrentList<T> _list)
            {
                list = _list;
//                _lock = new SpinningLock();
            }

            /// <summary>Gets the currently pointed-to item</summary>
            public T Current
            {
                get 
                {
                    // _lock.EnterWriteLock();
                    bool lockTaken = false;
                    _lock.TryEnter(-1, ref lockTaken);

                    if (isError)
                    {
                        // _lock.ExitWriteLock();
                        _lock.Exit();

                        throw new InvalidOperationException("The enumerator is in an error state");
                    }
                    else
                    {
                        T returnValue = current;

                        // _lock.ExitWriteLock();
                        _lock.Exit();

                        return returnValue;
                    }
                }
            }

            /// <summary>Disposes of this object</summary>
            public void Dispose() {}

            /// <summary>
            /// Gets the currently pointed-to item
            /// </summary>
            object System.Collections.IEnumerator.Current
            {
                get 
                {
                    // _lock.EnterReadLock();
                    bool lockTaken = false;
                    _lock.TryEnter(-1, ref lockTaken);

                    if (isError)
                    {
                        // _lock.ExitReadLock();
                        _lock.Exit();

                        throw new InvalidOperationException("The enumerator is in an error state");
                    }
                    else
                    {
                        T returnValue = current;

                        // _lock.ExitReadLock();
                        _lock.Exit();

                        return returnValue;
                    }
                }
            }

            /// <summary>Moves to the next item to be enumerated</summary>
            /// <returns>The next item</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool MoveNext()
            {
                // _lock.EnterWriteLock();

                if (isError) 
                {
                    // _lock.ExitWriteLock();
                    return false;
                }

                if (list.EnterReadLock())
                {
                    index++;

                    if (index >= list.Count)
                    {
                        isError = true;
                        current = list.DefaultValue;
                    }
                    else
                    {
                        current = list[index];
                    }

                    list.ExitReadLock();
                    // _lock.ExitWriteLock();

                    return !isError;
                }
                else
                {
                    isError = true;
                    current = list.DefaultValue;
                    // _lock.ExitWriteLock();
                    return false;
                }
            }

            /// <summary>Resets this enumerator</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Reset()
            {
                // _lock.EnterWriteLock();
                index = -1;
                current = list.DefaultValue;
                isError = false;
                // _lock.ExitWriteLock();
            }
        }

        #endregion Inner classes

        #region Fields and properties

        /// <summary>The default value for the type</summary>
        public readonly T DefaultValue = default(T);

        /// <summary>Used to compare items for equality during search operations, etc.</summary>
        private EqualityComparer<T> equalityComparer = EqualityComparer<T>.Default;

        /// <summary>The default capacity to use when constructing the underlying list</summary>
        private const int DefaultCapacity = 10;

        /// <summary>The capacity increase multiplier</summary>
        private const float MinimumIncreaseFactor = 2.0f;

        /// <summary>The capacity increase multiplier</summary>
        private float increaseFactor = 2.0f;

        /// <summary>The capacity increase multiplier</summary>
        public float IncreaseFactor
        {
            get
            {
                return increaseFactor;
            }

            set
            {
                increaseFactor = Math.Max(MinimumIncreaseFactor, value);
            }
        }

        /// <summary>A default, empty array to use when initializing a list</summary>
        private static readonly T[] EmptyItems = new T[0];

        /// <summary>The concurrency locking object</summary>
//        private ILock _lock;

        private SpinLock _lock = new SpinLock(false);

        /// <summary>The underlying collection of elements in the list</summary>
        private T[] items;

        /// <summary>The number of elements actually stored in the list</summary>
        private int size;

        /// <summary>If true, the default value for the underlying type will be returned if a failed access occurs, and additions past the end of the list will always be safely made at the next available index</summary>
        public bool IsSafeIndexingEnabled { get; set; }

        /// <summary>If true, enumerators will enumerate over a copy of this collection's data to decrease contention; if false, a synchronized enumerator type will be used instead</summary>
        public bool IsEnumeratorCopyingEnabled { get; set; }

        #endregion Fields and properties

        /// <summary>Constructs a new instance</summary>
        /// <param name="capacity">The starting capacity</param>
        /// <param name="lockType">The type of lock to use for synchronization</param>
        public ConcurrentList(int capacity, LockType lockType) 
        {
//            _lock = Lock.CreateLock(lockType);

            if (capacity <= 0)
                items = EmptyItems;
            else
                items = new T[capacity];

            IsSafeIndexingEnabled = true;
            IsEnumeratorCopyingEnabled = true;
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="capacity">The starting capacity</param>
        public ConcurrentList(int capacity) : this(capacity, LockType.Spinning) {}

        /// <summary>Constructs a new instance</summary>
        /// <param name="lockType">The type of lock to use for synchronization</param>
        public ConcurrentList(LockType lockType) : this(DefaultCapacity, lockType) {}

        /// <summary>Constructs a new instance</summary>
        public ConcurrentList() : this(DefaultCapacity, LockType.Spinning) {}

        /// <summary>Constructs a new instance containing the specified items</summary>
        /// <param name="_items">The items to initially store</param>
        /// <param name="lockType">The type of lock to use for synchronization</param>
        public ConcurrentList(IEnumerable<T> _items, LockType lockType)
        {
//            _lock = Lock.CreateLock(lockType);

            IsSafeIndexingEnabled = true;
            IsEnumeratorCopyingEnabled = true;

            if (items == null)
            {
                items = EmptyItems;
                return;
            }

            items = new T[DefaultCapacity];
            size = 0;

            IEnumerator<T> enumerator = _items.GetEnumerator();
            while (enumerator.MoveNext())
                AddItem(enumerator.Current);
        }

        /// <summary>Constructs a new instance containing the specified items</summary>
        /// <param name="items">The items to initially store</param>
        public ConcurrentList(ICollection<T> _items) : this(_items, LockType.Spinning) { }

        /// <summary>Constructs a new instance containing the specified items</summary>
        /// <param name="items">The items to initially store</param>
        /// <param name="lockType">The type of lock to use for synchronization</param>
        public ConcurrentList(T[] _items, LockType lockType)
        {
  //          _lock = Lock.CreateLock(lockType);

            IsSafeIndexingEnabled = true;
            IsEnumeratorCopyingEnabled = true;

            if (_items == null)
            {
                items = EmptyItems;
                return;
            }

            int count = _items.Length;
            items = new T[Math.Max(count, DefaultCapacity)];
            _items.CopyTo(items, 0);
            size = count;
        }

        /// <summary>Constructs a new instance containing the specified items</summary>
        /// <param name="items">The items to initially store</param>
        public ConcurrentList(T[] _items) : this(_items, LockType.Spinning) { }

        /// <summary>Enters a read lock for this list</summary>
        /// <returns>true if successfully entered, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool EnterReadLock()
        {
            // _lock.EnterReadLock();
            bool lockTaken = false;
            _lock.TryEnter(-1, ref lockTaken);

            return true;
        }

        /// <summary>Exits a read lock for this list</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void ExitReadLock()
        {
            // _lock.ExitReadLock();
            _lock.Exit();
        }

        /// <summary>Gets the index of the specified object in this list</summary>
        /// <param name="item">The item for which to check</param>
        /// <returns>The index of the item</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int IndexOf(T item)
        {
            // _lock.EnterReadLock();
            bool lockTaken = false;
            _lock.TryEnter(-1, ref lockTaken);

            int returnValue = Array.IndexOf<T>(items, item);
            // _lock.ExitReadLock();
            _lock.Exit();

            return returnValue;
        }

        /// <summary>Increases the capacity of the list</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void IncreaseCapacity()
        {
            int capacity = items.Length;
            if (capacity == 0) 
            {
                items = new T[DefaultCapacity];
            }
            else 
            {
                if (capacity < DefaultCapacity)
                    capacity = (int)(DefaultCapacity * increaseFactor);
                else
                    capacity = (int)(capacity * increaseFactor);
                T[] newItems = new T[capacity];
                items.CopyTo(newItems, 0);
                items = newItems;
            }
        }

        /// <summary>Inserts an item at the specified index, if it lies within the list or at the next index; otherwise simply returns</summary>
        /// <param name="index">The index at which to insert</param>
        /// <param name="item">The item to insert</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Insert(int index, T item)
        {
            if (index < 0) return;

            // _lock.EnterWriteLock();
            bool lockTaken = false;
            _lock.TryEnter(-1, ref lockTaken);

            if (index > size)
            {
                if (IsSafeIndexingEnabled)
                {
                    index = size;
                }
                else
                {
                    // _lock.ExitWriteLock();
                    _lock.Exit();

                    return;
                }
            }

            if (size == items.Length)
                IncreaseCapacity();

            if (index < size)
                Array.Copy(items, index, items, index + 1, size - index);
            items[index] = item;
            size++;

            // _lock.ExitWriteLock();
            _lock.Exit();
        }

        /// <summary>Removes an item at the specified index</summary>
        /// <param name="index">The index at which to remove</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RemoveAt(int index)
        {
            if (index < 0) return;

            // _lock.EnterWriteLock();
            bool lockTaken = false;
            _lock.TryEnter(-1, ref lockTaken);

            bool pastEnd = index >= size;
            if (!pastEnd)
            {
                Array.Copy(items, index + 1, items, index, (size - index) - 1);
                size--;
                items[size] = DefaultValue;
            }

            // _lock.ExitWriteLock();
            _lock.Exit();
        }

        /// <summary>Gets the item at the specified index, or either a default if none is available (and the list so configured), or throws an exception in such a case</summary>
        /// <param name="index">The index for which to fetch the item</param>
        /// <returns>An item from this list at the specified index, if available</returns>
        public T this[int index]
        {
            get
            {
                if (index < 0) throw new IndexOutOfRangeException();

                // _lock.EnterReadLock();
                bool lockTaken = false;
                _lock.TryEnter(-1, ref lockTaken);

                if (index >= size)
                {
                    // _lock.ExitReadLock();
                    _lock.Exit();

                    if (IsSafeIndexingEnabled)
                        return DefaultValue;
                    else
                        throw new IndexOutOfRangeException("Attempt to access index " + index + " in list with " + size + " elements");
                }
                else
                {
                    T returnValue = items[index];
                    // _lock.ExitReadLock();
                    _lock.Exit();

                    return returnValue;
                }
            }

            set
            {
                if (index < 0) throw new IndexOutOfRangeException();

                // _lock.EnterWriteLock();
                bool lockTaken = false;
                _lock.TryEnter(-1, ref lockTaken);

                if (index >= size)
                {
                    if (IsSafeIndexingEnabled)
                    {
                        AddItem(value);
                    }
                    else
                    {
                        // _lock.ExitWriteLock();
                        _lock.Exit();

                        throw new IndexOutOfRangeException("Attempt to access index " + index + " in list with " + size + " elements");
                    }
                }
                else
                {
                    items[index] = value;
                }

                // _lock.ExitWriteLock();
                _lock.Exit();
            }
        }

        /// <summary>Adds the specified item to this list</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void AddItem(T item)
        {
            if (size == items.Length)
                IncreaseCapacity();
            items[size++] = item;
        }

        /// <summary>Adds an item</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Add(T item)
        {
            // _lock.EnterWriteLock();
            bool lockTaken = false;
            _lock.TryEnter(-1, ref lockTaken);
            
            AddItem(item);

            // _lock.ExitWriteLock();
            _lock.Exit();
        }

        /// <summary>Removes all items from this list</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            // _lock.EnterWriteLock();
            bool lockTaken = false;
            _lock.TryEnter(-1, ref lockTaken);

            Array.Clear(items, 0, size);
            size = 0;

            // _lock.ExitWriteLock();
            _lock.Exit();
        }

        /// <summary>Indicates whether this list contains the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>true if the item is found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(T item)
        {
            if (((object)item) == null)
            {
                // _lock.EnterReadLock();
                bool lockTaken = false;
                _lock.TryEnter(-1, ref lockTaken);

                for (int x = 0; x < size; x++)
                {
                    if (((object)items[x]) == null)
                    {
                        // _lock.ExitReadLock();
                        _lock.Exit();

                        return true;
                    }
                }

                // _lock.ExitReadLock();
                _lock.Exit();

                return false;
            }
            else
            {
                // _lock.EnterReadLock();
                bool lockTaken = false;
                _lock.TryEnter(-1, ref lockTaken);

                for (int x = 0; x < size; x++)
                {
                    if (equalityComparer.Equals(item, items[x]))
                    {
                        // _lock.ExitReadLock();
                        _lock.Exit();

                        return true;
                    }
                }
                // _lock.ExitReadLock();
                _lock.Exit();
                return false;
            }
        }

        /// <summary>
        /// Copies this list to the specified array
        /// </summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index at which to start copying</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null) throw new ArgumentNullException();
            else if (arrayIndex < 0) throw new IndexOutOfRangeException();
            else if (arrayIndex >= array.Length) return;

            // _lock.EnterReadLock();
            bool lockTaken = false;
            _lock.TryEnter(-1, ref lockTaken);

            if (size == 0)
            {
                // _lock.ExitReadLock();
                _lock.Exit();

                return;
            }

            Array.Copy(items, 0, array, arrayIndex, size);

            // _lock.ExitReadLock();
            _lock.Exit();
        }

        /// <summary>Gets the number of items in this list</summary>
        public int Count
        {
            get 
            {
                return size;
                /* The below is technically synchronized while the above is not, but it would always be a dirty read anyway...
                // _lock.EnterReadLock();
                int returnValue = size;
                // _lock.ExitReadLock();
                return returnValue; */
            }
        }

        /// <summary>Indicates whether this collection is read-only</summary>
        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Removes the specified item from this list
        /// </summary>
        /// <param name="item">The item to remove</param>
        /// <returns>true if found and removed, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Remove(T item)
        {
            // _lock.EnterWriteLock();
            bool lockTaken = false;
            _lock.TryEnter(-1, ref lockTaken);

            int index = Array.IndexOf<T>(items, item);
            if (index < 0)
            {
                // _lock.ExitWriteLock();
                _lock.Exit();

                return false;
            }

            Array.Copy(items, index + 1, items, index, size - index);
            size--;
            items[size] = DefaultValue;

            // _lock.ExitWriteLock();
            _lock.Exit();

            return true;
        }

        /// <summary>Constructs a non-synchronized copy of this list</summary>
        /// <returns>A non-thread-safe copy ofthis list</returns>
        //public List<T> Copy()
        //{
        //    // _lock.EnterReadLock();
        //    List<T> copy = new FastList<T>(items);
        //    // _lock.ExitReadLock();
        //    return copy;
        //}

        /// <summary>
        /// Gets an enumerator
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerator<T> GetEnumerator()
        {
            if (IsEnumeratorCopyingEnabled) 
            {
                // _lock.EnterReadLock();
                bool lockTaken = false;
                _lock.TryEnter(-1, ref lockTaken);

                T[] copy = new T[size];
                if (size > 0)
                    Array.Copy(items, 0, copy, 0, size);

                // _lock.ExitReadLock();
                _lock.Exit();

                return new ArrayEnumerator<T>(copy);
            }
            else 
            {
                return new LockingEnumerator(this);
            }
        }

        /// <summary>
        /// Gets an enumerator
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            if (IsEnumeratorCopyingEnabled)
            {
                // _lock.EnterReadLock();
                bool lockTaken = false;
                _lock.TryEnter(-1, ref lockTaken);

                T[] copy = new T[size];
                if (size > 0)
                    Array.Copy(items, 0, copy, 0, size);

                // _lock.ExitReadLock();
                _lock.Exit();

                return new ArrayEnumerator<T>(copy);
            }
            else
            {
                return new LockingEnumerator(this);
            }
        }
    }
}
