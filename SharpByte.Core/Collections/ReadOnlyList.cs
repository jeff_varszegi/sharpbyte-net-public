﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections
{
    /// <summary>Contains extension/utility logic for the ReadOnlyList class</summary>
    public static class ReadOnlyListExtensions
    {
        /// <summary>Creates a read-only list with the elements in this enumerable</summary>
        /// <typeparam name="T">The element type</typeparam>
        /// <param name="source">this enumerable</param>
        /// <returns>A read-only list containing the elements in this enumerable</returns>
        public static ReadOnlyList<T> ToReadOnlyList<T>(this IEnumerable<T> source)
        {
            if (source == null) return ReadOnlyList<T>.Empty;
            else return new ReadOnlyList<T>(source);
        }
    }
    
    /// <summary>A read-only list, providing greater safety than the IReadOnlList interface</summary>
    /// <typeparam name="T">The element type for the list</typeparam>
    public sealed class ReadOnlyList<T> : IList<T>
    {
        /// <summary>An enumerator for a read-only list</summary>
        /// <typeparam name="T">The list's element data type</typeparam>
        private sealed class ReadOnlyListEnumerator : IEnumerator<T>
        {
            /// <summary>The list for this enumerator</summary>
            private ReadOnlyList<T> list;

            /// <summary>The current item</summary>
            private T current;

            /// <summary>The index of the current item</summary>
            private int index = -1;

            /// <summary>The current item</summary>
            public T Current { get { return current; } }

            /// <summary>Constructs a new instance</summary>
            /// <param name="l">The list for this enumerator</param>
            public ReadOnlyListEnumerator(ReadOnlyList<T> l)
            {
                list = l;
            }

            /// <summary>Disposes of this object</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Dispose() { }

            /// <summary>The current item</summary>
            object System.Collections.IEnumerator.Current { get { return current; } }

            /// <summary>Moves to the next item</summary>
            /// <returns>true if successful, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool MoveNext()
            {
                if (index >= list.Count - 1)
                {
                    current = default(T);
                    return false;
                }
                else
                {
                    current = list.items[++index];
                    return true;
                }
            }

            /// <summary>Resets this enumerator</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Reset()
            {
                index = -1;
                current = default(T);
            }
        }

        /// <summary>An empty singleton instance</summary>
        public static readonly ReadOnlyList<T> Empty = new ReadOnlyList<T>();

        /// <summary>The elements of this list</summary>
        private T[] items;

        /// <summary>The number of elements in this list</summary>
        private int count;

        /// <summary>Constructs a new, empty instance</summary>
        public ReadOnlyList() 
        {
            count = 0;
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="i">The items for the list</param>
        public ReadOnlyList(T[] i)
        {
            if (i == null)
            {
                count = 0;
                return;
            }

            count = i.Length;
            items = new T[count];
            Array.Copy(i, items, count);
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="i">The items for the list</param>
        public ReadOnlyList(IEnumerable<T> i)
        {
            if (i == null)
            {
                count = 0;
                return;
            }

            try
            {
                items = i.ToArray();
                count = items.Length;
                return;
            } catch { }

            count = 0;
        }

        /// <summary>Gets the index of the first appearance of the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>The index of the specified item's first appearance, or -1 if not found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int IndexOf(T item)
        {
            for (int x = 0; x < count; x++)
                if (object.ReferenceEquals(item, items[x]))
                    return x;

            return -1;
        }

        /// <summary>Throws an exception, as this is a read-only list</summary>
        /// <param name="index">The index at which to insert</param>
        /// <param name="item">The item to insert</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Insert(int index, T item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception, as this is a read-only list</summary>
        /// <param name="index">The index at which to remove</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RemoveAt(int index)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Gets the item at the specified index (attempts to set will throw an exception, as this is a read-only list)</summary>
        /// <param name="index">The index at which to get the item</param>
        /// <returns>The specified item</returns>
        public T this[int index]
        {
            get
            {
                return items[index];
            }
            set
            {
                throw new InvalidOperationException("Attempt to mutate a read-only collection");
            }
        }

        /// <summary>Throws an exception</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Add(T item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Checks whether this list contains the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>true if the item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(T item)
        {
            for (int x = 0; x < count; x++)
                if (object.ReferenceEquals(item, items[x]))
                    return true;

            return false;
        }

        /// <summary>Copies this list's elements to the specified array</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index at which to start</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null) return;
            else if (arrayIndex < 0) arrayIndex = 0;

            Array.Copy(items, 0, array, arrayIndex, Math.Min(count, array.Length - arrayIndex));
        }

        /// <summary>The number of items in this list</summary>
        public int Count { get { return count; } }

        /// <summary>Returns true</summary>
        public bool IsReadOnly { get { return true; } }

        /// <summary>Throws an exception</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>Does not return a value; throws an exception in all cases</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Remove(T item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Gets an enumerator for this list</summary>
        /// <returns>An enumerator for this list</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerator<T> GetEnumerator()
        {
            if (count == 0) return EmptyListEnumerator<T>.Instance;
            else return new ReadOnlyListEnumerator(this);
        }

        /// <summary>Gets an enumerator for this list</summary>
        /// <returns>An enumerator for this list</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            if (count == 0) return EmptyListEnumerator<T>.Instance;
            else return new ReadOnlyListEnumerator(this);
        }
    }
}
