﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System.Collections.Generic;

namespace SharpByte.Collections
{
    /// <summary>A queue</summary>
    /// <typeparam name="T">The type handled by this queue</typeparam>
    public interface IQueue<T> : IList<T>
    {
        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        T Remove();

        /// <summary>Gets the item at the front of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        T Peek();

        /// <summary>The queue output strategy for this queue</summary>
        QueueOutputStrategy OutputStrategy { get; set; }

        /// <summary>Adds a collection (which may be a queue) to receive items from this queue</summary>
        /// <param name="output">The output to add</param>
        void AddOutput(ICollection<T> output);

        /// <summary>Removes an output</summary>
        /// <param name="output">The output to remove</param>
        void RemoveOutput(ICollection<T> output);

        /// <summary>Removes all output from this queue</summary>
        void ClearOutput();

        /// <summary>Flushes this queue to any defined output queues; if no output queues exist, does nothing. Note that if output queues throw an exception, 
        /// items may be flushed without being sent to output; if this is possible due to the underlying queue implementation, first use ToList() to take a copy of this queue's contents</summary>
        /// <returns>true if the items in this queue were added to any output queue(s) successfully or this queue contained no items, otherwise false</returns>
        bool Flush();
    }
}
