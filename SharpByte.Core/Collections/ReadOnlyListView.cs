﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections
{
    /// <summary>Contains extension/utility logic for the ReadOnlyListView class</summary>
    public static class ReadOnlyListViewExtensions
    {
        /// <summary>Creates a read-only view of the elements in this enumerable</summary>
        /// <typeparam name="T">The element type</typeparam>
        /// <param name="source">this enumerable</param>
        /// <returns>A read-only list containing the elements in this enumerable</returns>
        public static ReadOnlyListView<T> ToReadOnlyListView<T>(this IList<T> source)
        {
            if (source == null) return ReadOnlyListView<T>.Empty;
            else return new ReadOnlyListView<T>(source);
        }
    }
    
    /// <summary>A read-only view of a list, providing greater safety than the IReadOnlyList interface. 
    /// The underlying data structure can continue to be modified, but this view is safe to pass to using code that 
    /// should only be able to read the data</summary>
    /// <typeparam name="T">The type for the list/view</typeparam>
    public sealed class ReadOnlyListView<T> : IList<T>
    {
        /// <summary>An enumerator for a read-only list</summary>
        /// <typeparam name="T">The list's element data type</typeparam>
        private sealed class ReadOnlyListViewEnumerator : IEnumerator<T>
        {
            /// <summary>The list for this enumerator</summary>
            private ReadOnlyListView<T> list;

            /// <summary>The current item</summary>
            private T current;

            /// <summary>The index of the current item</summary>
            private int index = -1;

            /// <summary>Indicates whether this enumerator has passed the end of the list</summary>
            private bool pastEnd = false;

            /// <summary>The current item</summary>
            public T Current { get { return current; } }

            /// <summary>Constructs a new instance</summary>
            /// <param name="l">The list for this enumerator</param>
            public ReadOnlyListViewEnumerator(ReadOnlyListView<T> l)
            {
                list = l;
            }

            /// <summary>Disposes of this object</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Dispose() { }

            /// <summary>The current item</summary>
            object System.Collections.IEnumerator.Current { get { return current; } }

            /// <summary>Moves to the next item</summary>
            /// <returns>true if successful, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool MoveNext()
            {
                if (pastEnd) return false;

                if (index >= list.list.Count - 1)
                {
                    pastEnd = true;
                    current = default(T);
                    return false;
                }
                else
                {
                    try {
                        current = list.list[++index];
                        return true;
                    } catch {}
                    pastEnd = true;
                    current = default(T);
                    return false;
                }
            }

            /// <summary>Resets this enumerator</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Reset()
            {
                index = -1;
                pastEnd = false;
                current = default(T);
            }
        }

        /// <summary>An empty singleton instance</summary>
        public static readonly ReadOnlyListView<T> Empty = new ReadOnlyListView<T>();

        /// <summary>The underlying list</summary>
        private IList<T> list;

        /// <summary>Constructs a new, empty instance</summary>
        public ReadOnlyListView() 
        {
            list = new List<T>();
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="l">The underlying list</param>
        public ReadOnlyListView(IList<T> l)
        {
            if (l == null)
            {
                list = new List<T>();
                return;
            }

            list = l;
        }

        /// <summary>Gets the index of the first appearance of the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>The index of the specified item's first appearance, or -1 if not found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int IndexOf(T item)
        {
            return list.IndexOf(item);
        }

        /// <summary>Throws an exception, as this is a read-only list</summary>
        /// <param name="index">The index at which to insert</param>
        /// <param name="item">The item to insert</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Insert(int index, T item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception, as this is a read-only list</summary>
        /// <param name="index">The index at which to remove</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RemoveAt(int index)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Gets the item at the specified index (attempts to set will throw an exception, as this is a read-only list)</summary>
        /// <param name="index">The index at which to get the item</param>
        /// <returns>The specified item</returns>
        public T this[int index]
        {
            get
            {
                return list[index];
            }
            set
            {
                throw new InvalidOperationException("Attempt to mutate a read-only collection");
            }
        }

        /// <summary>Throws an exception</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Add(T item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Checks whether this list contains the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>true if the item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(T item)
        {
            return list.Contains(item);
        }

        /// <summary>Copies this list's elements to the specified array</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index at which to start</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(T[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        /// <summary>The number of items in this list</summary>
        public int Count { get { return list.Count; } }

        /// <summary>Returns true</summary>
        public bool IsReadOnly { get { return true; } }

        /// <summary>Throws an exception</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>Does not return a value; throws an exception in all cases</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Remove(T item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Gets an enumerator for this list</summary>
        /// <returns>An enumerator for this list</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerator<T> GetEnumerator()
        {
            return new ReadOnlyListViewEnumerator(this);
        }

        /// <summary>Gets an enumerator for this list</summary>
        /// <returns>An enumerator for this list</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new ReadOnlyListViewEnumerator(this);
        }
    }
}
