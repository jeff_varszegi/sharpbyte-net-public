﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi, www.sharpbyte.net                                                                          *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections.Specialized
{
    /// <summary>A set implementation optimized for checking for existence of small to medium numbers of strings</summary>
    public sealed class StringHashSet : ISet<string>
    {
        #region Inner classes

        /// <summary>A list of values for a particular hashed address</summary>
        private struct Entry
        {
            /// <summary>A list of items for this entry</summary>
            public string[] Items;

            /// <summary>The number of items in this entry</summary>
            public int Count;

            /// <summary>Adds an item to this entry</summary>
            /// <param name="item">The item to add</param>
            /// <returns>true if added, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool Add(string item)
            {
                if (Items == null)
                {
                    Items = new string[2];
                    Items[0] = item;
                    Count = 1;
                    return true;
                }
                else
                {
                    for (int x = 0; x < Count; x++)
                        if (Items[x] == item) return false;

                    if (Count == Items.Length)
                    {
                        string[] newItems = new string[Count * 2];
                        Array.Copy(Items, newItems, Count);
                        Items = newItems;
                    }

                    Items[Count++] = item;
                    return true;
                }
            }

            /// <summary>Indicates whether this entry contains the specified item</summary>
            /// <param name="item">The item for which to check</param>
            /// <returns>true if the item was found, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool Contains(string item)
            {
                for (int x = 0; x < Count; x++)
                    if (Items[x] == item) return true;

                return false;
            }

            /// <summary>Removes the specified item from this entry</summary>
            /// <param name="item">The item to remove</param>
            /// <returns>true if found and removed, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool Remove(string item)
            {
                for (int x = 0; x < Count; x++)
                {
                    if (Items[x] == item)
                    {
                        if (x < --Count)
                            Array.Copy(Items, x + 1, Items, x, Count - x);

                        return true;
                    }
                }

                return false;
            }
        }

        #endregion Inner classes

        /// <summary>The default starting capacity</summary>
        private const int DefaultStartingCapacity = 64;

        /// <summary>The minimum starting capacity</summary>
        private const int MinimumStartingCapacity = 16;

        /// <summary>The maximum entry count</summary>
        private const int MaximumEntryCount = 131072;

        /// <summary>Entries for hashed items</summary>
        private Entry[] entries;

        /// <summary>The number of entries</summary>
        private int entryCount;

        /// <summary>The number of items in this set</summary>
        private int count = 0;

        #region Properties

        /// <summary>Gets the number of items in this set</summary>
        public int Count { get { return count; } }

        /// <summary>Indicates whether this collection is read-only</summary>
        public bool IsReadOnly { get { return false; } }

        #endregion Properties

        #region Constructor methods

        /// <summary>Default constructor</summary>
        public StringHashSet()
        {
            entryCount = 64;
            entries = new Entry[entryCount];
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="capacity">The capacity to use</param>
        public StringHashSet(int capacity)
        {
            if (capacity < MinimumStartingCapacity) capacity = MinimumStartingCapacity;

            if (capacity <= 128)
                entryCount = capacity * 3;
            else if (capacity <= 1024)
                entryCount = capacity * 2;
            else
                entryCount = capacity;

            entries = new Entry[entryCount];
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="items">The items used to initialize the set</param>
        public StringHashSet(IEnumerable<string> items)
        {
            if (items == null)
            {
                entryCount = MinimumStartingCapacity * 3;
                entries = new Entry[entryCount];
                return;
            }

            int itemCount = items.Count();

            if (itemCount <= 128)
                entryCount = itemCount * 3;
            else if (itemCount <= 1024)
                entryCount = itemCount * 2;
            else
                entryCount = itemCount;

            entries = new Entry[entryCount];

            foreach (string item in items)
                this.Add(item);
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="items">The items used to initialize the set</param>
        public StringHashSet(string[] items)
        {
            if (items == null)
            {
                entryCount = MinimumStartingCapacity * 3;
                entries = new Entry[entryCount];
                return;
            }

            int itemCount = items.Length;

            if (itemCount <= 128)
                entryCount = itemCount * 2;
            else if (itemCount <= 1024)
                entryCount = itemCount * 2;
            else
                entryCount = itemCount;

            entries = new Entry[entryCount];

            for(int x = 0; x < items.Length; x++) 
                this.Add(items[x]);
        }

        #endregion Constructor methods

        #region Collection methods

        /// <summary>Adds the specified item to this set</summary>
        /// <param name="item">The item to add</param>
        /// <returns>true if the item was added, false if already in the set</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Add(string item)
        {
            // TODO: Add ability to grow

            if (entries[item.GetHashCodeFastAbsolute() % entryCount].Add(item))
            {
                count++;
                return true;
            }
            else
                return false;
        }

        /// <summary>Adds the specified item to this collection, if not already present</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ICollection<string>.Add(string item)
        {
            if (entries[item.GetHashCodeFastAbsolute() % entryCount].Add(item))
                count++;
        }

        /// <summary>Removes all items from this set</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            Array.Clear(entries, 0, entryCount);
            count = 0;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void GetEntry(string key, out Entry entry)
        {
            int hashCode = key.GetHashCodeFastAbsolute() % entryCount;
            entry = entries[hashCode];
        }

        /// <summary>Indicates whether this set contains the specified item</summary>
        /// <param name="item">The item for which to check</param>
        /// <returns>true if the item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(string item)
        {
            //            return entries[item.GetHashCodeFastAbsolute() % entryCount].Contains(item);

            int hashcode = item.GetHashCodeFastAbsolute() % entryCount;

            for (int x = entries[hashcode].Count - 1; x >= 0; x--)
                    if (object.ReferenceEquals(entries[hashcode].Items[x], item)) return true;

                return false;
        }

        /// <summary>Copies the elements of this set to the specified array, in no dependable order</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index at which to begin copying</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(string[] array, int arrayIndex)
        {
            if (array == null || arrayIndex >= array.Length) return;
            int copyCount = array.Length - arrayIndex;

            int x, y, itemCount;
            for (x = 0; x < entryCount && copyCount > 0; x++)
            {
                itemCount = entries[x].Count;
                if (itemCount > 0)
                {
                    for (y = 0; y < itemCount && copyCount > 0; y++)
                    {
                        array[arrayIndex++] = entries[x].Items[y];
                        copyCount--;
                    }
                }
            }
        }

        /// <summary>Removes the specified item from this collection</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>true if found and removed, otherwise false (i.e. not found)</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Remove(string item)
        {
            if (entries[item.GetHashCodeFastAbsolute() % entryCount].Remove(item))
            {
                count--;
                return true;
            }
            else
                return false;
        }

        #endregion Collection methods

        #region Methods based on set operations

        /// <summary>Removes all items in the specified other collection from this one</summary>
        /// <param name="other">The collection the elements of which to remove from this one</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ExceptWith(IEnumerable<string> other)
        {
            foreach (string item in other)
                this.Remove(item);
        }

        /// <summary>Removes items from this collection which are not in the specified collection</summary>
        /// <param name="other">The collection the elements of which to restrict this one</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void IntersectWith(IEnumerable<string> other)
        {
            foreach (string item in this)
                if (!other.Contains(item))
                    this.Remove(item);
        }

        /// <summary>Indicates whether this set is a proper/strict subset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in this collection plus at least one more appears in the other collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsProperSubsetOf(IEnumerable<string> other)
        {
            foreach (string item in this)
                if (!other.Contains(item))
                    return false;
            return this.Count < other.Count();
        }

        /// <summary>Indicates whether this set is a proper/strict superset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in the other collection plus at least one more appears in the this collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsProperSupersetOf(IEnumerable<string> other)
        {
            foreach (string item in other)
                if (!this.Contains(item))
                    return false;
            return this.Count > other.Count();
        }

        /// <summary>Indicates whether this set is a subset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in this collection appears in the other collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsSubsetOf(IEnumerable<string> other)
        {
            foreach (string item in this)
                if (!other.Contains(item))
                    return false;
            return true;
        }

        /// <summary>Indicates whether this set is a superset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in the other collection appears in the this collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsSupersetOf(IEnumerable<string> other)
        {
            foreach (string item in other)
                if (!this.Contains(item))
                    return false;
            return true;
        }

        /// <summary>Indicates whether this collection contains at least one element equal to one in the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if the collections have at least one equal element, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Overlaps(IEnumerable<string> other)
        {
            if (this.Count <= other.Count())
            {
                foreach (string item in this)
                    if (other.Contains(item))
                        return true;
                return false;
            }
            else
            {
                foreach (string item in other)
                    if (this.Contains(item))
                        return true;
                return false;
            }
        }

        /// <summary>Indicates whether this collection and the specified other one contain the same elements</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if the sets cotnain the same elemnts, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool SetEquals(IEnumerable<string> other)
        {
            int otherCount = 0;
            foreach (string item in other)
            {
                if (!this.Contains(item))
                    return false;
                otherCount++;
            }

            return count == otherCount;
        }

        /// <summary>Updates this collection to only contain items that are contained in either this or the specified collection, but not both</summary>
        /// <param name="other">The collection to compare</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SymmetricExceptWith(IEnumerable<string> other)
        {
            foreach (string item in other)
                if (!this.Remove(item))
                    this.Add(item);
        }

        /// <summary>Adds elements from the specified collection to this one, if missing</summary>
        /// <param name="other">The collection to compare</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void UnionWith(IEnumerable<string> other)
        {
            foreach (string item in other)
                this.Add(item);
        }

        #endregion Methods based on set operations

        #region Enumerators

        /// <summary>Gets an enumerator for this collection</summary>
        /// <returns>An enumerator for this collection</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerator<string> GetEnumerator()
        {
            string[] itemArray = new string[count];
            this.CopyTo(itemArray, 0);
            return new ArrayEnumerator<string>(itemArray);
        }

        /// <summary>Gets an enumerator for this collection</summary>
        /// <returns>An enumerator for this collection</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        IEnumerator IEnumerable.GetEnumerator()
        {
            string[] itemArray = new string[count];
            this.CopyTo(itemArray, 0);
            return new ArrayEnumerator<string>(itemArray);
        }

        #endregion Enumerators
    }
}
