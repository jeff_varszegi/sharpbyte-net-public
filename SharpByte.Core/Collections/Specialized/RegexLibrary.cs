﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;

namespace SharpByte.Collections.Specialized
{
    /// <summary>A collection of long-lived objects which reduces lock contention</summary>
    public sealed class RegexLibrary
    {
        #region Singleton instance

        /// <summary>A singleton instance</summary>
        private static RegexLibrary defaultInstance;

        /// <summary>A synchronization locking object</summary>
        private static object defaultInstanceLock = new object();

        /// <summary>Gets a singleton instance</summary>
        public static RegexLibrary Default
        {
            get 
            {
                if (defaultInstance == null)
                    lock (defaultInstanceLock)
                        if (defaultInstance == null)
                            defaultInstance = new RegexLibrary();
                return defaultInstance;   
            }
        }

        #endregion

        /// <summary>Gets a dictionary key for the specified regex</summary>
        /// <param name="pattern">The regex pattern for which to get the key</param>
        /// <param name="options">Regex options to add to the key</param>
        /// <returns>A dictionary key for the specified regex pattern and options</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string GetKey(string pattern, RegexOptions options)
        {
            return
                pattern +
                (((options & RegexOptions.Compiled) != 0) ? "1" : "0") +
                (((options & RegexOptions.CultureInvariant) != 0) ? "1" : "0") +
                (((options & RegexOptions.ECMAScript) != 0) ? "1" : "0") +
                (((options & RegexOptions.ExplicitCapture) != 0) ? "1" : "0") +
                (((options & RegexOptions.IgnoreCase) != 0) ? "1" : "0") +
                (((options & RegexOptions.IgnorePatternWhitespace) != 0) ? "1" : "0") +
                (((options & RegexOptions.Multiline) != 0) ? "1" : "0") +
                (((options & RegexOptions.RightToLeft) != 0) ? "1" : "0") +
                (((options & RegexOptions.Singleline) != 0) ? "1" : "0");
        }

        /// <summary>Inner collections</summary>
        private ConcurrentDictionary<string, Regex>[] values;

        /// <summary>Used to track whether a bucket has been initialized</summary>
        private object[] locks;

        /// <summary>Used to track whether a bucket has been initialized</summary>
        private bool[] initializationFlags;

        /// <summary>Synchronization objects</summary>
        private ConcurrentDictionary<string, object>[] valueLocks;

        /// <summary>The minimum partition count to use</summary>
        private const int MinimumPartitionCount = 16;

        /// <summary>The number of partitions/internal collections</summary>
        private int partitionCount;

        /// <summary>Controls whether uncompiled regexes are stored for future reuse</summary>
        public bool IsUncompiledStorageEnabled { get; set; }

        /// <summary>Constructs a new instance</summary>
        /// <param name="partitions">The number of partitions to use</param>
        /// <param name="uncompiledStorageEnabled">If true, uncompiled regexes will also be stored</param>
        public RegexLibrary(int partitions, bool uncompiledStorageEnabled = true)
        {
            partitionCount = Math.Max(partitions, MinimumPartitionCount);
            IsUncompiledStorageEnabled = uncompiledStorageEnabled;

            values = new ConcurrentDictionary<string, Regex>[partitionCount];
            valueLocks = new ConcurrentDictionary<string, object>[partitionCount];
            locks = new object[partitionCount];
            for (int x = 0; x < partitionCount; x++)
                locks[x] = new object();
            initializationFlags = new bool[partitionCount];
        }

        /// <summary>Constructs a new instance</summary>
        public RegexLibrary() : this(128) {}

        /// <summary>Removes all regexes from this library</summary>
        public void Clear()
        {
            for (int x = 0; x < partitionCount; x++)
            {
                Monitor.Enter(locks[x]);
                if (initializationFlags[x])
                {
                    initializationFlags[x] = false;
                    values[x].Clear();
                }
            }

            for (int x = partitionCount - 1; x >= 0; x--)
                Monitor.Exit(locks[x]);
        }

        /// <summary>Gets or creates a regex</summary>
        /// <param name="pattern">The pattern to use in constructing the regex</param>
        /// <param name="options">The options to use in constructing the regex</param>
        /// <returns>A regex with the specified pattern and options</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Regex GetRegex(string pattern, RegexOptions options = RegexOptions.None)
        {
            if (pattern == null || pattern.Length == 0) throw new ArgumentException("Invalid (null or zero-length) pattern");
            Regex regex;

            if (!IsUncompiledStorageEnabled && ((options & RegexOptions.Compiled) == 0))
                return new Regex(pattern, options);

            string key = GetKey(pattern, options);
            int index = key.GetHashCodeFastAbsolute() % partitionCount;
            ConcurrentDictionary<string, Regex> regexCollection;

            if (!initializationFlags[index])
            {
                lock (locks[index])
                {
                    if (!initializationFlags[index])
                    {
                        regexCollection = new ConcurrentDictionary<string, Regex>();
                        values[index] = regexCollection;
                        valueLocks[index] = new ConcurrentDictionary<string, object>();
                        valueLocks[index][key] = new object();
                        initializationFlags[index] = true;

                        regex = new Regex(pattern, options);
                        regexCollection[key] = regex;

                        return regex;
                    }
                }
            }

            regexCollection = values[index];
            if (regexCollection.TryGetValue(key, out regex))
                return regex;

            lock (valueLocks[index].GetOrAdd(key, new object()))
            {
                if (regexCollection.TryGetValue(key, out regex))
                    return regex;

                regexCollection[key] = regex = new Regex(pattern, options);
            }

            return regex;
        }

    }
}
