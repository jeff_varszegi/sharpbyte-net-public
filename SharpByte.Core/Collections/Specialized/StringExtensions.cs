﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;

namespace SharpByte.Collections.Specialized
{
    /// <summary>Contains extension/utility logic for string objects</summary>
    public static partial class StringExtensions
    {
        /* [MethodImpl(MethodImplOptions.AggressiveInlining)]
           public unsafe static bool EqualsFast(this string s, string t)
           {
               // if (object.ReferenceEquals(s, t)) return true;
               //  if ((Object)a==(Object)b) return true;

               int length = s.Length, x;
               if (length != t.Length) return false;

               unsafe
               {
                   fixed (char* text1 = s, text2 = t)
                   {
                       char* charPointer1 = text1;
                       char* charPointer2 = text2;

                       for (x = s.Length >> 7; x > 0; x--)
                       {
                           if (*(long*)charPointer1 != *(long*)charPointer2) return false;

                           if (*(long*)(charPointer1+4) != *(long*)(charPointer2+4)) return false;
                           if (*(long*)(charPointer1+8) != *(long*)(charPointer2+8)) return false;
                           if (*(long*)(charPointer1+12) != *(long*)(charPointer2+12)) return false;
                           if (*(long*)(charPointer1+16) != *(long*)(charPointer2+16)) return false;
                           if (*(long*)(charPointer1+20) != *(long*)(charPointer2+20)) return false;
                           if (*(long*)(charPointer1+24) != *(long*)(charPointer2+24)) return false;
                           if (*(long*)(charPointer1+28) != *(long*)(charPointer2+28)) return false;

                           if (*(long*)(charPointer1+32) != *(long*)(charPointer2+32)) return false;
                           if (*(long*)(charPointer1+36) != *(long*)(charPointer2+36)) return false;
                           if (*(long*)(charPointer1+40) != *(long*)(charPointer2+40)) return false;
                           if (*(long*)(charPointer1+44) != *(long*)(charPointer2+44)) return false;
                           if (*(long*)(charPointer1+48) != *(long*)(charPointer2+48)) return false;
                           if (*(long*)(charPointer1+52) != *(long*)(charPointer2+52)) return false;
                           if (*(long*)(charPointer1+56) != *(long*)(charPointer2+56)) return false;
                           if (*(long*)(charPointer1+60) != *(long*)(charPointer2+60)) return false;

                           if (*(long*)(charPointer1+64) != *(long*)(charPointer2+64)) return false;
                           if (*(long*)(charPointer1+68) != *(long*)(charPointer2+68)) return false;
                           if (*(long*)(charPointer1+72) != *(long*)(charPointer2+72)) return false;
                           if (*(long*)(charPointer1+76) != *(long*)(charPointer2+76)) return false;
                           if (*(long*)(charPointer1+80) != *(long*)(charPointer2+80)) return false;
                           if (*(long*)(charPointer1+84) != *(long*)(charPointer2+84)) return false;

                           if (*(long*)(charPointer1+88) != *(long*)(charPointer2+88)) return false;
                           if (*(long*)(charPointer1+92) != *(long*)(charPointer2+92)) return false;
                           if (*(long*)(charPointer1+96) != *(long*)(charPointer2+96)) return false;
                           if (*(long*)(charPointer1+100) != *(long*)(charPointer2+100)) return false;
                           if (*(long*)(charPointer1+104) != *(long*)(charPointer2+104)) return false;
                           if (*(long*)(charPointer1+108) != *(long*)(charPointer2+108)) return false;
                           if (*(long*)(charPointer1+112) != *(long*)(charPointer2+112)) return false;

                           if (*(long*)(charPointer1+116) != *(long*)(charPointer2+116)) return false;
                           if (*(long*)(charPointer1+120) != *(long*)(charPointer2+120)) return false;
                           if (*(long*)(charPointer1+124) != *(long*)(charPointer2+124)) return false;

                           charPointer1 += 128;
                           charPointer2 += 128;
                       }

                       for (x = s.Length >> 4; x > 0; x--)
                       {
                           if (*(long*)charPointer1 != *(long*)charPointer2) return false;
                           if (*(long*)(charPointer1+4) != *(long*)(charPointer2+4)) return false;
                           if (*(long*)(charPointer1+8) != *(long*)(charPointer2+8)) return false;
                           if (*(long*)(charPointer1+12) != *(long*)(charPointer2+12)) return false;

                           charPointer1 += 16;
                           charPointer2 += 16;
                       }

                       length = length % 16;
                       while (length > 0) 
                       {
                           if (*(int*)charPointer1 != *(int*)charPointer2) break;
                           charPointer1 += 2;
                           charPointer2 += 2;
                           length -= 2;
                       }

                       return (length <= 0);
                   }

               }
           } */

        /// <summary>Gets a hash code with performance optimizations, suitable for use in data structures but not cryptography</summary>
        /// <param name="s">this string</param>
        /// <returns>A hash code for this string</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int GetHashCodeFast(this string s)
        {
            if (s == null) return 0;

            int hashValue1 = 5381;
            int hashValue2 = 5381;
            int x;

            unsafe
            {
                fixed (char* text = s)
                {
                    char* charPointer = text;

                    #region 1. Step through characters 32 at a time

                    for (x = s.Length >> 5; x > 0; x--)
                    {
                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[0];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[1];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[2];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[3];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[4];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[5];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[6];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[7];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[8];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[9];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[10];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[11];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[12];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[13];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[14];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[15];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[16];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[17];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[18];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[19];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[20];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[21];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[22];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[23];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[24];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[25];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[26];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[27];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[28];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[29];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[30];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[31];

                        charPointer += 32;
                    }

                    #endregion

                    #region 2. Step through remaining characters 8 at a time

                    for (x = (s.Length % 32) >> 3; x > 0; x--)
                    {
                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[0];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[1];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[2];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[3];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[4];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[5];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[6];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[7];

                        charPointer += 8;
                    }

                    #endregion

                    #region 3. Step through remaining characters 2 at a time

                    /* int character;
                       while ((character = characterPointer[0]) != 0) {
                           hashValue1 = ((hashValue1 << 5) + hashValue1) ^ character;
                           character = characterPointer[1];
                           if (character == 0)
                               break;
                           hashValue2 = ((hashValue2 << 5) + hashValue2) ^ character;
                           characterPointer += 2;
                       } */

                    while (charPointer[0] != 0)
                    {
                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[0];
                        if (charPointer[1] == 0)
                            break;
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[1];
                        charPointer += 2;
                    }

                    #endregion

                    return hashValue1 + (hashValue2 * 1566083941);
                }
            }

        }

        /// <summary>Gets an absolute-value hash code with performance optimizations, suitable for use in data structures but not cryptography</summary>
        /// <param name="s">this string</param>
        /// <returns>A hash code for this string</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int GetHashCodeFastAbsolute(this string s)
        {
            if (s == null) return 0;

            int hashValue1 = 5381;
            int hashValue2 = 5381;
            int x;

            unsafe
            {
                fixed (char* text = s)
                {
                    char* charPointer = text;

                    #region 1. Step through characters 32 at a time

                    for (x = s.Length >> 5; x > 0; x--)
                    {
                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[0];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[1];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[2];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[3];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[4];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[5];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[6];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[7];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[8];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[9];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[10];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[11];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[12];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[13];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[14];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[15];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[16];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[17];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[18];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[19];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[20];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[21];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[22];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[23];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[24];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[25];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[26];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[27];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[28];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[29];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[30];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[31];

                        charPointer += 32;
                    }

                    #endregion

                    #region 2. Step through remaining characters 8 at a time

                    for (x = (s.Length % 32) >> 3; x > 0; x--)
                    {
                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[0];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[1];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[2];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[3];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[4];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[5];

                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[6];
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[7];

                        charPointer += 8;
                    }

                    #endregion

                    #region 3. Step through remaining characters 2 at a time

                    //int character;
                    //  while ((character = characterPointer[0]) != 0) {
                    //      hashValue1 = ((hashValue1 << 5) + hashValue1) ^ character;
                    //      character = characterPointer[1];
                    //      if (character == 0)
                    //          break;
                    //      hashValue2 = ((hashValue2 << 5) + hashValue2) ^ character;
                    //      characterPointer += 2;
                    //  }

                    while (charPointer[0] != 0)
                    {
                        hashValue1 = ((hashValue1 << 5) + hashValue1) ^ charPointer[0];
                        if (charPointer[1] == 0)
                            break;
                        hashValue2 = ((hashValue2 << 5) + hashValue2) ^ charPointer[1];
                        charPointer += 2;
                    }

                    #endregion

                    #region 4. Get the final hash value and return the absolute value of it

                    hashValue1 = hashValue1 + (hashValue2 * 1566083941);
                    return (hashValue1 + (hashValue1 >> 31)) ^ (hashValue1 >> 31); // faster than Math.Abs()

                    #endregion
                }
            }

        }

    }

}

