﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace SharpByte.Collections.Specialized
{
    /// <summary>Implements a lightweight, char-optimized set for unique-character operations</summary>
    public sealed class CharHashSet : ISet<char>
    {
        #region Inner classes

        /// <summary>A list of values for a particular hashed address</summary>
        private struct Entry
        {
            /// <summary>A list of items for this entry</summary>
            public char[] Items;

            /// <summary>The number of items in this entry</summary>
            public int Count;

            /// <summary>Adds an item to this entry</summary>
            /// <param name="item">The item to add</param>
            /// <returns>true if added, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool Add(char item)
            {
                if (Items == null)
                {
                    Items = new char[2];
                    Items[0] = item;
                    Count = 1;
                    return true;
                }
                else
                {
                    for (int x = 0; x < Count; x++)
                        if (Items[x] == item) return false;

                    if (Count == Items.Length)
                    {
                        char[] newItems = new char[Count * 2];
                        Array.Copy(Items, newItems, Count);
                        Items = newItems;
                    }

                    Items[Count++] = item;
                    return true;
                }
            }

            /// <summary>Indicates whether this entry contains the specified item</summary>
            /// <param name="item">The item for which to check</param>
            /// <returns>true if the item was found, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool Contains(char item)
            {
                for (int x = 0; x < Count; x++)
                    if (Items[x] == item) return true;

                return false;
            }

            /// <summary>Removes the specified item from this entry</summary>
            /// <param name="item">The item to remove</param>
            /// <returns>true if found and removed, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool Remove(char item)
            {
                for (int x = 0; x < Count; x++)
                {
                    if (Items[x] == item)
                    {
                        if (x < --Count)
                            Array.Copy(Items, x + 1, Items, x, Count - x);

                        return true;
                    }
                }

                return false;
            }
        }

        #endregion Inner classes

        /// <summary>The default starting capacity</summary>
        private const int DefaultStartingCapacity = 64;

        /// <summary>The minimum starting capacity</summary>
        private const int MinimumStartingCapacity = 10;

        /// <summary>The maximum entry count</summary>
        private const int MaximumEntryCount = 65536;

        /// <summary>Entries for hashed items</summary>
        private Entry[] entries;

        /// <summary>The number of entries</summary>
        private int entryCount;

        /// <summary>The number of items in this set</summary>
        private int count = 0;

        #region Properties

        /// <summary>Gets the number of items in this set</summary>
        public int Count { get { return count; } }

        /// <summary>Indicates whether this collection is read-only</summary>
        public bool IsReadOnly { get { return false; } }

        #endregion Properties

        #region Constructor methods

        /// <summary>Default constructor</summary>
        private CharHashSet() { }

        /// <summary>Constructs a new instance</summary>
        /// <param name="capacity">The capacity to use</param>
        public CharHashSet(int capacity)
        {
            if (capacity < MinimumStartingCapacity) capacity = MinimumStartingCapacity;

            if (capacity <= 512)
                entryCount = capacity * 2;
            else
                entryCount = capacity;

            entries = new Entry[entryCount];
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="characters">The characters used to initialize the set</param>
        public CharHashSet(string characters)
        {
            if (characters == null)
            {
                entryCount = MinimumStartingCapacity * 2;
                entries = new Entry[entryCount];
                return;
            }

            if (characters.Length <= 512)
                entryCount = characters.Length * 2;
            else
                entryCount = Math.Min(characters.Length, 4096);

            for (int x = 0; x < characters.Length; x++)
                this.Add(characters[x]);       
        }

        #endregion Constructor methods

        #region Collection methods

        /// <summary>Adds the specified item to this set</summary>
        /// <param name="item">The item to add</param>
        /// <returns>true if the item was added, false if already in the set</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Add(char item)
        {
            if (entries[item % entryCount].Add(item))
            {
                count++;
                return true;
            }
            else
                return false;
        }

        /// <summary>Adds the specified item to this collection, if not already present</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        void ICollection<char>.Add(char item)
        {
            // TODO: Add ability to grow number of entries

            if (entries[item % entryCount].Add(item))
                count++;
        }

        /// <summary>Removes all items from this set</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            Array.Clear(entries, 0, entryCount);
            count = 0;
        }

        /// <summary>Indicates whether this set contains the specified item</summary>
        /// <param name="item">The item for which to check</param>
        /// <returns>true if the item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(char item)
        {
            return entries[item % entryCount].Contains(item);
        }

        /// <summary>Copies the elements of this set to the specified array, in no dependable order</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index at which to begin copying</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(char[] array, int arrayIndex)
        {
            if (array == null || arrayIndex >= array.Length) return;
            int copyCount = array.Length - arrayIndex;

            int x, y, itemCount;
            for (x = 0; x < entryCount && copyCount > 0; x++)
            {
                itemCount = entries[x].Count;
                if (itemCount > 0)
                {
                    for (y = 0; y < itemCount && copyCount > 0; y++)
                    {
                        array[arrayIndex++] = entries[x].Items[y];
                        copyCount--;
                    }
                }
            }
        }

        /// <summary>Removes the specified item from this collection</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>true if found and removed, otherwise false (i.e. not found)</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Remove(char item)
        {
            if (entries[item % entryCount].Remove(item))
            {
                count--;
                return true;
            }
            else
                return false;
        }

        #endregion Collection methods

        #region Methods based on set operations

        /// <summary>Removes all items in the specified other collection from this one</summary>
        /// <param name="other">The collection the elements of which to remove from this one</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ExceptWith(IEnumerable<char> other)
        {
            foreach (char item in other)
                this.Remove(item);
        }

        /// <summary>Removes items from this collection which are not in the specified collection</summary>
        /// <param name="other">The collection the elements of which to restrict this one</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void IntersectWith(IEnumerable<char> other)
        {
            foreach (char item in this)
                if (!other.Contains<char>(item))
                    this.Remove(item);
        }

        /// <summary>Indicates whether this set is a proper/strict subset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in this collection plus at least one more appears in the other collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsProperSubsetOf(IEnumerable<char> other)
        {
            foreach (char item in this)
                if (!other.Contains<char>(item))
                    return false;
            return this.Count < other.Count();
        }

        /// <summary>Indicates whether this set is a proper/strict superset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in the other collection plus at least one more appears in the this collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsProperSupersetOf(IEnumerable<char> other)
        {
            foreach (char item in other)
                if (!this.Contains(item))
                    return false;
            return this.Count > other.Count();
        }

        /// <summary>Indicates whether this set is a subset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in this collection appears in the other collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsSubsetOf(IEnumerable<char> other)
        {
            foreach (char item in this)
                if (!other.Contains<char>(item))
                    return false;
            return true;
        }

        /// <summary>Indicates whether this set is a superset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in the other collection appears in the this collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsSupersetOf(IEnumerable<char> other)
        {
            foreach (char item in other)
                if (!this.Contains(item))
                    return false;
            return true;
        }

        /// <summary>Indicates whether this collection contains at least one element equal to one in the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if the collections have at least one equal element, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Overlaps(IEnumerable<char> other)
        {
            if (this.Count <= other.Count())
            {
                foreach (char item in this)
                    if (other.Contains(item))
                        return true;
                return false;
            }
            else
            {
                foreach (char item in other)
                    if (this.Contains(item))
                        return true;
                return false;
            }
        }

        /// <summary>Indicates whether this collection and the specified other one contain the same elements</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if the sets cotnain the same elemnts, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool SetEquals(IEnumerable<char> other)
        {
            int otherCount = 0;
            foreach (char item in other)
            {
                if (!this.Contains(item))
                    return false;
                otherCount++;
            }

            return count == otherCount;
        }

        /// <summary>Updates this collection to only contain items that are contained in either this or the specified collection, but not both</summary>
        /// <param name="other">The collection to compare</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SymmetricExceptWith(IEnumerable<char> other)
        {
            foreach (char item in other)
                if (!this.Remove(item))
                    this.Add(item);
        }

        /// <summary>Adds elements from the specified collection to this one, if missing</summary>
        /// <param name="other">The collection to compare</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void UnionWith(IEnumerable<char> other)
        {
            foreach (char c in other)
                this.Add(c);
        }

        #endregion Methods based on set operations

        #region Enumerators

        /// <summary>Gets an enumerator for this collection</summary>
        /// <returns>An enumerator for this collection</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerator<char> GetEnumerator()
        {
            char[] itemArray = new char[count];
            this.CopyTo(itemArray, 0);
            return new ArrayEnumerator<char>(itemArray);
        }

        /// <summary>Gets an enumerator for this collection</summary>
        /// <returns>An enumerator for this collection</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        IEnumerator IEnumerable.GetEnumerator()
        {
            char[] itemArray = new char[count];
            this.CopyTo(itemArray, 0);
            return new ArrayEnumerator<char>(itemArray);
        }

        #endregion Enumerators

        #region Utility methods

        /// <summary>Returns a copy of the specified string containing only characters in this set</summary>
        /// <param name="s">The string to filter</param>
        /// <returns>A copy of the specified string containing only the characters in this set</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public string Filter(string s)
        {
            if (s == null) return "";

            StringBuilder sb = new StringBuilder(s.Length);

            char c;
            for (int x = 0; x < s.Length; x++)
                if (this.Contains(c = s[x]))
                    sb.Append(c);

            return sb.ToString();
        }

        #endregion
    }
}
