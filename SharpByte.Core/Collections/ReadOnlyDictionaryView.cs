﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections
{
    /// <summary>A read-only dictionary view, providing greater safety than the IReadOnlyDictionary interface. 
    /// The underlying data structure can continue to be modified, but this view is safe to pass to using code that 
    /// should only be able to read the data</summary>
    public sealed class ReadOnlyDictionaryView<TKey, TValue> : IDictionary<TKey, TValue>
    {
        /// <summary>An empty singleton instance</summary>
        public static readonly ReadOnlyDictionaryView<TKey, TValue> Empty = new ReadOnlyDictionaryView<TKey, TValue>();

        /// <summary>The dictionary wrapped by this instance</summary>
        private IDictionary<TKey, TValue> dictionary;

        /// <summary>Creates a new, empty instance</summary>
        public ReadOnlyDictionaryView() {
            dictionary = new Dictionary<TKey, TValue>();
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="d">The dictionary for which to create a read-only copy</param>
        public ReadOnlyDictionaryView(IDictionary<TKey, TValue> d)
        {
            if (d == null) dictionary = new Dictionary<TKey, TValue>();
            else dictionary = d;
        }

        /// <summary>Throws an exception</summary>
        /// <param name="key">The key</param>
        /// <param name="value">The value</param>
        public void Add(TKey key, TValue value)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Indicates whether this dictionary contains the specified key</summary>
        /// <param name="key">The key for which to check</param>
        /// <returns>true if the key was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool ContainsKey(TKey key)
        {
            return dictionary.ContainsKey(key);
        }

        /// <summary>Gets a collection of keys</summary>
        public ICollection<TKey> Keys
        {
            get 
            {
                return dictionary.Keys.ToList();
            }
        }

        /// <summary>
        /// Throws an exception
        /// </summary>
        /// <param name="key">The key for which to remove the value</param>
        /// <returns>Does not return; throws an exception</returns>
        public bool Remove(TKey key)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Tries to get the specified value</summary>
        /// <param name="key">The key for which to get the value</param>
        /// <param name="value">The value for the key, if any</param>
        /// <returns>true if the key was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool TryGetValue(TKey key, out TValue value)
        {
            return dictionary.TryGetValue(key, out value);
        }

        /// <summary>Values for this dictionary</summary>
        public ICollection<TValue> Values
        {
            get
            {
                return dictionary.Values.ToList();
            }
        }

        /// <summary>Gets a value by key</summary>
        /// <param name="key">The key for which to get the value</param>
        /// <returns>The specified value</returns>
        public TValue this[TKey key]
        {
            get
            {
                return dictionary[key];
            }
            set
            {
                throw new InvalidOperationException("Attempt to mutate a read-only collection");
            }
        }

        /// <summary>Throws an exception</summary>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception</summary>
        public void Clear()
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Checks for a key-value pair</summary>
        /// <param name="item">The pair for which to check</param>
        /// <returns>true if found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return dictionary.Contains(item);
        }

        /// <summary>Copies to the specified array</summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            dictionary.ToArray().CopyTo(array, arrayIndex);
        }

        /// <summary>Gets the number of elements in this collection</summary>
        public int Count { get { return dictionary.Count; } }

        /// <summary>Returns true</summary>
        public bool IsReadOnly { get { return true; } }

        /// <summary>Throws an exception</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>Does not return; throws an exception</returns>
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Gets an enumerator for key-value pairs in this dictionary</summary>
        /// <returns>An enumerator</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }

        /// <summary>Gets an enumerator for key-value pairs in this dictionary</summary>
        /// <returns>An enumerator</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }
    }
}
