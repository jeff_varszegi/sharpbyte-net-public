﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace SharpByte.Collections
{
    /// <summary>A queue</summary>
    /// <typeparam name="T">The type of item accepted by this queue</typeparam>
    public abstract class Queue<T> : IQueue<T>
    {
        /// <summary>Adds an item</summary>
        /// <param name="item">The item to add</param>
        public delegate void AddItem(T item);

        /// <summary>A synchronization construct</summary>
        private ReaderWriterLockSlim outputLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);

        /// <summary>Outputs which will be fed by this queue</summary>
        private List<ICollection<T>> outputs = new List<ICollection<T>>();

        /// <summary>Indicates whether this queue has outputs</summary>
        private bool hasOutputs = false;

        /// <summary>Holds the index of the last output queue to which an item was added</summary>
        private int lastQueueIndex = -1;

        /// <summary>The queue output strategy for this queue</summary>
        private QueueOutputStrategy outputStrategy = QueueOutputStrategy.Next;

        /// <summary>The queue output strategy for this queue</summary>
        public QueueOutputStrategy OutputStrategy
        {
            get { return outputStrategy; }
            set { outputStrategy = value; }
        }

        /// <summary>Adds a collection (which may be a queue) to receive items from this queue</summary>
        /// <param name="output">The output to add</param>
        public void AddOutput(ICollection<T> output)
        {
            if (output == null) return;

            outputLock.EnterWriteLock();
            outputs.Add(output);
            hasOutputs = true;
            outputLock.ExitWriteLock();
        }

        /// <summary>Removes an output</summary>
        /// <param name="output">The output to remove</param>
        public void RemoveOutput(ICollection<T> output)
        {
            if (output == null) return;

            outputLock.EnterWriteLock();
            outputs.Remove(output);
            hasOutputs = outputs.Count > 0;
            lastQueueIndex = -1;
            outputLock.ExitWriteLock();
        }

        /// <summary>Removes all output from this queue</summary>
        public void ClearOutput()
        {
            outputLock.EnterWriteLock();
            outputs.Clear();
            lastQueueIndex = -1;
            hasOutputs = false;
            outputLock.ExitWriteLock();
        }

        /// <summary>Flushes this queue to any defined output queues; if no output queues exist, does nothing. Note that if output queues throw an exception, 
        /// items may be flushed without being sent to output; if this is possible due to the underlying queue implementation, first use ToList() to take a copy of this queue's contents</summary>
        /// <returns>true if the items in this queue were added to any output queue(s) successfully or this queue contained no items, otherwise false</returns>
        public bool Flush()
        {
            outputLock.EnterWriteLock();

            if (!hasOutputs)
            {
                outputLock.ExitWriteLock();
                return false;
            }

            List<T> output = this.ToList();
            this.Clear(); // TODO

            if (output.Count == 0)
            {
                outputLock.ExitWriteLock();
                return true;
            }

            for (int x = 0; x < output.Count; x++)
                AddToOutputUnsafe(output[x]);

            outputLock.ExitWriteLock();
            return false;
        }

        /// <summary>Attempts to add an item to output for this queue, without locking</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void AddToOutputUnsafe(T item)
        {
            switch (OutputStrategy)
            {
                case QueueOutputStrategy.All:
                    for (int x = outputs.Count - 1; x >= 0; x--)
                        try { outputs[x].Add(item); } catch { }

                    return;

                case QueueOutputStrategy.Capacity:
                    ICollection<T> current, smallest = outputs[0];
                    int currentCount, smallestCount = smallest.Count;
                    for (int x = 1; x < outputs.Count; x++)
                    {
                        current = outputs[x];
                        currentCount = current.Count;
                        if (currentCount < smallestCount)
                        {
                            smallest = current;
                            smallestCount = currentCount + 1;
                        }
                    }

                    try { smallest.Add(item); } catch {}

                    return;

                case QueueOutputStrategy.Next:
                    int nextQueueIndex = (lastQueueIndex + 1) % outputs.Count;
                    lastQueueIndex = nextQueueIndex;

                    try { outputs[nextQueueIndex].Add(item); } catch {}

                    return;
            }
        }

        /// <summary>Attempts to add an item to output for this queue. Will not add the type default, as this feature is not intended for use with value types</summary>
        /// <param name="item">The item to add</param>
        /// <returns>true if the item was added to any output queues, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected bool AddToOutput(T item)
        {
            if (!hasOutputs || object.ReferenceEquals(item, default(T))) return false;

            switch (OutputStrategy)
            {
                case QueueOutputStrategy.All:
                    outputLock.EnterReadLock();
                    if (!hasOutputs)
                    {
                        outputLock.ExitReadLock();
                        return false;
                    }

                    bool returnValue = false;

                    for (int x = outputs.Count - 1; x >= 0; x--)
                        try {
                            outputs[x].Add(item);
                            returnValue = true;
                        } catch { }

                    outputLock.ExitReadLock();
                    return returnValue;

                case QueueOutputStrategy.Capacity:
                    outputLock.EnterReadLock();
                    if (!hasOutputs)
                    {
                        outputLock.ExitReadLock();
                        return false;
                    }

                    ICollection<T> current, smallest = outputs[0];
                    int currentCount, smallestCount = smallest.Count;
                    for (int x = 1; x < outputs.Count; x++)
                    {
                        current = outputs[x];
                        currentCount = current.Count;
                        if (currentCount < smallestCount)
                        {
                            smallest = current;
                            smallestCount = currentCount + 1;
                        }
                    }

                    try
                    {
                        smallest.Add(item);
                    }
                    catch
                    {
                        outputLock.ExitReadLock();
                        return false;
                    }
                    outputLock.ExitReadLock();
                    return true;

                case QueueOutputStrategy.Next:
                    outputLock.EnterReadLock();
                    if (!hasOutputs)
                    {
                        outputLock.ExitReadLock();
                        return false;
                    }

                    int nextQueueIndex = (lastQueueIndex + 1) % outputs.Count;
                    lastQueueIndex = nextQueueIndex;
                    
                    try 
                    {
                        outputs[nextQueueIndex].Add(item);
                    }
                    catch
                    {
                        outputLock.ExitReadLock();
                        return false;
                    }
                    outputLock.ExitReadLock();
                    return true;
            }

            return false;
        }

        /// <summary>Adds an item to the end of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract void Add(T item);

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Remove();

        /// <summary>Attempts to remove the specified item from this list</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>true if the specified item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract bool Remove(T item);

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        bool System.Collections.Generic.ICollection<T>.Remove(T item) { return this.Remove(item); }

        /// <summary>Gets the item at the front of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract T Peek();

        /// <summary>Gets the index of the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>The index of the specified item, or -1 if not found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract int IndexOf(T item);

        /// <summary>Inserts the item at the specified index</summary>
        /// <param name="index">The index at which to insert the item</param>
        /// <param name="item">The item to insert</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract void Insert(int index, T item);

        /// <summary>Removes the item at the specified index</summary>
        /// <param name="index">The index at which to remove the item</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract void RemoveAt(int index);

        /// <summary>Gets or sets the item at a specified index</summary>
        /// <param name="index">The index at which to access the item</param>
        /// <returns>The item at the specified index</returns>
        public abstract T this[int index] { get; set; }

        /// <summary>Removes all items from this collection</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract void Clear();

        /// <summary>Indicates whether this collection contains the specified item</summary>
        /// <param name="item">The item for which to check</param>
        /// <returns>true if the specified item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract bool Contains(T item);

        /// <summary>Copies the items in this collection to the specified array</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index in the target array at which to begin copying</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract void CopyTo(T[] array, int arrayIndex);

        /// <summary>Gets the number of items in this queue</summary>
        public abstract int Count { get; }

        /// <summary>Returns false</summary>
        public bool IsReadOnly { get { return false; } }

        /// <summary>Gets an enumerator</summary>
        /// <returns>An enumerator for this queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public abstract IEnumerator<T> GetEnumerator();

        /// <summary>Gets an enumerator</summary>
        /// <returns>An enumerator for this queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() { return this.GetEnumerator(); }
    }
}
