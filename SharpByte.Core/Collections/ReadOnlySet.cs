﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections
{
    /// <summary>A read-only set</summary>
    public sealed class ReadOnlySet<T> : ISet<T>
    {
        /// <summary>An empty singleton instance</summary>
        public static readonly ReadOnlySet<T> Empty = new ReadOnlySet<T>();

        /// <summary>The set wrapped by this instance</summary>
        private ISet<T> set;

        /// <summary>An array of values</summary>
        private T[] valueArray;

        /// <summary>The number of key-value pairs in this dictionary</summary>
        private int count;

        /// <summary>Creates a new, empty instance</summary>
        public ReadOnlySet() {
            set = new HashSet<T>();
            count = 0;
        }

        /// <summary>Constructs a new instance</summary>
        /// <param name="s">The set for which to create a read-only copy</param>
        public ReadOnlySet(ISet<T> s)
        {
            if (s == null) set = new HashSet<T>();
            else set = new HashSet<T>(s);
            count = set.Count;
        }

        /// <summary>Throws an exception</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Add(T value)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception</summary>
        void ICollection<T>.Add(T item)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Indicates whether this dictionary contains the specified value</summary>
        /// <param name="value">The value for which to check</param>
        /// <returns>true if the value was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(T value)
        {
            return set.Contains(value);
        }

        /// <summary>Throws an exception</summary>
        /// <param name="value">The value to remove</param>
        /// <returns>Does not return; throws an exception</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Remove(T value)
        {   
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Clear()
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Copies to the specified array</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index at which to start copying</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void CopyTo(T[] array, int arrayIndex)
        {
            if (valueArray == null)
                valueArray = set.ToArray();

            valueArray.CopyTo(array, arrayIndex);
        }

        /// <summary>Gets the number of elements in this collection</summary>
        public int Count { get { return count; } }

        /// <summary>Returns true</summary>
        public bool IsReadOnly { get { return true; } }

        #region Enumerators

        /// <summary>Gets an enumerator for key-value pairs in this dictionary</summary>
        /// <returns>An enumerator</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public IEnumerator<T> GetEnumerator()
        {
            return set.GetEnumerator();
        }

        /// <summary>Gets an enumerator for key-value pairs in this dictionary</summary>
        /// <returns>An enumerator</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return set.GetEnumerator();
        }

        #endregion

        #region Methods based on set operations

        /// <summary>Throws an exception</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void ExceptWith(IEnumerable<T> other)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void IntersectWith(IEnumerable<T> other)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Indicates whether this set is a proper/strict subset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in this collection plus at least one more appears in the other collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsProperSubsetOf(IEnumerable<T> other)
        {
            return set.IsProperSubsetOf(other);
        }

        /// <summary>Indicates whether this set is a proper/strict superset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in the other collection plus at least one more appears in the this collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsProperSupersetOf(IEnumerable<T> other)
        {
            return set.IsProperSupersetOf(other);
        }

        /// <summary>Indicates whether this set is a subset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in this collection appears in the other collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsSubsetOf(IEnumerable<T> other)
        {
            return set.IsSubsetOf(other);
        }

        /// <summary>Indicates whether this set is a superset of the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if every item in the other collection appears in the this collection, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool IsSupersetOf(IEnumerable<T> other)
        {
            return set.IsSupersetOf(other);
        }

        /// <summary>Indicates whether this collection contains at least one element equal to one in the specified collection</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if the collections have at least one equal element, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Overlaps(IEnumerable<T> other)
        {
            return set.Overlaps(other);
        }

        /// <summary>Indicates whether this collection and the specified other one contain the same elements</summary>
        /// <param name="other">The collection to compare</param>
        /// <returns>true if the sets cotnain the same elemnts, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool SetEquals(IEnumerable<T> other)
        {
            return set.SetEquals(other);
        }

        /// <summary>Throws an exception</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void SymmetricExceptWith(IEnumerable<T> other)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        /// <summary>Throws an exception</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void UnionWith(IEnumerable<T> other)
        {
            throw new InvalidOperationException("Attempt to mutate a read-only collection");
        }

        #endregion Methods based on set operations
    }
}
