﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections
{
    /// <summary>Implements a singleton enumerator for empty lists</summary>
    /// <typeparam name="T">The type for the lists to enumerate</typeparam>
    public class EmptyListEnumerator<T> : IEnumerator<T>
    {
        /// <summary>The singleton instance for this type</summary>
        public static readonly EmptyListEnumerator<T> Instance = new EmptyListEnumerator<T>();

        /// <summary>Gets the type default</summary>
        public T Current { get { return default(T); } }

        /// <summary>Gets the type default</summary>
        object System.Collections.IEnumerator.Current { get { return default(T); } }

        /// <summary>Default constructor</summary>
        private EmptyListEnumerator() { }

        /// <summary>Does nothing</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Dispose() { }

        /// <summary>Returns false</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool MoveNext() { return false; }

        /// <summary>Does nothing</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Reset() { }
    }
}
