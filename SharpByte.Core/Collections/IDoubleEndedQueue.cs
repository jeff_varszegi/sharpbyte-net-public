﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

namespace SharpByte.Collections
{
    /// <summary>A double-ended queue</summary>
    /// <typeparam name="T">The type handled by this queue</typeparam>
    public interface IDoubleEndedQueue<T> : IQueue<T>
    {
        /// <summary>Adds an item to the front of the queue</summary>
        /// <param name="item">The item to add</param>
        void AddFirst(T item);

        /// <summary>Adds an item to the end of the queue</summary>
        /// <param name="item">The item to add</param>
        void AddLast(T item);

        /// <summary>Gets the item from the front of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        T PeekFirst();

        /// <summary>Gets the item from the end of the queue, without removing it</summary>
        /// <returns>The item at the end of the queue</returns>
        T PeekLast();

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        T RemoveFirst();

        /// <summary>Removes an item from the end of the queue</summary>
        /// <returns>The item at the end of the queue</returns>
        T RemoveLast();
    }
}
