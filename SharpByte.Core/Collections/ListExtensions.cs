﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;

#if (!COLOCATE_EXTENSIONS && !GLOBAL_EXTENSIONS)
using SharpByte.Extensions;
#endif
using SharpByte.Utility;

#if COLOCATE_EXTENSIONS
namespace System
{
#elif !GLOBAL_EXTENSIONS
namespace SharpByte.Collections
{
#endif

/// <summary>Provides extension/utility logic for list objects</summary>
public static partial class ListExtensions
{ 
    /// <summary>A random number generator</summary>
    private static MultiRandom random;

    /// <summary>A synchronization object</summary>
    private static object randomLock = new object();

    /// <summary>Gets a random number generator</summary>
    private static MultiRandom StaticRandom
    {
        get
        {
            if (random == null)
                lock (randomLock)
                    if (random == null)
                        random = new MultiRandom();

            return random;
        }
    }

    /// <summary>Shuffles this collection in place</summary>
    /// <typeparam name="T">The type of items</typeparam>
    /// <param name="items">The collection to shuffle</param>
    public static void Shuffle<T>(this IList<T> items)
    {
        int y;
        T temp;
        int count = items.Count;

        MultiRandom r = StaticRandom;

        for (int x = count - 1; x >= 0; x--)
        {
            y = r.Next(count);
            if (x != y)
            {
                temp = items[x];
                items[x] = items[y];
                items[y] = temp;
            }
        }
    }

    /// <summary>Shuffles this collection in place</summary>
    /// <typeparam name="T">The type of items</typeparam>
    /// <param name="items">The collection to shuffle</param>
    public static void Shuffle(this IList items)
    {
        int y;
        object temp;
        int count = items.Count;

        MultiRandom r = StaticRandom;

        for (int x = count - 1; x >= 0; x--)
        {
            y = r.Next(count);
            if (x != y)
            {
                temp = items[x];
                items[x] = items[y];
                items[y] = temp;
            }
        }
    }
}

#if (COLOCATE_EXTENSIONS || !GLOBAL_EXTENSIONS)
}
#endif
