﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

namespace SharpByte.Collections
{
    /// <summary>A priority queue</summary>
    /// <typeparam name="T">The type handled by this queue</typeparam>
    public interface IPriorityQueue<T> : IDoubleEndedQueue<T>
    {
        /// <summary>Adds an item to the front of the specified priority</summary>
        /// <param name="item">The item to add</param>
        /// <param name="priority">The priority at which to add the item</param>
        void AddFirst(T item, int priority);

        /// <summary>Adds an item to the front of the specified priority</summary>
        /// <param name="item">The item to add</param>
        /// <param name="priority">The priority at which to add the item</param>
        void Add(T item, int priority);

        /// <summary>Adds an item to the end of the specified priority</summary>
        /// <param name="item">The item to add</param>
        /// <param name="priority">The priority at which to add the item</param>
        void AddLast(T item, int priority);

        /// <summary>Gets the item from the front of the specified priority, without removing it</summary>
        /// <param name="priority">The priority at which to peek at the item</param>
        /// <returns>The item at the front of the queue</returns>
        T PeekFirst(int priority);

        /// <summary>Gets the item from the specified priority, without removing it</summary>
        /// <param name="priority">The priority at which to peek at the item</param>
        /// <returns>The next item in the queue</returns>
        T Peek(int priority);

        /// <summary>Gets the item from the end of the specified priority, without removing it</summary>
        /// <param name="priority">The priority at which to peek at the item</param>
        /// <returns>The item at the end of the queue</returns>
        T PeekLast(int priority);

        /// <summary>Removes an item from the front of the specified priority</summary>
        /// <param name="priority">The priority at which to remove the item</param>
        /// <returns>The item at the front of the priority</returns>
        T RemoveFirst(int priority);

        /// <summary>Removes an item from the specified priority</summary>
        /// <param name="priority">The priority at which to remove the item</param>
        /// <returns>The first available item from the priority</returns>
        T Remove(int priority);

        /// <summary>Removes an item from the end of the specified priority</summary>
        /// <param name="priority">The priority at which to remove the item</param>
        /// <returns>The item at the end of the priority</returns>
        T RemoveLast(int priority);
    }
}
