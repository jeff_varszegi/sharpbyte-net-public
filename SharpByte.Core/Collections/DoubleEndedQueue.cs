﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections
{
    /// <summary>A non-concurrent, double-ended queue</summary>
    /// <typeparam name="T">The type for this collection</typeparam>
    public class DoubleEndedQueue<T> : Queue<T>, IDoubleEndedQueue<T>
    {
        #region Inner classes

        private class DoubleEndedQueueEnumerator : IEnumerator<T>, System.Collections.IEnumerator
        {
            /// <summary>The queue enumerated by this enumerator</summary>
            private DoubleEndedQueue<T> queue;

            /// <summary>The current index pointed to by this enumerator</summary>
            private int index = -1;

            /// <summary>The current item pointed to by this enumerator</summary>
            private T current;

            /// <summary>Indicates whether an error condition has been found, such as enumerating off the end of the list</summary>
            private bool isError = false;

            /// <summary>Constructs a new instance</summary>
            /// <param name="_queue">The queue to enumerate</param>
            public DoubleEndedQueueEnumerator(DoubleEndedQueue<T> _queue)
            {
                queue = _queue;
                current = default(T);
            }

            /// <summary>Gets the currently pointed-to item</summary>
            public T Current
            {
                get
                {
                    return current;
                }
            }

            /// <summary>Disposes of this object</summary>
            public void Dispose() { }

            /// <summary>
            /// Gets the currently pointed-to item
            /// </summary>
            object System.Collections.IEnumerator.Current { get { return current; } }

            /// <summary>Moves to the next item to be enumerated</summary>
            /// <returns>The next item</returns>
            public bool MoveNext()
            {
                if (isError) return false;

                index++;

                if (index >= queue.count)
                {
                    isError = true;
                    current = default(T);
                    return false;
                }
                else
                {
                    current = queue[index];
                    return true;
                }
            }

            /// <summary>Resets this enumerator</summary>
            public void Reset()
            {
                index = -1;
                current = default(T);
                isError = false;
            }
        }

        #endregion Inner classes

        #region Fields and properties

        /// <summary>The default capacity for new instances</summary>
        public const int DefaultCapacity = 50;

        /// <summary>The maximum capacity for new instances</summary>
        public const int MaximumInitialCapacity = 10000000;

        /// <summary>The maximum capacity for new instances</summary>
        public const int MinimumInitialCapacity = 10;

        /// <summary>The default growth factor</summary>
        public const double DefaultGrowthFactor = 3.0D;

        /// <summary>The minimum growth factor</summary>
        public const double MinimumGrowthFactor = 2.0D;

        /// <summary>The minimum growth factor</summary>
        private const double MaximumGrowthFactor = 20.0D;

        /// <summary>The maximum size at which to increase capacity</summary>
        private int maximumGrowthCapacity;

        /// <summary>The elements stored in this queue</summary>
        private T[] items;

        /// <summary>The index of the first item in the queue</summary>
        private int startIndex = 0;

        /// <summary>The index of the last item in the queue</summary>
        private int endIndex = -1;

        /// <summary>The number of items which can currently be stored in this queue without resizing</summary>
        private int capacity;

        /// <summary>The number by which the capacity is multiplied during resizing operations</summary>
        private double growthFactor;

        private int count;

        /// <summary>The number of items in this collection</summary>
        public override int Count { get { return count; } }

        #endregion Fields and properties

        /// <summary>Creates a new instance</summary>
        /// <param name="initialCapacity">The initial capacity of the queue</param>
        /// <param name="initialGrowthFactor">The growth factor to use when increasing capacity</param>
        public DoubleEndedQueue(int initialCapacity, double initialGrowthFactor)
        {
            capacity = initialCapacity.RestrictTo(MinimumInitialCapacity, MaximumInitialCapacity);
            items = new T[capacity];
            count = 0;

            growthFactor = growthFactor.RestrictTo(MinimumGrowthFactor, MaximumGrowthFactor);
            maximumGrowthCapacity = (int)(int.MaxValue / growthFactor);
        }

        /// <summary>Constructs a new instance</summary>
        public DoubleEndedQueue() : this(DefaultCapacity, DefaultGrowthFactor) { }

        /// <summary>Increases the capacity of this queue</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void IncreaseCapacity()
        {
            if (capacity == int.MaxValue) return;

            capacity = capacity < maximumGrowthCapacity ? (int)(capacity * growthFactor) : int.MaxValue;

            T[] newItems = new T[capacity];

            if (startIndex > endIndex)
            {
                Array.Copy(items, startIndex, newItems, 0, (Count - startIndex));
                Array.Copy(items, 0, newItems, (Count - startIndex), endIndex + 1);
            }
            else
                Array.Copy(items, startIndex, newItems, 0, Count);

            items = newItems;
            startIndex = 0;
            endIndex = Count - 1;
        }

        /// <summary>Adds an item to the front of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddFirst(T item) 
        {
            if (AddToOutput(item)) return;

            if (count == capacity)
            {
                IncreaseCapacity();
                startIndex = capacity - 1;
            }
            else
            {
                startIndex = startIndex == 0 ? capacity - 1 : startIndex - 1;
            }
            items[startIndex] = item;
            count++;
        }

        /// <summary>Adds an item to the end of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddLast(T item)
        {
            if (AddToOutput(item)) return;

            if (count == capacity)
            {
                IncreaseCapacity();
                endIndex++;
            }
            else
            {
                endIndex = endIndex == capacity - 1 ? 0 : endIndex + 1;
            }
            items[endIndex] = item;
            count++;
        }

        /// <summary>Adds an item to the end of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Add(T item) { AddLast(item); }

        /// <summary>Attempts to peek at the first item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool TryPeekFirst(out T returnValue)
        {
            if (count == 0)
            {
                returnValue = default(T);
                return false;
            }
            else
            {
                returnValue = items[startIndex];
                return true;
            }
        }

        /// <summary>Gets the item from the front of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T PeekFirst() 
        {
            T returnValue;
            TryPeekFirst(out returnValue);
            return returnValue;
        }

        /// <summary>Attempts to peek at the first item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool TryPeek(out T returnValue)
        {
            return TryPeekFirst(out returnValue);
        }

        /// <summary>Gets the item at the front of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override T Peek() { return PeekFirst(); }

        /// <summary>Attempts to peek at the last item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool TryPeekLast(out T returnValue)
        {
            if (count == 0)
            {
                returnValue = default(T);
                return false;
            }
            else
            {
                returnValue = items[endIndex];
                return true;
            }
        }

        /// <summary>Gets the item from the end of the queue, without removing it</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T PeekLast()
        {
            T returnValue;
            TryPeekLast(out returnValue);
            return returnValue;
        }

        /// <summary>Attempts to remove the first item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool TryRemoveFirst(out T returnValue)
        {
            if (count == 0)
            {
                returnValue = default(T);
                return false;
            }

            returnValue = items[startIndex];
            startIndex = (startIndex + 1) % capacity;
            count--;
            return true;
        }

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T RemoveFirst() 
        {
            T returnValue;
            TryRemoveFirst(out returnValue);
            return returnValue;
        }

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override T Remove() { return RemoveFirst(); }

        /// <summary>Attempts to remove the last item in the queue</summary>
        /// <param name="returnValue">The return value, if successful</param>
        /// <returns>true if successful, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool TryRemoveLast(out T returnValue)
        {
            if (count == 0)
            {
                returnValue = default(T);
                return false;
            }

            returnValue = items[endIndex];
            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
            count--;
            return true;
        }

        /// <summary>Removes an item from the front of the queue</summary>
        /// <returns>The item at the front of the queue</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T RemoveLast()
        {
            T returnValue;
            TryRemoveLast(out returnValue);
            return returnValue;
        }

        /// <summary>Gets the index of the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>The index of the specified item, or -1 if not found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override int IndexOf(T item)
        {
            if (count == 0) return -1;

            int stopIndex = (endIndex + 1) % capacity;
            int returnValue = 0;
            for (int x = startIndex; x != stopIndex; x = (x + 1) % capacity)
            {
                if (object.ReferenceEquals(items[x], item))
                    return returnValue;
                else
                    returnValue++;
            }

            return -1;
        }

        /// <summary>Inserts the item at the specified index</summary>
        /// <param name="index">The index at which to insert the item</param>
        /// <param name="item">The item to insert</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Insert(int index, T item)
        {
            if (index < 0) return;
            else if (index >= count) index = count;

            if (count == capacity)
            {
                IncreaseCapacity();
                if (index < count)
                    Array.Copy(items, index, items, index + 1, count - index);
                items[index] = item;
            }
            else
            {
                int translatedIndex = (startIndex + index) % items.Length;

                if (index < count)
                {
                    if (translatedIndex >= startIndex)
                    {
                        if (startIndex > 0)
                        {
                            Array.Copy(items, startIndex, items, startIndex - 1, (translatedIndex - startIndex) + 1);
                            startIndex--;
                        }
                        else
                        {
                            // Shift forward into the hole
                            Array.Copy(items, translatedIndex, items, translatedIndex + 1, endIndex - translatedIndex);
                            endIndex++;
                        }
                    }
                    else
                    {
                        // Shift forward into the hole
                        Array.Copy(items, translatedIndex, items, translatedIndex + 1, endIndex - translatedIndex);
                        endIndex++;
                    }
                }

                items[translatedIndex] = item;
            }

            count++;
        }

        /// <summary>Removes the item at the specified index</summary>
        /// <param name="index">The index at which to remove the item</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void RemoveAt(int index)
        {
            if (index < 0 || index >= count) return;

            int translatedIndex = (startIndex + index) % capacity;

            if (translatedIndex == startIndex)
            {
                items[startIndex] = default(T);
                startIndex = (startIndex + 1) % capacity;
            }
            else if (translatedIndex > startIndex)
            {
                Array.Copy(items, startIndex, items, startIndex + 1, translatedIndex - startIndex);
                items[startIndex] = default(T);
                startIndex = (startIndex + 1) % capacity;
            }
            else if (translatedIndex == endIndex)
            {
                items[endIndex] = default(T);
                endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
            }
            else // translatedIndex < EndIndex
            {
                Array.Copy(items, translatedIndex + 1, items, translatedIndex, endIndex - translatedIndex);
                items[endIndex] = default(T);
                endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
            }

            count--;
        }

        /// <summary>Gets or sets the item at a specified index</summary>
        /// <param name="index">The index at which to access the item</param>
        /// <returns>The item at the specified index</returns>
        public override T this[int index]
        {
            get
            {
                if (index < 0 || index >= count) return default(T);
                return items[(startIndex + index) % capacity];
            }

            set
            {
                if (index < 0 || index >= count) return;
                items[(startIndex + index) % capacity] = value;
            }
        }

        /// <summary>Removes all items from this collection</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Clear()
        {
            Array.Clear(items, 0, capacity);
            count = 0;
            startIndex = 0;
            endIndex = -1;
        }

        /// <summary>Indicates whether this collection contains the specified item</summary>
        /// <param name="item">The item for which to check</param>
        /// <returns>true if the specified item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool Contains(T item)
        {
            if (count == 0) return false;

            if (startIndex > endIndex)
            {
                for (int x = startIndex; x < capacity; x++)
                {
                    if (object.ReferenceEquals(item, items[x]))
                        return true;
                }

                for (int x = 0; x <= endIndex; x++)
                {
                    if (object.ReferenceEquals(item, items[x]))
                        return true;
                }
            }
            else
            {
                for (int x = startIndex; x <= endIndex; x++)
                    if (object.ReferenceEquals(item, items[x]))
                        return true;
            }

            return false;
        }

        /// <summary>Copies the items in this collection to the specified array</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index in the target array at which to begin copying</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null || arrayIndex < 0 || arrayIndex >= array.Length) return;

            int destinationSpace = (array.Length - arrayIndex) + 1;
            int itemsToCopy = destinationSpace < Count ? destinationSpace : Count;

            if (startIndex > endIndex)
            {
                int splitItemsToCopy = capacity - startIndex;

                if (splitItemsToCopy >= itemsToCopy)
                {
                    Array.Copy(items, startIndex, array, arrayIndex, itemsToCopy);
                }
                else
                {
                    Array.Copy(items, startIndex, array, arrayIndex, splitItemsToCopy);
                    arrayIndex += splitItemsToCopy;
                    splitItemsToCopy = itemsToCopy - splitItemsToCopy;
                    Array.Copy(items, 0, array, arrayIndex, splitItemsToCopy);
                }
            }
            else
            {
                Array.Copy(items, startIndex, array, 0, itemsToCopy);
            }
        }

        /// <summary>Attempts to remove the specified item from this list</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>true if the specified item was found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool Remove(T item)
        {
            if (count == 0) return false;

            if (startIndex > endIndex)
            {
                for (int translatedIndex = startIndex; translatedIndex < capacity; translatedIndex++)
                {
                    if (object.ReferenceEquals(item, items[translatedIndex]))
                    {
                        if (translatedIndex == startIndex)
                        {
                            items[startIndex] = default(T);
                            startIndex = (startIndex + 1) % capacity;
                        }
                        else // if (translatedIndex > StartIndex)
                        {
                            Array.Copy(items, startIndex, items, startIndex + 1, translatedIndex - startIndex);
                            items[startIndex] = default(T);
                            startIndex = (startIndex + 1) % capacity;
                        }

                        return true;
                    }
                }

                for (int translatedIndex = 0; translatedIndex <= endIndex; translatedIndex++)
                {
                    if (object.ReferenceEquals(item, items[translatedIndex]))
                    {
                        if (translatedIndex == endIndex)
                        {
                            items[endIndex] = default(T);
                            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
                        }
                        else // translatedIndex < EndIndex
                        {
                            Array.Copy(items, translatedIndex + 1, items, translatedIndex, endIndex - translatedIndex);
                            items[endIndex] = default(T);
                            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
                        }

                        return true;
                    }
                }

            }
            else
            {
                for (int translatedIndex = startIndex; translatedIndex <= endIndex; translatedIndex++)
                {
                    if (object.ReferenceEquals(item, items[translatedIndex]))
                    {
                        if (translatedIndex == startIndex)
                        {
                            items[startIndex] = default(T);
                            startIndex = (startIndex + 1) % capacity;
                        }
                        else if (translatedIndex > startIndex)
                        {
                            Array.Copy(items, startIndex, items, startIndex + 1, translatedIndex - startIndex);
                            items[startIndex] = default(T);
                            startIndex = (startIndex + 1) % capacity;
                        }
                        else if (translatedIndex == endIndex)
                        {
                            items[endIndex] = default(T);
                            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
                        }
                        else // translatedIndex < EndIndex
                        {
                            Array.Copy(items, translatedIndex + 1, items, translatedIndex, endIndex - translatedIndex);
                            items[endIndex] = default(T);
                            endIndex = endIndex == 0 ? capacity - 1 : endIndex - 1;
                        }

                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>Gets an enumerator for this list</summary>
        /// <returns>An enumerator</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override IEnumerator<T> GetEnumerator()
        {
            return new DoubleEndedQueueEnumerator(this);
        }

        /// <summary>Gets an enumerator for this list</summary>
        /// <returns>An enumerator</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new DoubleEndedQueueEnumerator(this);
        }
    }
}
