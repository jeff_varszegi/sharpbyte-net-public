﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections
{
    /// <summary>A queue which supports use as a double-ended and priority queue, as well as all list operations</summary>
    /// <typeparam name="T">The item type for this queue</typeparam>
    public class PriorityQueue<T> : Queue<T>, IPriorityQueue<T>
    {
        #region Inner classes

        /// <summary>Contains items for a priority</summary>
        private class Segment
        {
            /// <summary>The multiplier by which to increase capacity each time</summary>
            public const int GrowthFactor = 3;

            /// <summary>The maximum size at which to increase capacity</summary>
            public const int MaximumGrowthCapacity = int.MaxValue / GrowthFactor;

            /// <summary>The elements stored for this segment's priority</summary>
            public T[] Items;

            /// <summary>The index of the first item</summary>
            public int StartIndex;

            /// <summary>The index of the end item</summary>
            public int EndIndex = -1;

            /// <summary>The number of items stored in this segment</summary>
            public int Count;

            /// <summary>The current storage capacity of this segment</summary>
            public int Capacity;

            /// <summary>The item at the front of this list</summary>
            public T First { get { return Count == 0 ? default(T) : Items[StartIndex]; } }

            /// <summary>The item at the end of this list</summary>
            public T Last { get { return Count == 0 ? default(T) : Items[EndIndex]; } }

            /// <summary>Constructs a new instance</summary>
            /// <param name="capacity">The starting capacity of the new segment</param>
            public Segment(int capacity)
            {
                Items = new T[capacity];
                Capacity = capacity;
            }

            /// <summary>Increases the capacity of this list</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void IncreaseCapacity()
            {
                if (Capacity == int.MaxValue) return;

                Capacity = Capacity < MaximumGrowthCapacity ? Capacity * GrowthFactor : int.MaxValue;

                T[] newItems = new T[Capacity];

                if (StartIndex > EndIndex) 
                {
                    Array.Copy(Items, StartIndex, newItems, 0, (Count - StartIndex));
                    Array.Copy(Items, 0, newItems, (Count - StartIndex), EndIndex + 1);
                }
                else
                    Array.Copy(Items, StartIndex, newItems, 0, Count);

                Items = newItems;
                StartIndex = 0;
                EndIndex = Count - 1;
            }

            /// <summary>Inserts the specified item at the specified index, or the last available index if the specified one is off the end
            /// <param name="index">The index at which to insert</param>
            /// <param name="item">The item to insert</param>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Insert(int index, T item)
            {
                if (index < 0) return;
                else if (index >= Count) index = Count;

                if (Count == Capacity)
                {
                    IncreaseCapacity();
                    if (index < Count)
                        Array.Copy(Items, index, Items, index + 1, Count - index);
                    Items[index] = item;
                }
                else
                {
                    int translatedIndex = (StartIndex + index) % Items.Length;

                    if (index < Count)
                    {
                        if (translatedIndex >= StartIndex)
                        {
                            if (StartIndex > 0) 
                            {
                                Array.Copy(Items, StartIndex, Items, StartIndex - 1, (translatedIndex - StartIndex) + 1);
                                StartIndex--;
                            }
                            else 
                            {
                                // Shift forward into the hole
                                Array.Copy(Items, translatedIndex, Items, translatedIndex + 1, EndIndex - translatedIndex);
                                EndIndex++;
                            }
                        }
                        else
                        {
                            // Shift forward into the hole
                            Array.Copy(Items, translatedIndex, Items, translatedIndex + 1, EndIndex - translatedIndex);
                            EndIndex++;
                        }
                    }

                    Items[translatedIndex] = item;
                }

                Count++;
            }

            /// <summary>Adds an item to the front of this list</summary>
            /// <param name="item">The item to add</param>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void AddFirst(T item)
            {
                if (Count == Capacity)
                {
                    IncreaseCapacity();
                    StartIndex = Capacity - 1;
                }
                else
                {
                    StartIndex = StartIndex == 0 ? Capacity - 1 : StartIndex - 1;                    
                }
                Items[StartIndex] = item;
                Count++;
            }

            /// <summary>Adds an item to the end of this list</summary>
            /// <param name="item">The item to add</param>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void AddLast(T item)
            {
                if (Count == Capacity)
                {
                    IncreaseCapacity();
                    EndIndex++;
                }
                else
                {
                    EndIndex = EndIndex == Capacity - 1 ? 0 : EndIndex + 1;
                }
                Items[EndIndex] = item;
                Count++;
            }

            /// <summary>Copies the items in this collection to the specified array</summary>
            /// <param name="array">The array to which to copy</param>
            /// <param name="arrayIndex">The index in the target array at which to begin copying</param>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public int CopyTo(T[] array, int arrayIndex)
            {
                int destinationSpace = (array.Length - arrayIndex) + 1;
                int itemsToCopy = destinationSpace < Count ? destinationSpace : Count;

                if (StartIndex > EndIndex)
                {
                    int splitItemsToCopy = Capacity - StartIndex;

                    if (splitItemsToCopy >= itemsToCopy)
                    {
                        Array.Copy(Items, StartIndex, array, arrayIndex, itemsToCopy);
                        return itemsToCopy;
                    }

                    Array.Copy(Items, StartIndex, array, arrayIndex, splitItemsToCopy);
                    arrayIndex += splitItemsToCopy;
                    splitItemsToCopy = itemsToCopy - splitItemsToCopy;
                    Array.Copy(Items, 0, array, arrayIndex, splitItemsToCopy);
                }
                else
                {
                    Array.Copy(Items, StartIndex, array, 0, itemsToCopy);
                }

                return itemsToCopy;
            }

            /// <summary>Removes an item from the front of this list</summary>
            /// <returns>The requested item, or the type default if none is available</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public T RemoveFirst()
            {
                if (Count == 0) return default(T);
                T returnValue = Items[StartIndex];
                Items[StartIndex] = default(T);
                StartIndex = (StartIndex + 1) % Capacity;
                Count--;
                return returnValue;
            }

            /// <summary>Removes an item from the end of this list</summary>
            /// <returns>The requested item, or the type default if none is available</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public T RemoveLast()
            {
                if (Count == 0) return default(T);
                T returnValue = Items[EndIndex];
                Items[EndIndex] = default(T);
                EndIndex = EndIndex == 0 ? Capacity - 1 : EndIndex - 1;
                Count--;
                return returnValue;
            }

            /// <summary>Gets the index of the specified item, or -1 if not found</summary>
            /// <param name="item">The item for which to search</param>
            /// <returns>The index, or -1 if not found</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public int IndexOf(T item)
            {
                if (Count == 0) return -1;

                int stopIndex = (EndIndex + 1) % Capacity;
                int returnValue = 0;
                for (int x = StartIndex; x != stopIndex; x = (x + 1) % Capacity)
                {
                    if (object.ReferenceEquals(Items[x], item))
                        return returnValue;
                    else
                        returnValue++;
                }

                return -1;
            }

            /// <summary>Removes an item at the specified index</summary>
            /// <param name="index">The index at which to remove the item</param>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void RemoveAt(int index)
            {
                if (index < 0 || index >= Count) return;
                                
                int translatedIndex = (StartIndex + index) % Capacity;

                if (translatedIndex == StartIndex)
                {
                    Items[StartIndex] = default(T);
                    StartIndex = (StartIndex + 1) % Capacity;
                }
                else if (translatedIndex > StartIndex)
                {
                    Array.Copy(Items, StartIndex, Items, StartIndex + 1, translatedIndex - StartIndex);
                    Items[StartIndex] = default(T);
                    StartIndex = (StartIndex + 1) % Capacity;
                }
                else if (translatedIndex == EndIndex)
                {
                    Items[EndIndex] = default(T);
                    EndIndex = EndIndex == 0 ? Capacity - 1 : EndIndex - 1;
                }
                else // translatedIndex < EndIndex
                {
                    Array.Copy(Items, translatedIndex + 1, Items, translatedIndex, EndIndex - translatedIndex);
                    Items[EndIndex] = default(T);
                    EndIndex = EndIndex == 0 ? Capacity - 1 : EndIndex - 1;
                }

                Count--;
            }

            /// <summary>Accesses the item at the specified index</summary>
            /// <param name="index">The index of the item to access</param>
            /// <returns>For the getter, returns the item at the index if found, otherwise the type default</returns>
            public T this[int index]
            {
                get
                {
                    if (index < 0 || index >= Count) return default(T);
                    return Items[(StartIndex + index) % Capacity];
                }

                set
                {
                    if (index < 0 || index >= Count) return;
                    Items[(StartIndex + index) % Capacity] = value;
                }
            }

            /// <summary>Removes all items in this list</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Clear()
            {
                Array.Clear(Items, 0, Capacity);
                Count = 0;
                StartIndex = 0;
                EndIndex = -1;
            }

            /// <summary>Indicates whether this list contains the specified item</summary>
            /// <param name="item">The item to find</param>
            /// <returns>true if the specified item is contained in this list, otherwise false</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool Contains(T item)
            {
                if (Count == 0) return false;

                if (StartIndex > EndIndex)
                {
                    for (int x = StartIndex; x < Capacity; x++)
                        if (object.ReferenceEquals(item, Items[x]))
                            return true;

                    for (int x = 0; x <= EndIndex; x++)
                        if (object.ReferenceEquals(item, Items[x]))
                            return true;
                }
                else
                {
                    for(int x = StartIndex; x <= EndIndex; x++)
                        if (object.ReferenceEquals(item, Items[x]))
                            return true;
                }

                return false;
            }

            /// <summary>Removes the specified item</summary>
            /// <param name="item"></param>
            /// <returns></returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool Remove(T item)
            {
                if (Count == 0) return false;

                if (StartIndex > EndIndex)
                {
                    for (int translatedIndex = StartIndex; translatedIndex < Capacity; translatedIndex++)
                    {
                        if (object.ReferenceEquals(item, Items[translatedIndex]))
                        {
                            if (translatedIndex == StartIndex)
                            {
                                Items[StartIndex] = default(T);
                                StartIndex = (StartIndex + 1) % Capacity;
                            }
                            else // if (translatedIndex > StartIndex)
                            {
                                Array.Copy(Items, StartIndex, Items, StartIndex + 1, translatedIndex - StartIndex);
                                Items[StartIndex] = default(T);
                                StartIndex = (StartIndex + 1) % Capacity;
                            }

                            return true;
                        }
                    }

                    for (int translatedIndex = 0; translatedIndex <= EndIndex; translatedIndex++)
                    {
                        if (object.ReferenceEquals(item, Items[translatedIndex]))
                        {
                            if (translatedIndex == EndIndex)
                            {
                                Items[EndIndex] = default(T);
                                EndIndex = EndIndex == 0 ? Capacity - 1 : EndIndex - 1;
                            }
                            else // translatedIndex < EndIndex
                            {
                                Array.Copy(Items, translatedIndex + 1, Items, translatedIndex, EndIndex - translatedIndex);
                                Items[EndIndex] = default(T);
                                EndIndex = EndIndex == 0 ? Capacity - 1 : EndIndex - 1;
                            }

                            return true;
                        }
                    }

                }
                else
                {
                    for (int translatedIndex = StartIndex; translatedIndex <= EndIndex; translatedIndex++)
                    {
                        if (object.ReferenceEquals(item, Items[translatedIndex]))
                        {
                            if (translatedIndex == StartIndex)
                            {
                                Items[StartIndex] = default(T);
                                StartIndex = (StartIndex + 1) % Capacity;
                            }
                            else if (translatedIndex > StartIndex)
                            {
                                Array.Copy(Items, StartIndex, Items, StartIndex + 1, translatedIndex - StartIndex);
                                Items[StartIndex] = default(T);
                                StartIndex = (StartIndex + 1) % Capacity;
                            }
                            else if (translatedIndex == EndIndex)
                            {
                                Items[EndIndex] = default(T);
                                EndIndex = EndIndex == 0 ? Capacity - 1 : EndIndex - 1;
                            }
                            else // translatedIndex < EndIndex
                            {
                                Array.Copy(Items, translatedIndex + 1, Items, translatedIndex, EndIndex - translatedIndex);
                                Items[EndIndex] = default(T);
                                EndIndex = EndIndex == 0 ? Capacity - 1 : EndIndex - 1;
                            }

                            return true;
                        }
                    }
                }

                return false;
            }

        }

        /// <summary>An enumerator which uses synchronization</summary>
        private class PriorityQueueEnumerator : IEnumerator<T>, System.Collections.IEnumerator
        {
            /// <summary>The list to enumerate</summary>
            public PriorityQueue<T> queue;

            /// <summary>The current segment pointed to by this enumerator</summary>
            public Segment segment;

            /// <summary>The current segment index pointed to by this enumerator</summary>
            public int segmentIndex = -1;

            /// <summary>The current segment index pointed to by this enumerator</summary>
            public int index = -1;

            /// <summary>The current item pointed to by this enumerator</summary>
            public T current = default(T);

            /// <summary>Indicates whether an error condition has been found, such as enumerating off the end of the list</summary>
            public bool isError = false;

            /// <summary>Constructs a new instance</summary>
            /// <param name="_queue">The queue to enumerate</param>
            public PriorityQueueEnumerator(PriorityQueue<T> _queue)
            {
                queue = _queue;
            }

            /// <summary>Gets the currently pointed-to item</summary>
            public T Current
            {
                get
                {
                    if (isError) throw new InvalidOperationException("The enumerator is in an error state");
                    return current;
                }
            }

            /// <summary>Disposes of this object</summary>
            public void Dispose() { }

            /// <summary>
            /// Gets the currently pointed-to item
            /// </summary>
            object System.Collections.IEnumerator.Current
            {
                get
                {
                    if (isError) throw new InvalidOperationException("The enumerator is in an error state");
                    return current;
                }
            }

            /// <summary>Moves to the next item to be enumerated</summary>
            /// <returns>The next item</returns>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public bool MoveNext()
            {
                if (isError) return false;

                if (segmentIndex == -1)
                {
                    segmentIndex = queue.IsPriorityDescending ? queue.maximumPriorityIndex : 0;
                    segment = queue.segments[segmentIndex];
                }

                index++;

                while (segment != null && index >= segment.Count)
                {
                    if (queue.IsPriorityDescending)
                    {
                        segmentIndex--;
                        if (segmentIndex >= 0)
                            segment = queue.segments[segmentIndex];
                    }
                    else
                    {
                        segmentIndex++;
                        if (segmentIndex <= queue.maximumPriorityIndex)
                            segment = queue.segments[segmentIndex];
                    }
                }

                if (segment != null || index < segment.Count)
                {
                    current = segment[index];
                    return true;
                }
                else
                {
                    isError = true;
                    current = default(T);
                    return false;
                }
            }

            /// <summary>Resets this enumerator</summary>
            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public void Reset()
            {
                index = -1;
                segmentIndex = -1;
                segment = null;
                current = default(T);
                isError = false;
            }
        }

        #endregion Inner classes

        #region Fields and properties

        /// <summary>The default segment size</summary>
        private const int DefaultSegmentSize = 20;

        /// <summary>The maximum number of priorities manageable by a queue</summary>
        public const int MaximumPriorityCount = 100;

        /// <summary>The number of priorities managed by this queue</summary>
        public int PriorityCount { get; private set; }

        /// <summary>The highest-numbered index for a priority, i.e. one less than PriorityCount</summary>
        private int maximumPriorityIndex;

        /// <summary>If true, higher-numbered priorities will be returned first, i.e. lower-numbered priority levels are given lower priority</summary>
        public bool IsPriorityDescending { get; private set; }

        /// <summary>The priority segments managed by this queue</summary>
        private Segment[] segments;

        #endregion Fields and properties

        /// <summary>Constructs a new instance</summary>
        /// <param name="priorityCount">The number of priorities to be managed by the queue</param>
        /// <param name="isPriorityDescending">If true, higher-numbered priorities will be returned first, i.e. lower-numbered priority levels are given lower priority</param>
        public PriorityQueue(int priorityCount, bool isPriorityDescending)
        {
            PriorityCount = Math.Min(MaximumPriorityCount, Math.Max(priorityCount, 1));
            maximumPriorityIndex = PriorityCount - 1;
            IsPriorityDescending = isPriorityDescending;

            segments = new Segment[PriorityCount];
            for (int x = 0; x < segments.Length; x++)
                segments[x] = new Segment(DefaultSegmentSize);
        }

        /// <summary>Constructs a new instance with a single priority, descending</summary>
        public PriorityQueue() : this(1, true) {}

        /// <summary>Gets the zero-based index of the specified item, if it exists in this data structure</summary>
        /// <param name="item">The item for which to check</param>
        /// <returns>The zero-based index if found, in priority order, otherwise -1</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override int IndexOf(T item)
        {
            int index = 0, x, y;
            Segment segment;

            if (IsPriorityDescending) 
            {
                for (x = maximumPriorityIndex; x >= 0; x--)
                {
                    segment = segments[x];
                    for (y = 0; y < segment.Count; y++, index++)
                        if (object.ReferenceEquals(item, segment[y]))
                            return index;
                }
            }
            else
            {
                for (x = 0; x <= maximumPriorityIndex; x++)
                {
                    segment = segments[x];
                    for (y = 0; y < segment.Count; y++, index++)
                        if (object.ReferenceEquals(item, segment[y]))
                            return index;
                }
            }

            return -1;
        }

        /// <summary>Inserts the specified item at the specified index (with the default priority), or the last available index if the specified one is off 
        /// the end of the queue</summary>
        /// <param name="index">The index at which to insert</param>
        /// <param name="item">The item to insert</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Insert(int index, T item)
        {
            if (index < 0) throw new IndexOutOfRangeException();

            Segment segment;
            int x, segmentCount;

            if (IsPriorityDescending)
            {
                for (x = maximumPriorityIndex; x >= 0; x--)
                {
                    segment = segments[x];
                    segmentCount = segment.Count;

                    if (index < segmentCount || x == 0)
                    {
                        segment.Insert(index, item);
                        return;
                    }

                    index -= segmentCount;
                }
            }
            else
            {
                for (x = 0; x <= maximumPriorityIndex; x++)
                {
                    segment = segments[x];
                    segmentCount = segment.Count;

                    if (index < segmentCount || x == maximumPriorityIndex)
                    {
                        segment.Insert(index, item);
                        return;
                    }

                    x -= segmentCount;
                }
            }
        }

        /// <summary>Due to the nature of a queue, the index of an item will be constantly changing. Removal from an end is recommended instead of this call, which is provided for compatibility with the IList interface</summary>
        /// <param name="index">The index at which to remove</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void RemoveAt(int index)
        {
            if (index < 0) throw new IndexOutOfRangeException();

            Segment segment;
            int x, segmentCount;

            if (IsPriorityDescending)
            {
                for (x = maximumPriorityIndex; x >= 0; x--)
                {
                    segment = segments[x];
                    segmentCount = segment.Count;

                    if (index < segmentCount)
                        segment.RemoveAt(index);

                    index -= segmentCount;
                }
            }
            else
            {
                for (x = 0; x <= maximumPriorityIndex; x++)
                {
                    segment = segments[x];
                    segmentCount = segment.Count;

                    if (index < segmentCount)
                        segment.RemoveAt(index);

                    index -= segmentCount;
                }
            }
        }

        /// <summary>Gets or sets an element at the specified index</summary>
        /// <param name="index">The index for which to get or set the item</param>
        /// <returns>An item, for the getter operation</returns>
        public override T this[int index]
        {
            get
            {
                if (index < 0 || index >= Count) return default(T);

                Segment segment;
                int segmentCount;
                if (IsPriorityDescending)
                {
                    for (int x = maximumPriorityIndex; x >= 0; x--)
                    {
                        segment = segments[x];
                        segmentCount = segment.Count;

                        if (index < segmentCount)
                            return segment[index];

                        index -= segmentCount;
                    }
                }
                else
                {
                    for (int x = 0; x <= maximumPriorityIndex; x++)
                    {
                        segment = segments[x];
                        segmentCount = segment.Count;

                        if (index < segmentCount)
                            return segment[index];

                        index -= segmentCount;
                    }
                }

                return default(T);
            }

            set
            {
                if (index < 0 || index >= Count) return;

                Segment segment;
                int segmentCount;
                if (IsPriorityDescending)
                {
                    for (int x = maximumPriorityIndex; x >= 0; x--)
                    {
                        segment = segments[x];
                        segmentCount = segment.Count;

                        if (index < segmentCount)
                        {
                            segment[index] = value;
                            return;
                        }

                        index -= segmentCount;
                    }
                }
                else
                {
                    for (int x = 0; x <= maximumPriorityIndex; x++)
                    {
                        segment = segments[x];
                        segmentCount = segment.Count;

                        if (index < segmentCount)
                        {
                            segment[index] = value;
                            return;
                        }

                        index -= segmentCount;
                    }
                }
            }
        }

        /// <summary>Adds the specified item to the front of the specified priority in this queue</summary>
        /// <param name="item">The item to add</param>
        /// <param name="priority">The priority for which to add the item</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddFirst(T item, int priority)
        {
            if (AddToOutput(item)) return;
            priority = priority.RestrictTo(0, maximumPriorityIndex);
            segments[priority].AddFirst(item);
        }

        /// <summary>Adds the specified item to the front of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddFirst(T item)
        { 
            AddFirst(item, IsPriorityDescending ? maximumPriorityIndex : 0); 
        }

        /// <summary>Adds the specified item to the end of the specified priority in this queue</summary>
        /// <param name="item">The item to add</param>
        /// <param name="priority">The priority for which to add the item</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddLast(T item, int priority)
        {
            if (AddToOutput(item)) return;
            priority = priority.RestrictTo(0, maximumPriorityIndex);
            segments[priority].AddLast(item);
        }

        /// <summary>Adds the specified item to the end of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void AddLast(T item)
        { 
            AddLast(item, IsPriorityDescending ? 0: maximumPriorityIndex); 
        }

        /// <summary>Adds the specified item to the end of the queue</summary>
        /// <param name="item">The item to add</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Add(T item) { AddLast(item); }

        /// <summary>Adds the specified item to the end of the specified priority</summary>
        /// <param name="item">The item to add</param>
        /// <param name="priority">The priority at which to add the item</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Add(T item, int priority) { AddLast(item, priority); }

        /// <summary>Removes the first item from the front of the specified priority </summary>
        /// <param name="priority">The priority for which to get-and-remove the first item</param>
        /// <returns>The first available item, or null if none is found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T RemoveFirst(int priority)
        {
            priority = priority.RestrictTo(0, maximumPriorityIndex);
            return segments[priority].RemoveFirst();
        }

        /// <summary>Removes the first item from the front of the specified priority </summary>
        /// <param name="priority">The priority for which to get-and-remove the first item</param>
        /// <returns>The first available item, or null if none is found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Remove(int priority)
        {
            return RemoveFirst(priority);
        }

        /// <summary>Removes the first item from the front of the highest-priority segment</summary>
        /// <returns>The first available item, or null if none is found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T RemoveFirst()
        {
            Segment segment;
            if (IsPriorityDescending)
            {
                for (int x = maximumPriorityIndex; x >= 0; x--)
                {
                    segment = segments[x];
                    if (segment.Count > 0)
                        return segment.RemoveFirst();
                }
            }
            else
            {
                for (int x = 0; x <= maximumPriorityIndex; x++)
                {
                    segment = segments[x];
                    if (segment.Count > 0)
                        return segment.RemoveFirst();
                }
            }
            return default(T);
        }

        /// <summary>Removes the first item from the front of the (highest-priority first) segment</summary>
        /// <returns>The first available item, or null if none is found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override T Remove() { return RemoveFirst(); }

        /// <summary>Removes the last item from the end of the (lowest-priority first) segment</summary>
        /// <returns>The first available item, or null if none is found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T RemoveLast(int priority)
        {
            priority = priority.RestrictTo(0, maximumPriorityIndex);
            return segments[priority].RemoveLast();
        }

        /// <summary>Removes the last item from the end of the (lowest-priority first) segment</summary>
        /// <returns>The first available item, or null if none is found</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T RemoveLast()
        {
            Segment segment;
            if (IsPriorityDescending)
            {
                for (int x = 0; x <= maximumPriorityIndex; x++)
                {
                    segment = segments[x];
                    if (segment.Count > 0)
                        return segment.RemoveLast();
                }
            }
            else
            {
                for (int x = maximumPriorityIndex; x >= 0; x--)
                {
                    segment = segments[x];
                    if (segment.Count > 0)
                        return segment.RemoveLast();
                }
            }
            return default(T);
        }

        /// <summary>Gets the first item without removing it at the specified priority, if available, else the default for the type</summary>
        /// <param name="priority">The priority in which to look</param>
        /// <returns>The first item if found, otherwise the type default</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T PeekFirst(int priority)
        {
            if (priority < 0 || priority > maximumPriorityIndex) throw new ArgumentOutOfRangeException();

            return segments[priority].First;
        }

        /// <summary>Gets the first item without removing it at the specified priority, if available, else the default for the type</summary>
        /// <param name="priority">The priority in which to look</param>
        /// <returns>The first item if found, otherwise the type default</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T Peek(int priority)
        {
            return PeekFirst(priority);
        }

        /// <summary>Gets the first item without removing it, if available, else the default for the type</summary>
        /// <returns>The first item if found, otherwise the type default</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T PeekFirst() 
        {
            Segment segment;
            if (IsPriorityDescending)
            {
                for (int x = maximumPriorityIndex; x >= 0; x--)
                {
                    segment = segments[x];
                    if (segment.Count > 0)
                        return segment.First;
                }
            }
            else
            {
                for (int x = 0; x <= maximumPriorityIndex; x++)
                {
                    segment = segments[x];
                    if (segment.Count > 0)
                        return segment.First;
                }
            }
            return default(T);
        }

        /// <summary>Gets the first item without removing it, if available, else the default for the type</summary>
        /// <returns>The first item if found, otherwise the type default</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override T Peek()
        {
            return PeekFirst();
        }

        /// <summary>Gets the last item  without removing it at the specified priority, if available, else the default for the type</summary>
        /// <param name="priority">The priority in which to look</param>
        /// <returns>The last item if found, otherwise the type default</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T PeekLast(int priority)
        {
            if (priority < 0 || priority > maximumPriorityIndex) throw new ArgumentOutOfRangeException();

            return segments[priority].Last;
        }

        /// <summary>Gets the last item without removing it, if available, else the default for the type</summary>
        /// <param name="priority">The priority in which to look</param>
        /// <returns>The last item if found, otherwise the type default</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public T PeekLast() 
        {
            Segment segment;
            if (IsPriorityDescending)
            {
                for (int x = 0; x <= maximumPriorityIndex; x++)
                {
                    segment = segments[x];
                    if (segment.Count > 0)
                        return segment.Last;
                }
            }
            else
            {
                for (int x = maximumPriorityIndex; x >= 0; x--)
                {
                    segment = segments[x];
                    if (segment.Count > 0)
                        return segment.Last;
                }
            }
            return default(T);
        }

        /// <summary>Removes all items from the queue</summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void Clear()
        {
            for (int x = 0; x <= maximumPriorityIndex; x++)
                segments[x].Clear();
        }

        /// <summary>Indicates whether this queue contains the specified item</summary>
        /// <param name="item">The item to find</param>
        /// <returns>true if the specified item is found, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool Contains(T item)
        {
            for (int x = 0; x <= maximumPriorityIndex; x++)
                if (segments[x].Contains(item)) return true;

            return false;
        }

        /// <summary>Copies the contents of the queue to the specified array</summary>
        /// <param name="array">The array to which to copy</param>
        /// <param name="arrayIndex">The index at which to start copying</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null) return;
            
            int arrayLength = array.Length;
            if (arrayIndex < 0 || arrayIndex >= arrayLength) return;

            Segment segment;
            if (IsPriorityDescending)
            {
                for (int x = maximumPriorityIndex; x >= 0; x--)
                {
                    segment = segments[x];
                    arrayIndex += segment.CopyTo(array, arrayIndex);
                    if (arrayIndex >= arrayLength)
                        return;
                }
            }
            else
            {
                for (int x = 0; x < maximumPriorityIndex; x++)
                {
                    segment = segments[x];
                    arrayIndex += segment.CopyTo(array, arrayIndex);
                    if (arrayIndex >= arrayLength)
                        return;
                }
            }            
        }

        /// <summary>Gets the total count of all items in this collection</summary>
        public override int Count
        {
            get 
            {
                int count = 0;
                for (int x = 0; x <= maximumPriorityIndex; x++)
                    count += segments[x].Count;
                return count;
            }
        }

        /// <summary>Gets the count of items for the specified priority</summary>
        /// <param name="priority">The priority for which to get the count</param>
        /// <returns>The count for the specified priority</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public int GetCount(int priority)
        {
            if (priority < 0 || priority >= PriorityCount)
                throw new IndexOutOfRangeException(); // In general the queue supports safe indexing, but here it's advisable to let calling code know of the error
            else
                return segments[priority].Count;
        }

        /// <summary>Removes the first occurrence of the specified item from this structure</summary>
        /// <param name="item">The item to remove</param>
        /// <returns>true if found and removed, otherwise false</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool Remove(T item)
        {
            if (IsPriorityDescending)
            {
                for (int x = maximumPriorityIndex; x >= 0; x--)
                    if (segments[x].Remove(item))
                        return true;
            }
            else
            {
                for (int x = 0; x < maximumPriorityIndex; x++)
                    if (segments[x].Remove(item))
                        return true;
            }

            return false;
        }

        /// <summary>Gets an enumerator for this data structure</summary>
        /// <returns>An enumerator which traverses this structure in priority and index order</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override IEnumerator<T> GetEnumerator()
        {
            return new PriorityQueueEnumerator(this);
        }

        /* Used in testing
        public void Print()
        {
            for (int x = 0; x <= maximumPriorityIndex; x++)
            {
                Segment segment = segments[x];
                Console.WriteLine("SEGMENT " + x + ":");
                Console.WriteLine();

                for (int y = 0; y < segment.Count; y++)
                    System.Console.WriteLine("[" + y.ToString().PadLeft(3, '0') + "] " + segment[y]);

                //for (int y = 0; y < segment.Capacity; y++)
                //    System.Console.WriteLine("{" + y.ToString().PadLeft(3, '0') + "} " + segment.Items[y]);

                Console.WriteLine();
                Console.WriteLine();
            }
        }
        */

    }
}
