﻿/**************************************************************************************************************************************\
*                                                                                                                                      *
*   SOFTWARE LICENSE                                                                                                                   *
*                                                                                                                                      *
*   Copyright (c) 2017 Jeffrey K. Varszegi / SharpByte (www.sharpbyte.net)                                                             *
*                                                                                                                                      *
*   Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files   *
*   (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify,       *
*   merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is          *
*   furnished to do so, subject to the following conditions:                                                                           *
*                                                                                                                                      *
*   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.     *
*                                                                                                                                      *
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES    *
*   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *
*   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR     *
*   IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                                      *
*                                                                                                                                      *
\**************************************************************************************************************************************/

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SharpByte.Collections
{
    /// <summary>An enumerator/array wrapper which uses a passed array as its data source in-place</summary>
    public sealed class ArrayEnumerator<T> : IEnumerator<T>, System.Collections.IEnumerator
    {
        /// <summary>The items to enumerate</summary>
        private T[] items;

        /// <summary>The current index pointed to by this enumerator</summary>
        private int index = -1;

        /// <summary>The number of items in the array</summary>
        private int length;

        /// <summary>The current item pointed to by this enumerator</summary>
        private T current;

        /// <summary>Constructs a new instance</summary>
        /// <param name="_items">The items to enumerate</param>
        public ArrayEnumerator(T[] _items)
        {
            items = _items;
            length = _items.Length;
        }

        /// <summary>Gets the currently pointed-to item</summary>
        public T Current
        {
            get
            {
                return current;
            }
        }

        /// <summary>Disposes of this object</summary>
        public void Dispose() {}

        /// <summary>Gets the currently pointed-to item</summary>
        object System.Collections.IEnumerator.Current
        {
            get
            {
                return current;
            }
        }

        /// <summary>Moves to the next item to be enumerated</summary>
        /// <returns>The next item</returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool MoveNext()
        {
            index++;

            if (index >= length)
            {
                current = default(T);
                return false;
            }
            else
            {
                current = items[index];
                return true;
            }
        }

        /// <summary>Resets this enumerator</summary>
        public void Reset()
        {
            index = -1;
            current = default(T);
        }
    }
}
